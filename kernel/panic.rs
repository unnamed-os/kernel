extern crate alloc;

use core::panic::PanicInfo;

use kernel_native_cpu::cur_cpu;
use kernel_native_defs::cpu::CPU;
use kernel_mem::heap::Heap;
use kernel_runtime::runtime;
use klog::disable_additional_writers;
use klog::{Logger, libkernel_log::CommonLogger};
use tinystd::sizes::BinarySize;

#[panic_handler]
fn kernel_rust_panic_handler(_info: &PanicInfo) -> ! {
    disable_additional_writers();
    let logger = Logger::new("kernel-core/panic");
    logger.fatal("");
    logger.fatalf(format_args!("Rust code panic: {}", _info));
    let runtime = runtime();
    logger.infof(format_args!("CPU {}", cur_cpu().cpu_id()));
    logger.infof(format_args!(
        "Heap size: {}, used: {}",
        BinarySize::from(runtime.get::<Heap>().heap_size()), BinarySize::from(runtime.get::<Heap>().used_heap())
    ));
    cur_cpu().halt_execution()
}
