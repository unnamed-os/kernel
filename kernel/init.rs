// SPDX-License-Identifier: GPL-3.0-or-later

use crate::kernel::Kernel;
use libkernel::boot_iface::BootInterface;

pub fn kernel_main(boot_iface: &BootInterface) -> ! {
    Kernel::start(boot_iface)
}
