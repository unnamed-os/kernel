
use alloc::sync::Arc;
use kernel_hw::acpi::ACPI;
use kernel_mem::SliceMapSize;
use kernel_mem::heap::Heap;
use kernel_mem::map_phys_slice_mut;
use kernel_mem::vmm::VirtualMemoryManager;
use kernel_native_cpu::cur_cpu;
use kernel_native_defs::cpu::CPU;
use kernel_native_defs::interrupts::InterruptFuncs;
use kernel_native_defs::interrupts::InterruptsTrait;
use kernel_native_defs::mem::MemoryFuncs;
use kernel_native_defs::mem::PagingFuncs;
use kernel_native_interrupts::Interrupts;
use kernel_native_mem::Memory;
use kernel_runtime::init_runtime;
use kernel_runtime::runtime;
use kernel_runtime::runtime_store;
use kernel_runtime::runtime_get;
use klog::force_serial_logger;
use klog::register_additional_writer;
use libkernel::boot_iface::BootInterface;
use klog::{Logger, libkernel_log::CommonLogger};
use libkernel::mem::mem_protection_rw;
use libkernel_graphics::defs::framebuffer::Framebuffer;
use libkernel_graphics::render::SimpleFramebufferRenderer;
use spin::Mutex;
use tinystd::alignment::align_down;
use tinystd::alignment::align_up;
use tinystd::sizes::BinarySize;
use tinystd::sizes::KIB;

use crate::boot::NewStackInfo;
use crate::boot::next_existing_cpu_stack;
use crate::boot::notify_heap_ready;
use crate::boot::submit_new_cpu_stack;
use crate::boot::wait_for_boot_protocol;

pub struct Kernel;

impl Kernel {

    pub(crate) fn start(boot_iface: &BootInterface) -> ! {
        let memory = Memory::new();
        memory.init(boot_iface.meminfo).expect("could not initialize memory");
        let vmm = VirtualMemoryManager::new(&memory);
        let heap = Heap::new();
        let interrupts = Interrupts::new();

        interrupts.init_bsp();

        Self::prepare_preboot_smp_if_needed(boot_iface, &memory);
        memory.init_paging().expect("could not initialize paging");
        heap.init(&memory, &vmm).expect("could not initialize heap");
        notify_heap_ready();

        wait_for_boot_protocol();

        Self::setup_preboot_smp_if_needed(boot_iface, &vmm, &memory);

        // Now that the heap is ready, we can move some structures
        // into the runtime.
        let runtime = init_runtime();
        runtime.set_boot_iface(*boot_iface);

        runtime_store!(Arc::new(memory) => impl PagingFuncs, impl MemoryFuncs);
        runtime_store!(vmm);
        runtime_store!(heap);
        runtime_store!(Arc::new(interrupts) => impl InterruptFuncs);

        Self::runtime(&boot_iface)
    }

    fn prepare_preboot_smp_if_needed(
        boot_iface: &BootInterface, memory: &Memory,
    ) {
        if boot_iface.preboot_smp {
            // SMP is initialized before starting the kernel
            // Receive the current stack addresses
            let cpu_count = boot_iface.ap_cpu_count.unwrap() - 1;
            for _ in 0..cpu_count {
                let stack_info = next_existing_cpu_stack();
                let page_size = memory.preferred_page_size(stack_info.stack_size).expect("failure getting page size for stack");
                let stack_start = align_down(stack_info.stack_end - stack_info.stack_size, page_size);
                let stack_end = align_up(stack_info.stack_end, page_size);

                memory.remap_region(
                    stack_start, stack_end,
                    mem_protection_rw(), Some(page_size), "AP stack"
                ).expect("failure remapping stack for AP");
            }
        }
    }

    fn setup_preboot_smp_if_needed(
        boot_iface: &BootInterface, vmm: &VirtualMemoryManager, memory: &Memory,
    ) {
        let cpu_count = boot_iface.ap_cpu_count.unwrap() - 1;
        let stack_size = 20 * KIB as usize;
        let page_size = memory.preferred_page_size(stack_size).expect("failure getting page size for stack");
        for _ in 0..cpu_count {
            let stack_region = vmm.allocate_stack_region(stack_size, Some(page_size)).unwrap();
            memory.allocate_frames_and_map(stack_region, stack_size, mem_protection_rw()).unwrap();
            submit_new_cpu_stack(NewStackInfo {
                stack_size,
                stack_start: stack_region
            })
        }
    }

    fn runtime(boot_iface: &BootInterface) -> ! {
        let logger = Logger::new("kernel-core/runtime");
        let runtime = runtime();

        runtime_get!(impl InterruptFuncs).runtime_init_bsp();

        if boot_iface.fb.is_some() {
            logger.info("Initializing display");
            Self::init_display(&boot_iface);
            logger.info("Display has been initialized");
        }

        logger.infof(format_args!(
            "Total physical memory: {}, used: {}",
            BinarySize::from(runtime_get!(impl MemoryFuncs).total_memory()).rounded(),
            BinarySize::from(runtime_get!(impl MemoryFuncs).used_memory()).rounded()
        ));
        logger.infof(format_args!(
            "Heap size: {}, used: {}",
            BinarySize::from(runtime_get!(Heap).heap_size()).rounded(),
            BinarySize::from(runtime_get!(Heap).used_heap()).rounded()
        ));

        logger.infof(format_args!("BSP Processor {}: {}", cur_cpu().cpu_id(), cur_cpu().cpu_name()));

        // Initialize the BSP timer
        runtime_get!(impl InterruptFuncs).init_timer(cur_cpu().cpu_id());

        logger.info("Unleashing secondary processors");
        runtime.unleash_secondary_processors();

        logger.info("Idle.");

        runtime.coroutine_scope.launch(init_acpi()).unwrap();
        runtime.coroutine_scope.run()
    }

    fn init_display(boot_iface: &BootInterface) {
        let fb = boot_iface.fb.unwrap();

        let renderer = SimpleFramebufferRenderer::new(Framebuffer {
            buffer: Arc::new(Mutex::new(
                unsafe { map_phys_slice_mut(fb.phys_address, SliceMapSize::Bytes(fb.size())) }
            )),
            width: fb.width,
            height: fb.height,
            pitch: fb.pitch,
            bpp: fb.bpp,
            masks: fb.masks,
        });

        let runtime = runtime();
        runtime.set_fb_renderer(renderer);
        runtime.add_console();

        register_additional_writer(write_to_console);
        force_serial_logger();
    }
}

fn write_to_console(str: &str) -> Result<(), ()> {
    if let Some(console) = runtime().current_console() {
        console.print(str);
    }
    Ok(())
}

async fn init_acpi() {
    runtime_store!(ACPI::from(runtime().boot_iface().acpi_rsdp));
}

