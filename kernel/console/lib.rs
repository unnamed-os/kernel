#![no_std]

extern crate alloc;

use alloc::{sync::Arc, collections::BTreeMap, vec::Vec};
use libkernel_fonts::{preloaded::default_console_font, fontdue::Metrics};
use libkernel_graphics::{defs::{visual::color::{Color, WHITE, BLACK}, geometry::point::Point}, render::{FramebufferRenderer, SimpleFramebufferRenderer}};
use spin::{Mutex, Once};

struct ConsoleState<FBR : FramebufferRenderer> {
    row: u16,
    col: u16,
    foreground_color: Color,
    background_color: Color,
    cursor_color: Color,
    font_size: f32,
    renderer: Arc<FBR>,
    raster_cache: BTreeMap<char, Arc<(Metrics, Vec<u8>)>>,
    max_cols: Once<u16>,
    max_rows: Once<u16>,
    cell_width: Once<i32>,
}

impl<FBR : FramebufferRenderer> ConsoleState<FBR> {
    pub fn new(renderer: Arc<FBR>) -> Self {
        ConsoleState {
            row: 0,
            col: 0,
            foreground_color: WHITE,
            background_color: BLACK,
            cursor_color: WHITE.with_alpha(0x7F),
            font_size: 15.001,
            renderer,
            raster_cache: BTreeMap::new(),
            max_cols: Once::new(),
            max_rows: Once::new(),
            cell_width: Once::new(),
        }
    }

    fn cell_width(&self) -> i32 {
        *self.cell_width.call_once(|| default_console_font().font().metrics(' ', self.font_size).advance_width as i32)
    }

    const fn cell_height(&self) -> i32 {
        self.font_size as i32
    }

    fn max_cols(&self) -> u16 {
        *self.max_cols.call_once(|| (self.renderer.width() / (self.cell_width() + self.cell_horizontal_gap())) as u16)
    }

    fn max_rows(&self) -> u16 {
        *self.max_rows.call_once(|| (self.renderer.height() / (self.cell_height() + self.cell_vertical_gap())) as u16)
    }

    const fn cell_horizontal_gap(&self) -> i32 { 0 }
    const fn cell_vertical_gap(&self) -> i32 { 3 }
    const fn console_border_width(&self) -> i32 { 0 }

    #[inline(always)]
    fn line_feed(&mut self) {
        self.row += 1;
        // TODO: implement scrolling
        if self.row == self.max_rows() {
            self.row = 0;
        }
    }

    #[inline(always)]
    fn carriage_return(&mut self) {
        self.col = 0;
    }

    #[inline(always)]
    fn cr_lf(&mut self) {
        self.carriage_return();
        self.line_feed();
    }

    fn handle_special_character(&mut self, c: char) -> bool {
        match c {
            '\n' => {
                self.cr_lf();
                true
            }
            _ => false
        }
    }
}

pub struct Console {
    state: Mutex<ConsoleState<SimpleFramebufferRenderer>>,
}

impl Console {
    pub fn new(renderer: Arc<SimpleFramebufferRenderer>) -> Self {
        Self {
            state: Mutex::new(ConsoleState::new(renderer)),
        }
    }

    pub fn print(&self, str: &str) {
        let font = default_console_font().font();
        let mut state = self.state.lock();

        let cell_width = state.cell_width();
        let cell_height = state.cell_height();
        let cell_hgap = state.cell_horizontal_gap();
        let cell_vgap = state.cell_vertical_gap();
        let border_width = state.console_border_width();

        for char in str.chars() {
            if state.handle_special_character(char) {
                continue;
            }
            let x_offs = border_width + state.col as i32 * (cell_width + cell_hgap);
            let y_offs = border_width + state.row as i32 * (cell_height + cell_vgap);
            let raster = if let Some(raster) = state.raster_cache.get(&char) {
                raster.clone()
            } else {
                let (metrics, bitmap) = font.rasterize(char, state.font_size);
                let arc = Arc::new((metrics, bitmap));
                state.raster_cache.insert(char, arc.clone());
                arc
            };

            for x in 0..raster.0.width {
                for y in 0..raster.0.height {
                    let intensity = raster.1[y * raster.0.width + x];
                    if intensity > 0 {
                        state.renderer.plot_pixel(
                            Point::new(
                                x_offs + x as i32 + raster.0.xmin,
                                y_offs + y as i32 - raster.0.ymin + cell_height - raster.0.height as i32
                            ),
                            state.foreground_color.at_intensity(intensity as f32 / 255.0)
                        )
                    }
                }
            }
            state.col += 1;
            if state.col >= state.max_cols() {
                state.cr_lf();
            }
        }
    }
}
