#![no_std]

use core::sync::atomic::{AtomicI32, Ordering};

use alloc::{sync::Arc, vec::Vec};
use kernel_console::Console;
use kernel_native_cpu::cur_cpu;
use kernel_native_defs::{cpu::CPU, interrupts::InterruptFuncs};
use klog::{Logger, libkernel_log::CommonLogger};
use libkernel::boot_iface::BootInterface;
use libkernel_concurrency::coroutines::{scope::CoroutineScope, context::CoroutineContext};
use libkernel_graphics::render::SimpleFramebufferRenderer;
use spin::{Mutex, Once};
use tinystd_alloc::{lateinit::LateInitArc, generics::GenericLateInitTypeMap};
use tinystd_sync::Waiter;

extern crate alloc;

static KERNEL_RUNTIME: Once<Arc<Runtime>> = Once::new();

pub fn init_runtime() -> Arc<Runtime> {
    KERNEL_RUNTIME.call_once(|| Arc::new(Runtime::new())).clone()
}

pub fn runtime() -> Arc<Runtime> {
    KERNEL_RUNTIME.wait().clone()
}

pub fn has_runtime() -> bool {
    KERNEL_RUNTIME.is_completed()
}

pub fn maybe_runtime() -> Option<Arc<Runtime>> {
    KERNEL_RUNTIME.get().and_then(|arc| Some(arc.clone()))
}

pub struct Runtime {
    // The consoles should only be manipulated from one thread
    // at a time. Adding a console will return its index and thus
    // this operation needs to be atomic.
    consoles: Mutex<Vec<Arc<Console>>>,
    fb_renderer: Mutex<Option<Arc<SimpleFramebufferRenderer>>>,
    current_console: AtomicI32,
    pub coroutine_scope: CoroutineScope,
    boot_interface: LateInitArc<BootInterface>,
    secondary_processor_waiter: Waiter,

    runtime_objects: GenericLateInitTypeMap,

    logger: Logger,
}

impl Runtime {
    pub fn new() -> Self {
        Self {
            consoles: Mutex::new(Vec::new()),
            fb_renderer: Mutex::new(None),
            current_console: AtomicI32::new(-1),
            coroutine_scope: CoroutineScope::new(CoroutineContext::empty()),
            boot_interface: LateInitArc::new(),
            secondary_processor_waiter: Waiter::new(),

            runtime_objects: GenericLateInitTypeMap::new(),

            logger: Logger::new("kernel-runtime"),
        }
    }

    pub fn set_fb_renderer(&self, renderer: SimpleFramebufferRenderer) {
        let mut fb_renderer = self.fb_renderer.lock();
        *fb_renderer = Some(Arc::new(renderer))
    }

    pub fn add_console(&self) -> i32 {
        let fb_renderer = self.fb_renderer.lock();
        if fb_renderer.is_none() {
            self.logger.error("No framebuffer renderer has been set");
            panic!("Tried to add console without framebuffer");
        }
        let mut consoles = self.consoles.lock();
        consoles.push(Arc::new(Console::new(fb_renderer.as_ref().unwrap().clone())));
        let console_idx = (consoles.len() - 1).try_into().unwrap();
        self.logger.infof(format_args!("Created new console with index {}", console_idx));
        let _ = self.current_console.compare_exchange(-1, 0, Ordering::Relaxed, Ordering::Relaxed);
        console_idx
    }

    pub fn current_console(&self) -> Option<Arc<Console>> {
        (*self.consoles.lock()).get(self.current_console.load(Ordering::Relaxed) as usize).cloned()
    }

    pub fn boot_iface(&self) -> Arc<BootInterface> {
        self.boot_interface.get()
    }

    pub fn set_boot_iface(&self, boot_iface: BootInterface) {
        self.boot_interface.set(boot_iface);
    }

    pub fn unleash_secondary_processors(&self) {
        self.secondary_processor_waiter.notify()
    }

    pub fn join_ap(&self) -> ! {
        self.secondary_processor_waiter.wait();
        let cpu_id = cur_cpu().cpu_id();
        self.get::<Arc<dyn InterruptFuncs>>().init_ap(cpu_id);
        self.get::<Arc<dyn InterruptFuncs>>().init_timer(cpu_id);
        self.logger.infof(format_args!("CPU {} joined the party: {}", cpu_id, cur_cpu().cpu_name()));
        // Join the coroutine scope
        self.coroutine_scope.run()
    }

    pub fn get<T : Sized + 'static>(&self) -> Arc<T> {
        self.runtime_objects.get().unwrap()
    }

    pub fn try_get<T : 'static>(&self) -> Option<Arc<T>> {
        self.runtime_objects.get()
    }

    /// Every runtime object can only be stored once
    pub fn store<T : 'static>(&self, obj: T) {
        self.runtime_objects.set(obj)
    }

    pub fn wait_for<T : 'static>(&self) -> Arc<T> {
        self.runtime_objects.wait_for()
    }

}

#[macro_export]
macro_rules! runtime_store {
    ($arc:expr => impl $trait_type:path, $(impl $more:path),+) => {
        let arc = $arc;
        runtime_store!(arc.clone() => impl $trait_type);
        runtime_store!(arc => $(impl $more),+);
    };
    ($arc:expr => impl $trait_type:path) => {
        $crate::runtime().store($arc as Arc<dyn $trait_type>);
    };
    ($obj:expr) => {
        $crate::runtime().store($obj);
    };
}


#[macro_export]
macro_rules! runtime_get {
    (try impl $trait_type:path) => {
        $crate::maybe_runtime().and_then(|runtime| runtime.try_get::<Arc<dyn $trait_type>>())
    };
    (try $t:path) => {
        $crate::maybe_runtime().and_then(|runtime| runtime.try_get::<$t>())
    };
    (impl $trait_type:path) => {
        $crate::runtime().get::<Arc<dyn $trait_type>>()
    };
    ($t:path) => {
        $crate::runtime().get::<$t>()
    };
}

