
fn main() {
    if option_env!("KERNEL_MEM_DEBUG").is_some() {
        println!("cargo:rustc-cfg=feature=\"debug\"");
    }
}