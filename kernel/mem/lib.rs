#![no_std]

pub mod heap;
pub mod vmm;

extern crate alloc;

use core::{mem::size_of, ptr::{slice_from_raw_parts_mut, slice_from_raw_parts, NonNull}, ops::{Deref, DerefMut}};

use crate::vmm::VirtualMemoryManager;
use alloc::{sync::Arc, borrow::ToOwned};
use kernel_native_defs::mem::PagingFuncs;
use kernel_runtime::runtime_get;
use libkernel::mem::{MemoryProtectionFlags, mem_protection_r, mem_protection_rw};
use tinystd::{types::Addr, alignment::align_down};

pub enum SliceMapSize {
    Bytes(usize),
    Elements(usize),
}

pub fn map_phys_region(phys_start: Addr, size: usize, prot: MemoryProtectionFlags) -> Addr {
    /*Logger::new("test").debugf(format_args!(
        "map_phys_region({}, {}, {:?})", PtrFormatted(phys_start), BinarySize::from(size).rounded(), prot
    ));*/
    let page_size = runtime_get!(impl PagingFuncs).preferred_page_size(size).unwrap();
    let aligned_phys_start = align_down(phys_start, page_size);
    let unaligned_offset = phys_start - aligned_phys_start;

    let virt_addr = runtime_get!(VirtualMemoryManager).allocate_virt_region(size, Some(page_size)).unwrap();
    /*Logger::new("test").debugf(format_args!(
        "gonna do map_pages({}, {}, {:?}, {}",
        PtrFormatted(aligned_phys_start), PtrFormatted(virt_addr),
        prot, BinarySize::from(size).rounded()
    ));*/
    runtime_get!(impl PagingFuncs).map_pages(aligned_phys_start, virt_addr, prot, size).unwrap();
    virt_addr + unaligned_offset
}

pub fn map_phys_ptr<T>(phys: Addr, size: usize) -> *const T {
    /*Logger::new("test").debugf(format_args!(
        "map_phys_ptr({}, {})", PtrFormatted(phys), BinarySize::from(size).rounded()
    ));*/
    map_phys_region(phys, size, mem_protection_r()) as *const T
}

pub fn map_phys_ptr_mut<T>(phys: Addr, size: usize) -> *mut T {
    map_phys_region(phys, size, mem_protection_rw()) as *mut T
}

pub unsafe fn map_phys<T>(phys: Addr) -> &'static T {
    map_phys_ptr::<T>(phys, size_of::<T>()).as_ref().unwrap()
}

pub unsafe fn map_phys_with_size<T>(phys: Addr, size: usize) -> &'static T {
    map_phys_ptr::<T>(phys, size).as_ref().unwrap()
}

pub unsafe fn map_phys_mut<T>(phys: Addr) -> &'static mut T {
    map_phys_ptr_mut::<T>(phys, size_of::<T>()).as_mut().unwrap()
}

pub unsafe fn map_phys_with_size_mut<T>(phys: Addr, size: usize) -> &'static mut T {
    map_phys_ptr_mut::<T>(phys, size).as_mut().unwrap()
}

pub unsafe fn unmap_phys<T>(virt: &T) {
    unmap_phys_ptr(virt as *const T);
}

pub unsafe fn unmap_phys_ptr<T>(virt: *const T) {
    unmap_phys_region(virt as Addr, size_of::<T>())
}

pub unsafe fn unmap_phys_mut<T>(virt: &mut T) {
    unmap_phys_ptr_mut(virt as *mut T);
}

pub unsafe fn unmap_phys_ptr_mut<T>(virt: *mut T) {
    unmap_phys_region(virt as Addr, size_of::<T>())
}

pub unsafe fn unmap_phys_region(virt_start: Addr, size: usize) {
    runtime_get!(VirtualMemoryManager).free_virt_region(virt_start, size).unwrap();
    runtime_get!(impl PagingFuncs).unmap_pages(virt_start, size).unwrap();
}

pub unsafe fn map_phys_slice<T>(phys: Addr, size: SliceMapSize) -> &'static [T] {
    slice_from_raw_parts(
        map_phys_ptr(
            phys, match size {
                SliceMapSize::Bytes(bytes) => bytes,
                SliceMapSize::Elements(elements) => elements * size_of::<T>()
            },
        ),
        match size {
            SliceMapSize::Bytes(bytes) => bytes / size_of::<T>(),
            SliceMapSize::Elements(elements) => elements
        },
    ).as_ref().unwrap()
}

pub unsafe fn map_phys_slice_mut<T>(phys: Addr, size: SliceMapSize) -> &'static mut [T] {
    slice_from_raw_parts_mut(
        map_phys_ptr_mut(
            phys, match size {
                SliceMapSize::Bytes(bytes) => bytes,
                SliceMapSize::Elements(elements) => elements * size_of::<T>()
            },
        ),
        match size {
            SliceMapSize::Bytes(bytes) => bytes / size_of::<T>(),
            SliceMapSize::Elements(elements) => elements
        },
    ).as_mut().unwrap()
}

pub struct MappedRef<T : 'static> {
    data: NonNull<T>,
    phys: Addr,
    unmap_on_drop: bool,
}

impl<T : 'static> MappedRef<T> {
    pub fn new(phys: Addr) -> Self {
        Self {
            data: NonNull::new(map_phys_ptr::<T>(phys, size_of::<T>()) as *mut T).unwrap(),
            phys,
            unmap_on_drop: true,
        }
    }

    pub unsafe fn from(phys: Addr, size: usize) -> Option<Self> {
        NonNull::new(map_phys_ptr::<T>(phys, size) as *mut T).and_then(|data| Some(
            Self { data, phys, unmap_on_drop: true }
        ))
    }

    /// The caller must guarantee that phys is mapped into memory
    /// at virt and that it can be unmapped from memory
    /// by using the unmap functionality.
    pub unsafe fn from_raw(phys: Addr, virt: Addr) -> Option<Self> {
        NonNull::new(virt as *mut T).and_then(|data| Some(
            Self { data, phys, unmap_on_drop: true })
        )
    }

    pub unsafe fn from_existing(phys: Addr, virt: Addr) -> Option<Self> {
        NonNull::new(virt as *mut T).and_then(|data| Some(
            Self { data, phys, unmap_on_drop: false }
        ))
    }

    pub fn addr(&self) -> Addr {
        self.data.as_ptr() as Addr
    }

    pub fn phys_addr(&self) -> Addr {
        self.phys
    }
}

impl<T : 'static> Drop for MappedRef<T> {
    fn drop(&mut self) {
        if self.unmap_on_drop {
            unsafe { unmap_phys_ptr(self.data.as_ptr()) }
        }
    }
}

impl<T : 'static> Deref for MappedRef<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { self.data.as_ref() }
    }
}

pub struct MappedMutRef<T : 'static> {
    data: NonNull<T>,
    phys: Addr,
}

impl<T : 'static> MappedMutRef<T> {
    pub fn new(phys: Addr) -> Self {
        Self {
            data: NonNull::new(map_phys_ptr::<T>(phys, size_of::<T>()) as *mut T).unwrap(),
            phys,
        }
    }

    pub unsafe fn from(phys: Addr, size: usize) -> Option<Self> {
        NonNull::new(map_phys_ptr::<T>(phys, size) as *mut T).and_then(|data| Some(
            Self { data, phys }
        ))
    }

    /// The caller must guarantee that phys is mapped into memory
    /// at virt and that it can be unmapped from memory
    /// by using the unmap functionality.
    pub unsafe fn from_raw(phys: Addr, virt: Addr) -> Option<Self> {
        NonNull::new(virt as *mut T).and_then(|data| Some(
            Self { data, phys })
        )
    }

    pub fn addr(&self) -> Addr {
        self.data.as_ptr() as Addr
    }

    pub fn phys_addr(&self) -> Addr {
        self.phys
    }
}

impl<T : 'static> Drop for MappedMutRef<T> {
    fn drop(&mut self) {
        unsafe { unmap_phys_ptr(self.data.as_ptr()) }
    }
}

impl<T : 'static> Deref for MappedMutRef<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { self.data.as_ref() }
    }
}

impl<T : 'static> DerefMut for MappedMutRef<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.data.as_mut() }
    }
}

#[derive(Clone)]
pub struct MappedArc<T : 'static> {
    mapped_ref: Arc<MappedRef<T>>
}

impl<T : 'static> MappedArc<T> {
    /// Consumes the passed mapped reference and returns
    /// a MappedArc variant of the same
    pub fn from(mapped_ref: MappedRef<T>) -> Self {
        Self { mapped_ref: Arc::new(mapped_ref) }
    }

    pub fn new(phys: Addr) -> Self {
        Self::from(MappedRef::new(phys))
    }
}

