
use core::{mem::size_of, ops::Range};

use kernel_native_defs::mem::MemoryFuncs;
use klog::{Logger, libkernel_log::CommonLogger};
use spin::RwLock;
use tinystd::{types::Addr, sizes::BinarySize, alignment::{align_up, align_down}, formatting::PtrFormatted};

pub struct VirtualMemoryManager {
    logger: Logger,
    virt_offset: RwLock<usize>,
    stack_offset: RwLock<usize>,
    virt_range: Range<Addr>,
    stack_range: Range<Addr>,
}

impl VirtualMemoryManager {

    pub fn new(mem: &dyn MemoryFuncs) -> Self {
        return Self {
            logger: Logger::new("kernel-core/vmm"),
            virt_offset: RwLock::new(0),
            stack_offset: RwLock::new(0),
            virt_range: mem.kernel_virt_memory_range(),
            stack_range: mem.kernel_virt_stack_memory_range(),
        }
    }

    pub fn allocate_virt_region(&self, size: usize, align: Option<usize>) -> Result<Addr, &'static str> {
        //self.logger.debugf(format_args!("allocate_virt_region({})", size));
        let mut virt_offset = self.virt_offset.write();
        let virt_addr = self.virt_range.start + *virt_offset;
        let aligned_size = align_up(size, align.unwrap_or(size_of::<usize>()));
        if virt_addr + aligned_size > self.virt_range.end {
            self.logger.errorf(format_args!(
                "Out of virtual memory. Virt start: {}, end: {}, size: {}, next offset: {}",
                PtrFormatted(self.virt_range.start), PtrFormatted(self.virt_range.end),
                BinarySize::from(self.virt_range.end - self.virt_range.start).rounded(),
                PtrFormatted(*virt_offset)
            ));
            Err("virtual memory exhausted")
        } else {
            *virt_offset += aligned_size;
            Ok(virt_addr)
        }
    }

    pub fn allocate_stack_region(&self, size: usize, align: Option<usize>) -> Result<Addr, &'static str> {
        let mut stack_offset = self.stack_offset.write();
        let virt_addr = align_down(self.stack_range.end, align.unwrap_or(size_of::<usize>())) - *stack_offset;
        let aligned_size = align_down(size, align.unwrap_or(size_of::<usize>()));
        if virt_addr - aligned_size < self.stack_range.start || virt_addr < self.stack_range.start {
            self.logger.errorf(format_args!(
                "Out of stack memory. Stacks start: {}, end {}, size: {}, next offset: {}",
                PtrFormatted(self.stack_range.start), PtrFormatted(self.stack_range.end),
                BinarySize::from(self.stack_range.end - self.stack_range.start).rounded(),
                PtrFormatted(*stack_offset)
            ));
            Err("virtual stack memory exhausted")
        } else {
            *stack_offset += aligned_size;
            // maybe we can add some kind of stack protector. an unmapped page that causes a page fault
            // when accessed so that we can detect stack overflows.
            Ok(virt_addr - aligned_size)
        }
    }

    pub fn free_virt_region(&self, virt_start: Addr, size: usize) -> Result<(), &'static str> {
        /*self.logger.noticef(format_args!(
            "free_virt_region({}, {}) stub!", PtrFormatted(virt_start), BinarySize::from(size).rounded()
        ));*/
        Ok(())
    }

    pub fn total_virt_space(&self) -> usize {
        self.virt_range.size_hint().1.unwrap() + self.stack_range.size_hint().1.unwrap()
    }
    pub fn free_virt_space(&self) -> usize {
        self.total_virt_space() - self.used_virt_space()
    }
    pub fn used_virt_space(&self) -> usize {
        *self.virt_offset.read() + *self.stack_offset.read()
    }

}
