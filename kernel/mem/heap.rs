
use core::{sync::atomic::{AtomicBool, Ordering}, alloc::{GlobalAlloc, Layout}, ptr::addr_of};

use kernel_native_defs::mem::PagingFuncs;
use kernel_runtime::runtime_get;
use libkernel::mem::mem_protection_rw;

use klog::{Logger, libkernel_log::CommonLogger};
use linked_list_allocator::LockedHeap;
use tinystd::{sizes::{MIB, BinarySize, KIB}, formatting::PtrFormatted, types::Addr};
use lazy_static::lazy_static;

use crate::vmm::VirtualMemoryManager;

const BOOTSTRAP_HEAP_SIZE: usize = 128 * KIB as usize;

static mut BOOTSTRAP_HEAP: [u8; BOOTSTRAP_HEAP_SIZE] = [0; BOOTSTRAP_HEAP_SIZE];
lazy_static! {
    static ref BOOTSTRAP_ALLOCATOR: LockedHeap = unsafe {
        LockedHeap::new(addr_of!(BOOTSTRAP_HEAP) as *mut u8, BOOTSTRAP_HEAP_SIZE)
    };
}

#[global_allocator]
static HEAP_ALLOCATOR: HeapAllocator = HeapAllocator::new();

pub const KERNEL_HEAP_SIZE: usize = 16 * MIB as usize;

pub struct Heap {
    initialized: AtomicBool,
    logger: Logger,
    allocator: LockedHeap,
}

impl Heap {

    pub fn new() -> Self {
        return Heap {
            initialized: AtomicBool::new(false),
            logger: Logger::new("kernel-core/heap"),
            allocator: LockedHeap::empty(),
        }
    }

    pub fn init(&self, memory: &dyn PagingFuncs, vmm: &VirtualMemoryManager) -> Result<(), &'static str> {
        if self.initialized.load(Ordering::Acquire) {
            return Err("heap cannot be initialized twice");
        }

        self.logger.info("Initializing heap");

        let heap_start = vmm.allocate_virt_region(KERNEL_HEAP_SIZE, None).unwrap();
        self.logger.infof(
            format_args!(
                "Allocating frames and mapping (start={}, size={})",
                PtrFormatted(heap_start),
                BinarySize::from(KERNEL_HEAP_SIZE).rounded(),
            )
        );
        if let Err(msg) = memory.allocate_frames_and_map(
            heap_start,
            KERNEL_HEAP_SIZE,
            mem_protection_rw()
        ) {
            self.logger.errorf(
                format_args!(
                    "An error occurred trying to allocate frames and map: {}", msg
                )
            );
            return Err("failed to map and allocate")
        }

        self.logger.info("Setting up allocator");
        unsafe {
            self.allocator.lock().init(heap_start as *mut u8, KERNEL_HEAP_SIZE);
        }

        self.logger.infof(
            format_args!(
                "Heap has been initialized with start addr {} and heap size {}",
                PtrFormatted(heap_start),
                BinarySize::from(KERNEL_HEAP_SIZE).rounded(),
            )
        );
        self.initialized.store(true, Ordering::Release);
        Ok(())
    }

    pub fn is_initialized(&self) -> bool { self.initialized.load(Ordering::Relaxed) }

    pub fn free_heap(&self) -> usize { self.allocator.lock().free() }
    pub fn used_heap(&self) -> usize { self.allocator.lock().used() }
    pub fn heap_size(&self) -> usize { self.allocator.lock().size() }

}

struct HeapAllocator;

impl HeapAllocator {
    const fn new() -> Self {
        HeapAllocator
    }
}

unsafe impl GlobalAlloc for HeapAllocator {

    unsafe fn alloc(&self, layout: core::alloc::Layout) -> *mut u8 {
        #[cfg(feature = "debug")]
        let logger = Logger::new("kernel-mem/heap-allocator");
        if let Some(heap) = runtime_get!(try Heap) {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!("[runtime] alloc({:?})", layout));
            }
            heap.allocator.alloc(layout)
        } else {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!("[bootstrap] alloc({:?})", layout));
            }
            BOOTSTRAP_ALLOCATOR.alloc(layout)
        }
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: core::alloc::Layout) {
        #[cfg(feature = "debug")]
        let logger = Logger::new("kernel-mem/heap-allocator");
        let bootstrap_allocator_range = BOOTSTRAP_HEAP.as_ptr_range();
        if bootstrap_allocator_range.contains(&(ptr as *const u8)) {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!(
                    "[bootstrap] dealloc({}, {:?})",
                    PtrFormatted(ptr as Addr), layout
                ));
            }
            BOOTSTRAP_ALLOCATOR.dealloc(ptr, layout)
        } else {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!(
                    "[runtime] dealloc({}, {:?})", PtrFormatted(ptr as Addr), layout
                ));
            }
            runtime_get!(try Heap).unwrap().allocator.dealloc(ptr, layout)
        }
    }

    unsafe fn alloc_zeroed(&self, layout: core::alloc::Layout) -> *mut u8 {
        #[cfg(feature = "debug")]
        let logger = Logger::new("kernel-mem/heap-allocator");
        if let Some(heap) = runtime_get!(try Heap) {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!(
                    "[runtime] alloc_zeroed({:?})", layout
                ));
            }
            heap.allocator.alloc_zeroed(layout)
        } else {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!(
                    "[bootstrap] alloc_zeroed({:?})", layout
                ));
            }
            BOOTSTRAP_ALLOCATOR.alloc_zeroed(layout)
        }
    }

    unsafe fn realloc(&self, ptr: *mut u8, layout: core::alloc::Layout, new_size: usize) -> *mut u8 {
        #[cfg(feature = "debug")]
        let logger = Logger::new("kernel-mem/heap-allocator");
        if let Some(heap) = runtime_get!(try Heap) {
            let bootstrap_allocator_range = BOOTSTRAP_HEAP.as_ptr_range();
            if bootstrap_allocator_range.contains(&(ptr as *const u8)) {
                #[cfg(feature = "debug")] {
                    logger.debugf(format_args!(
                        "[runtime] realloc({}, {:?}, {}) (dealloc from bootstrap)",
                        PtrFormatted(ptr as Addr), layout, new_size
                    ));
                }
                BOOTSTRAP_ALLOCATOR.dealloc(ptr, layout);
                // Migrate to runtime allocator
                // SAFETY: the caller must ensure that the `new_size` does not overflow.
                // `layout.align()` comes from a `Layout` and is thus guaranteed to be valid.
                let new_layout = unsafe { Layout::from_size_align_unchecked(new_size, layout.align()) };
                heap.allocator.alloc(new_layout)
            } else {
                #[cfg(feature = "debug")] {
                    logger.debugf(format_args!(
                        "[runtime] realloc({}, {:?}, {})",
                        PtrFormatted(ptr as Addr), layout, new_size
                    ));
                }
                heap.allocator.realloc(ptr, layout, new_size)
            }
        } else {
            #[cfg(feature = "debug")] {
                logger.debugf(format_args!(
                    "[bootstrap] realloc({}, {:?}, {})",
                    PtrFormatted(ptr as Addr), layout, new_size
                ));
            }
            BOOTSTRAP_ALLOCATOR.realloc(ptr, layout, new_size)
        }
    }

}

pub fn bootstrap_heap_virt_addr() -> Addr {
    unsafe { BOOTSTRAP_HEAP.as_ptr() as Addr }
}

pub fn bootstrap_heap_size() -> usize {
    BOOTSTRAP_HEAP_SIZE
}
