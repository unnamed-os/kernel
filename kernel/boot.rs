use tinystd::types::Addr;
use tinystd_sync::{RendezvousChannel, Waiter};

#[derive(Copy, Clone)]
pub struct ExistingStackInfo {
    pub stack_end: Addr,
    pub stack_size: Addr,
    pub cpu_id: usize,
}

static CPU_EXISTING_STACK_CHANNEL: RendezvousChannel<ExistingStackInfo> = RendezvousChannel::new();

pub(crate) fn next_existing_cpu_stack() -> ExistingStackInfo {
    CPU_EXISTING_STACK_CHANNEL.consume()
}

pub fn submit_existing_cpu_stack(stack_info: ExistingStackInfo) {
    CPU_EXISTING_STACK_CHANNEL.submit(stack_info)
}

#[derive(Copy, Clone)]
pub struct NewStackInfo {
    pub stack_start: Addr,
    pub stack_size: Addr
}

static CPU_NEW_STACK_CHANNEL: RendezvousChannel<NewStackInfo> = RendezvousChannel::new();

pub fn consume_new_cpu_stack() -> NewStackInfo {
    CPU_NEW_STACK_CHANNEL.consume()
}

pub fn submit_new_cpu_stack(stack_info: NewStackInfo) {
    CPU_NEW_STACK_CHANNEL.submit(stack_info);
}

static BOOT_PROTOCOL_WAITER: Waiter = Waiter::new();

pub fn notify_boot_protocol_done() {
    BOOT_PROTOCOL_WAITER.notify()
}

pub(crate) fn wait_for_boot_protocol() {
    BOOT_PROTOCOL_WAITER.wait()
}

static HEAP_WAITER: Waiter = Waiter::new();

pub(crate) fn notify_heap_ready() {
    HEAP_WAITER.notify()
}

pub fn wait_for_heap() {
    HEAP_WAITER.wait()
}
