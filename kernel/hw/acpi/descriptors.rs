
use core::{mem::size_of, ptr::slice_from_raw_parts};

use alloc::{string::{String, ToString}, format, vec::Vec};
use kernel_mem::MappedRef;
use tinystd::{types::Addr, mem::ptr_after_end_of};
use tinystd_alloc::{slices::verify_byte_sum_u8_checksum_of, alignment::align_pointer};

use super::{constants::{
    RSDP_SIGNATURE_LEN,
    OEM_ID_LEN,
    OEM_TABLE_ID_LEN,
    SDT_SIGNATURE_LEN,
    RSDP_SIGNATURE, XSDT_SIGNATURE
}, Table};

#[repr(C,packed)]
pub struct RSDPv10 {
    signature: [u8; RSDP_SIGNATURE_LEN],
    checksum: u8,
    oem_id: [u8; OEM_ID_LEN],
    revision: u8,
    rsdt_addr: u32,
}

impl HasSignature for RSDPv10 {
    fn signature(&self) -> String {
        String::from_utf8(self.signature.to_vec()).unwrap()
    }
}

impl Validatable for RSDPv10 {
    fn validate(&self) -> Result<(), String> {
        let signature = self.signature();
        if signature != RSDP_SIGNATURE {
            Err(format!(
                "RSDP signature does not match. Expected '{}' but got '{}'",
                RSDP_SIGNATURE, signature
            ))
        } else {
            self.verify().or_else(|e| Err(format!("failed RSDPv10 verification: {}", e)))
        }
    }
}

impl Verifiable for RSDPv10 {
    fn verify(&self) -> Result<(), String> {
        unsafe { verify_byte_sum_u8_checksum_of(self, None, 0, 0) }
    }
}

#[repr(C,packed)]
pub struct RSDPv20 {
    v1: RSDPv10,
    length: u32,
    xsdt_addr: u64,
    extended_checksum: u8,
    _reserved: [u8; 3],
}

impl HasSignature for RSDPv20 {
    fn signature(&self) -> String { self.v1.signature() }
}

impl Validatable for RSDPv20 {
    fn validate(&self) -> Result<(), String> {
        if let Err(msg) = self.v1.validate() {
            Err(format!("validation of RSDPv10 failed: {}", msg))
        } else if let Err(msg) = self.verify() {
            Err(format!("invalid RSDPv20 checksum: {}", msg))
        } else {
            Ok(())
        }
    }
}

impl Verifiable for RSDPv20 {
    fn verify(&self) -> Result<(), String> {
        unsafe { verify_byte_sum_u8_checksum_of(self, Some(self.length as usize), 0, 0) }
    }
}

impl RSDPv20 {
    pub(super) unsafe fn map_xsdt(&self) -> MappedRef<XSDT> {
        MappedRef::new(self.xsdt_addr as Addr)
    }

    pub fn remap_xsdt_with_correct_size(&self, xsdt: MappedRef<XSDT>) -> MappedRef<XSDT> {
        let xsdt_size = xsdt.header.length as usize;
        drop(xsdt);
        unsafe { MappedRef::from(self.xsdt_addr as Addr, xsdt_size) }.unwrap()
    }
}

#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct SDTHeader {
    signature: [u8; SDT_SIGNATURE_LEN],
    length: u32,
    revision: u8,
    checksum: u8,
    oem_id: [u8; OEM_ID_LEN],
    oem_table_id: [u8; OEM_TABLE_ID_LEN],
    oem_revision: u32,
    creator_id: u32,
    creator_revision: u32,
}

impl SDTHeader {
    pub fn signature(&self) -> String {
        String::from_utf8(self.signature.to_vec()).unwrap().trim().to_string()
    }

    pub fn oem_id(&self) -> String {
        String::from_utf8(self.oem_id.to_vec()).unwrap().trim().to_string()
    }

    pub fn oem_table_id(&self) -> String {
        String::from_utf8(self.oem_table_id.to_vec()).unwrap().trim().to_string()
    }

    pub fn size(&self) -> usize {
        self.length as usize
    }
}

impl Verifiable for SDTHeader {
    fn verify(&self) -> Result<(), String> {
        unsafe {
            verify_byte_sum_u8_checksum_of(self, Some(self.length as usize), 0, 0)
        }
    }
}

/**
 * XSDT - Extended System Description Table
 */
#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct XSDT {
    header: SDTHeader,
    // Variable number of pointers of tables follow after the header
}

impl Verifiable for XSDT {
    fn verify(&self) -> Result<(), String> {
        self.header.verify()
    }
}

impl Validatable for XSDT {
    fn validate(&self) -> Result<(), String> {
        self.verify()
    }
}

impl Table for XSDT {
    fn header(&self) -> SDTHeader { self.header }
    fn expect_signature() -> &'static str { XSDT_SIGNATURE }
}

impl XSDT {
    pub fn iter_tables(&self) -> TableIterator {
        TableIterator::new(
            self,
            (self.header.length as usize / size_of::<Addr>()) as usize,
        )
    }

    pub fn iter_tables_extended(&self) -> ExtendedTableIterator {
        ExtendedTableIterator::new(
            self,
            (self.header.length as usize / size_of::<Addr>()) as usize,
        )
    }
}

pub struct ExtendedTableIterator {
    tables: Vec<*const SDTHeader>,
    current_table: usize,
}

#[derive(Copy, Clone)]
pub struct ExtendedTableInfo {
    pub header: SDTHeader,
    pub phys_addr: Addr,
}

impl ExtendedTableIterator {
    fn new(original_xsdt: &XSDT, table_count: usize) -> Self {
        let mut tables: Vec<*const SDTHeader> = Vec::new();
        tables.resize(table_count, 0 as *const SDTHeader);
        let tables_box = unsafe {
            align_pointer(ptr_after_end_of::<_, *const SDTHeader>(original_xsdt))
        };

        // SAFETY: we will be using the box only to copy the slice into the vec
        tables.copy_from_slice(unsafe {
            slice_from_raw_parts(
                tables_box.as_ref() as *const *const SDTHeader, table_count
            ).as_ref().unwrap()
        });
        Self {
            tables, current_table: 0,
        }
    }
}

impl Iterator for ExtendedTableIterator {
    type Item = ExtendedTableInfo;

    fn next(&mut self) -> Option<Self::Item> {
        let table_addr = *self.tables.get(self.current_table)?;
        if table_addr as Addr == 0_usize {
            return None
        }
        let header = MappedRef::<SDTHeader>::new(table_addr as Addr);
        self.current_table += 1;
        return Some(ExtendedTableInfo {
            header: header.clone(),
            phys_addr: table_addr as Addr
        })
    }
}


pub struct TableIterator {
    tables: Vec<*const SDTHeader>,
    current_table: usize,
}

impl TableIterator {
    fn new(original_xsdt: &XSDT, table_count: usize) -> Self {
        let mut tables: Vec<*const SDTHeader> = Vec::new();
        tables.resize(table_count, 0 as *const SDTHeader);
        let after_header_ptr = ptr_after_end_of::<_, *const SDTHeader>(original_xsdt);

        let tables_box = unsafe { align_pointer(after_header_ptr) };
        // SAFETY: we will be using the box only to copy the slice into the vec
        tables.copy_from_slice(unsafe {
            slice_from_raw_parts(
                tables_box.as_ref() as *const *const SDTHeader, table_count
            ).as_ref().unwrap()
        });
        Self {
            tables, current_table: 0,
        }
    }
}

impl Iterator for TableIterator {
    type Item = SDTHeader;

    fn next(&mut self) -> Option<Self::Item> {
        let table_addr = *self.tables.get(self.current_table)?;
        if table_addr as Addr == 0_usize {
            return None
        }
        let header = MappedRef::<SDTHeader>::new(table_addr as Addr);
        self.current_table += 1;
        return Some(header.clone())
    }
}

/**
 * Generic Address Structure used throughout ACPI
 */
#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct GAS {
    pub space_id: u8,
    pub bit_width: u8,
    pub bit_offset: u8,
    pub access_size: u8,
    pub address: u64,
}

pub trait HasSignature {
    fn signature(&self) -> String;
}

pub trait Validatable {
    fn validate(&self) -> Result<(), String>;
}

pub trait Verifiable {
    fn verify(&self) -> Result<(), String>;
}

