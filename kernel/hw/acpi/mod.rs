
use alloc::{sync::Arc, string::String};
use kernel_mem::MappedRef;
use klog::{Logger, libkernel_log::CommonLogger};
use tinystd::{types::Addr, formatting::PtrFormatted};
use tinystd_alloc::lateinit::LateInitArc;

use self::{descriptors::{RSDPv20, Validatable, XSDT, SDTHeader}, tables::fadt::FADT};

mod constants;
mod descriptors;
mod iter;
pub mod tables;

pub struct ACPI {
    logger: Logger,
    rsdp_descriptor: MappedRef<RSDPv20>,
    xsdt: LateInitArc<MappedRef<XSDT>>
}

impl ACPI {
    pub fn from(rsdp: Addr) -> Self {
        let logger = Logger::new("kernel/hw/acpi");
        logger.infof(format_args!("Initializing ACPI from RSDP {}", PtrFormatted(rsdp)));

        let new = Self {
            logger,
            rsdp_descriptor: MappedRef::new(rsdp),
            xsdt: LateInitArc::new(),
        };

        if let Err(msg) = new.rsdp_descriptor.validate() {
            new.logger.errorf(format_args!("Validation of RSDP failed: {}", msg));
            panic!("invalid RSDP")
        }

        let xsdt = unsafe { new.rsdp_descriptor.map_xsdt() };
        if let Err(msg) = xsdt.validate() {
            new.logger.errorf(format_args!("Validation of XSDT failed: {}", msg));
            panic!("invalid XSDT")
        }
        new.xsdt.set(new.rsdp_descriptor.remap_xsdt_with_correct_size(xsdt));

        new.logger.infof(format_args!("ACPI is ready with RSDP {}", PtrFormatted(new.rsdp_descriptor.addr())));
        new.print_info();
        new
    }

    fn xsdt(&self) -> Arc<MappedRef<XSDT>> {
        self.xsdt.get()
    }

    pub fn print_info(&self) {
        self.xsdt().iter_tables().for_each(|sdt| {
            self.logger.infof(format_args!(
                "Table: {} (OEM Table ID: {}, OEM ID: {})", sdt.signature(), sdt.oem_table_id(), sdt.oem_id(),
            ))
        });

        if let Some(fadt) = self.table::<FADT>(None) {
            self.logger.infof(format_args!(
                "Preferred Power Management Profile: {}", fadt.preferred_pm_profile()
            ))
        }
    }

    pub fn table<T : Table + Clone + 'static>(&self, index: Option<usize>) -> Option<T> {
        let index = index.unwrap_or(0);
        let table_info = self.xsdt()
            .iter_tables_extended()
            .filter(|info| info.header.signature() == T::expect_signature())
            .nth(index)?;
        unsafe {
            MappedRef::<T>::from(table_info.phys_addr, table_info.header.size())
        }.and_then(|m| Some(m.clone()) )
    }

    pub fn table_ref<T : Table + 'static>(&self, index: Option<usize>) -> Option<MappedRef<T>> {
        let index = index.unwrap_or(0);
        let table_info = self.xsdt()
            .iter_tables_extended()
            .filter(|info| info.header.signature() == T::expect_signature())
            .nth(index)?;
        unsafe {
            MappedRef::<T>::from(table_info.phys_addr, table_info.header.size())
        }
    }
}

pub trait Table {
    fn header(&self) -> SDTHeader;
    fn signature(&self) -> String { self.header().signature() }
    fn expect_signature() -> &'static str;
}
