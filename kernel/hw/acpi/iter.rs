use core::{ptr::slice_from_raw_parts, mem::size_of, marker::PhantomData};

use alloc::vec::Vec;
use tinystd::mem::{ptr_after_end_of, addr_after_end_of};

use super::{Table, descriptors::SDTHeader};

pub struct VariableEntryIterator<T : Copy + 'static> {
    entries: Vec<T>,
    current_entry: usize,
}

impl<T : Copy + 'static> VariableEntryIterator<T> {
    pub fn new(first_entry: &'static T, entry_count: usize) -> Self {
        unsafe { Self::new_from_ptr(first_entry as *const T, entry_count) }
    }

    pub unsafe fn new_from_ptr(first_entry: *const T, entry_count: usize) -> Self {
        let entries = unsafe {
            slice_from_raw_parts(first_entry, entry_count).as_ref().unwrap()
        }.to_vec();
        Self {
            entries, current_entry: 0,
        }
    }

    pub unsafe fn new_after_end_of<V>(value: &V, entry_count: usize) -> Self {
        Self::new_from_ptr(ptr_after_end_of(value), entry_count)
    }

    pub unsafe fn from<TBL : Table>(table: &TBL) -> Self {
        Self::new_from_ptr(
            ptr_after_end_of::<TBL, T>(table),
            (table.header().size() - size_of::<SDTHeader>()) / size_of::<T>()
        )
    }
}

impl<T : Copy + 'static> Iterator for VariableEntryIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        let entry = self.entries.get(self.current_entry)?;
        self.current_entry += 1;
        Some(*entry)
    }
}

pub trait VariableLengthEntryDeterminator<'tbl, DT : 'static> {
    fn determine_length(of: &'tbl DT) -> usize;
}

pub struct VariableLengthEntryIterator<'tbl, DT : 'static, For : VariableLengthEntryDeterminator<'tbl, DT> + Table> {
    table: &'tbl For,
    next_offset: usize,
    _phantom_dt: PhantomData<DT>,
}

impl<
    'tbl, DT : 'static, For : VariableLengthEntryDeterminator<'tbl, DT> + Table
> VariableLengthEntryIterator<'tbl, DT, For> {
    pub fn from(table: &'tbl For) -> Self {
        Self {
            table,
            next_offset: 0,
            _phantom_dt: PhantomData::default(),
        }
    }
}

impl<'tbl, DT : 'static, For : VariableLengthEntryDeterminator<'tbl, DT> + Table> Iterator
for VariableLengthEntryIterator<'tbl, DT, For> {
    type Item = &'tbl DT;

    fn next(&mut self) -> Option<Self::Item> {
        let max_size = self.table.header().size() - size_of::<For>();
        if self.next_offset >= max_size {
            return None;
        }
        let start_addr = addr_after_end_of::<For>(self.table);
        let next_addr = start_addr + self.next_offset;
        let item = unsafe { (next_addr as *const DT).as_ref() }.unwrap();
        self.next_offset += For::determine_length(item);
        Some(item)
    }

}
