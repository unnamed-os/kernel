
pub const RSDP_SIGNATURE: &str = "RSD PTR ";

pub const VERSION: u32 = 20;
pub const OEM_ID_LEN: usize = 6;
pub const RSDP_SIGNATURE_LEN: usize = 8;
pub const SDT_SIGNATURE_LEN: usize = 4;
pub const OEM_TABLE_ID_LEN: usize = 8;

pub const XSDT_SIGNATURE: &str = "XSDT";


/*
 * ACPI Generic Address Structure (GAS) AddressSpace
 */

pub const GAS_ADDRESS_SPACE_SYSTEM_MEMORY: usize = 0;
pub const GAS_ADDRESS_SPACE_SYSTEM_IO: usize = 1;
pub const GAS_ADDRESS_SPACE_PCI_CONFIG_SPACE: usize = 2;
pub const GAS_ADDRESS_SPACE_EMBEDDED_CONTROLLER: usize = 3;
pub const GAS_ADDRESS_SPACE_SMBUS: usize = 4;
pub const GAS_ADDRESS_SPACE_SYSTEM_CMOS: usize = 5;
pub const GAS_ADDRESS_SPACE_PCI_BAR_TARGET: usize = 6;
pub const GAS_ADDRESS_SPACE_IPMI: usize = 7;
pub const GAS_ADDRESS_SPACE_GENERAL_PURPOSE_IO: usize = 8;
pub const GAS_ADDRESS_SPACE_GENERIC_SERIAL_BUS: usize = 9;
pub const GAS_ADDRESS_SPACE_PLATFORM_COMMUNICATION_CHANNEL: usize = 0x0A;
// 0x0B-0x7F reserved
// 0x80-0xFF OEM defined

/*
* ACPI Generic Address Structure (GAS) AccessSize
*/
pub const GAS_ACCESS_SIZE_UNDEFINED: u8 = 0; // legacy reasons
pub const GAS_ACCESS_SIZE_BYTE: u8 = 1;
pub const GAS_ACCESS_SIZE_16BIT: u8 = 2;
pub const GAS_ACCESS_SIZE_32BIT: u8 = 3;
pub const GAS_ACCESS_SIZE_64BIT: u8 = 4;
pub const GAS_ACCESS_SIZE_WORD: u8 = GAS_ACCESS_SIZE_16BIT;
pub const GAS_ACCESS_SIZE_DWORD: u8 = GAS_ACCESS_SIZE_32BIT;
pub const GAS_ACCESS_SIZE_QWORD: u8 = GAS_ACCESS_SIZE_64BIT;
pub const GAS_ACCESS_SIZE_END: u8 = 5;
