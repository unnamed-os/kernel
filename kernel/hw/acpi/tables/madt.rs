use core::{marker::PhantomData, fmt::Display, mem::{self, size_of}};

use tinystd::types::Addr;

use crate::acpi::{descriptors::SDTHeader, Table, iter::{VariableLengthEntryDeterminator, VariableLengthEntryIterator}};

const SIGNATURE: &str = "APIC";

#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct MADT {
    header: SDTHeader,
    local_apic_address: u32,
    flags: u32,
    // variable-length entries follow
}

impl Table for MADT {
    fn header(&self) -> SDTHeader { self.header }
    fn expect_signature() -> &'static str { SIGNATURE }
}

impl<'tbl> VariableLengthEntryDeterminator<'tbl, RecordHeader> for MADT {
    fn determine_length(of: &'tbl RecordHeader) -> usize {
        of.record_length as usize
    }
}

pub struct MADTEntryIterator<'tbl, E : Entry> {
    entry_iter: VariableLengthEntryIterator<'tbl, RecordHeader, MADT>,
    _phantom_e: PhantomData<E>,
}

impl<'tbl : 'static, E : Entry> Iterator for MADTEntryIterator<'tbl, E> {
    type Item = E;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(next_entry) = self.entry_iter.next() {
            if next_entry.entry_type == E::record_type() as u8 {
                return Some(next_entry.as_entry::<E>().clone())
            }
        }
        None
    }
}

impl MADT {
    pub fn iter<'tbl : 'static, E : Entry>(&'tbl self) -> MADTEntryIterator<'tbl, E> {
        MADTEntryIterator {
            entry_iter: VariableLengthEntryIterator::from(self),
            _phantom_e: PhantomData::default()
        }
    }
}

#[repr(u8)]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum RecordType {
    LocalAPIC = 0,
    IOAPIC = 1,
    IOAPICInterruptSourceOverride = 2,
    IOAPICNMISource = 3,
    LocalAPICNMI = 4,
    LocalAPICAddressOverride = 5,
    IOSAPIC = 6,
    LocalSAPIC = 7,
    PlatformInterruptSource = 8,
    LocalX2APIC = 9,
    X2APICNMI = 10,
    GICC = 11,
    GICD = 12,
    GICMSIFrame = 13,
    GICRedistributor = 14,
    GICInterruptTranslationService = 15,
    MPWakeup = 16,
    Unknown = 255,
}

const RECORD_TYPE_FIRST: RecordType = RecordType::LocalAPIC;
const RECORD_TYPE_LAST: RecordType = RecordType::MPWakeup;

impl TryFrom<u8> for RecordType {
    type Error = &'static str;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        if value >= RECORD_TYPE_FIRST as u8 && value <= RECORD_TYPE_LAST as u8 {
            Ok(unsafe { mem::transmute_copy(&value) })
        } else {
            Err("invalid value")
        }
    }
}

#[repr(C,packed)]
#[derive(Copy, Clone, Debug)]
pub struct RecordHeader {
    entry_type: u8, // This is RecordType but since it could contain ANY number, we will keep this as u8 and convert
    record_length: u8,
}

impl Display for RecordHeader {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!(
            "RecordHeader {{ type: {:?}, length: {} }}",
            self.entry_type.try_into().unwrap_or(RecordType::Unknown),
            self.record_length
        ))
    }
}

impl RecordHeader {
    pub fn as_entry<'e, E : Entry>(&'e self) -> &'e E {
        unsafe { (((self as *const Self as Addr) + size_of::<RecordHeader>()) as *const E).as_ref() }.unwrap()
    }
}

pub trait Entry : Copy + Clone + 'static {
    fn record_type() -> RecordType;
}

#[repr(C,packed)]
#[derive(Copy, Clone, Debug)]
pub struct LocalX2APIC {
    _reserved: u16,
    x2apic_id: u32,
    flags: u32,
    acpi_id: u32
}

impl Entry for LocalX2APIC {
    fn record_type() -> RecordType { RecordType::LocalX2APIC }
}


#[repr(C,packed)]
#[derive(Copy, Clone, Debug)]
pub struct LocalAPIC {
    acpi_processor_id: u8,
    apic_id: u8,
    flags: u32,
}

impl Entry for LocalAPIC {
    fn record_type() -> RecordType { RecordType::LocalAPIC }
}

#[derive(Copy, Clone, Debug)]
pub struct LocalAPICFlags {
    pub processor_enabled: bool,
    pub online_capable: bool,
}

impl Into<u32> for LocalAPICFlags {
    fn into(self) -> u32 {
        0_u32 |
            (self.processor_enabled as u32) << 0 |
            (self.online_capable as u32) << 1
    }
}

impl Display for LocalAPICFlags {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!(
            "LocalAPICFlags {{ enabled: {}, online capable: {} }}",
            self.processor_enabled, self.online_capable
        ))
    }
}

impl LocalAPIC {
    pub fn flags(&self) -> LocalAPICFlags {
        LocalAPICFlags {
            processor_enabled: self.flags & 0x1 == 1,
            online_capable: self.flags & 0x2 == 1,
        }
    }
}

impl Display for LocalAPIC {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!(
            "{{ ACPI Processor ID: {}, APIC ID: {}, {} }}",
            self.acpi_processor_id, self.apic_id, self.flags()
        ))
    }
}
