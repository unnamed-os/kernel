
// FADT = Fixed ACPI Description Table

use core::fmt::Display;

use alloc::{string::String, format};

use crate::acpi::{descriptors::{SDTHeader, GAS, Validatable}, Table, constants::GAS_ACCESS_SIZE_END};

pub const SIGNATURE: &str = "FACP";

#[derive(Debug)]
pub enum PowerManagementProfile {
    Unspecified = 0,
    Desktop,
    Mobile,
    Workstation,
    EnterpriseServer,
    SOHOServer,
    AppliancePC,
    PerformanceServer,
}

impl Display for PowerManagementProfile {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str(match self {
            Self::Unspecified => "Unspecified",
            Self::Desktop => "Desktop",
            Self::Mobile => "Mobile",
            Self::Workstation => "Workstation",
            Self::EnterpriseServer => "Enterprise Server",
            Self::SOHOServer => "SOHO Server",
            Self::AppliancePC => "Appliance PC",
            Self::PerformanceServer => "Performance Server",
        })
    }
}

impl From<u8> for PowerManagementProfile {
    fn from(value: u8) -> Self {
        match value {
            0 => Self::Unspecified,
            1 => Self::Desktop,
            2 => Self::Mobile,
            3 => Self::Workstation,
            4 => Self::EnterpriseServer,
            5 => Self::SOHOServer,
            6 => Self::AppliancePC,
            7 => Self::PerformanceServer,
            _ => Self::Unspecified
        }
    }
}

#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct FADT {
    header: SDTHeader,

    firmware_ctrl: u32,
    dsdt: u32,

    // was used in ACPI 1.0 and is no longer in use
    reserved: u8,

    // Preferred power management profile
    preferred_pm_profile: u8,
    sci_interrupt: u16,
    smi_command_port: u32,
    acpi_enable: u8,
    acpi_disable: u8,
    s4bios_req: u8,
    pstate_control: u8,
    pm1a_event_blk: u32,
    pm1b_event_blk: u32,
    pm1a_control_blk: u32,
    pm1b_control_blk: u32,
    pm2_control_blk: u32,
    pm_timer_blk: u32,
    gpe0_blk: u32,
    gpe1_blk: u32,
    pm1_event_len: u8,
    pm1_control_len: u8,
    pm2_control_len: u8,
    pm_timer_len: u8,
    gpe0_len: u8,
    gpe1_len: u8,
    gpe1_base: u8,
    cstate_control: u8,
    // worst case hardware latency to enter/exit C2 state
    worst_c2_latency: u16,
    // worst case hardware latency to enter/exit C3 state
    worts_c3_latency: u16,
    // flush size of processor's memory cache
    flush_size: u16,
    // flush stride of processor's memory cache
    flush_stride: u16,
    // duty cycle offset in processor's P_CNT register
    duty_offset: u8,
    // duty cycle width in processor's P_CNT register
    duty_width: u8,
    // day-alarm index in RTC CMOS
    day_alarm: u8,
    // month-alarm index in RTC CMOS
    month_alarm: u8,
    // century index in RTC CMOS
    century: u8,

    boot_architecture_flags: u16,

    reserved2: u8,
    flags: u32,

    reset_reg: GAS,

    reset_value: u8,
    reserved3: [u8; 3],

    // 64-bit pointers, only valid if ACPI 2.0+ is supported
    x_firmware_ctrl: u64,
    x_dsdt: u64,

    x_pm1a_event_blk: GAS,
    x_pm1b_event_blk: GAS,
    x_pm1a_control_blk: GAS,
    x_pm1b_control_blk: GAS,
    x_pm2_control_blk: GAS,
    x_pm_timer_blk: GAS,
    x_gpe0_blk: GAS,
    x_gpe1_blk: GAS,

}

impl Table for FADT {
    fn header(&self) -> SDTHeader { self.header }
    fn expect_signature() -> &'static str { SIGNATURE }
}

impl Validatable for FADT {
    fn validate(&self) -> Result<(), String> {
        if self.reset_reg.access_size >= GAS_ACCESS_SIZE_END {
            return Err(format!(
                "invalid reset register access size: {} (max: {})",
                self.reset_reg.access_size, GAS_ACCESS_SIZE_END
            ))
        }
        Ok(())
    }
}

impl FADT {
    pub fn preferred_pm_profile(&self) -> PowerManagementProfile {
        self.preferred_pm_profile.into()
    }
}
