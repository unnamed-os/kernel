use core::fmt::{Debug, Display};

use tinystd::formatting::PtrFormatted;

use crate::acpi::{descriptors::SDTHeader, Table, iter::VariableEntryIterator};

const SIGNATURE: &str = "MCFG";

#[repr(C,packed)]
#[derive(Copy, Clone)]
pub struct MCFG {
    header: SDTHeader,
    _reserved: u64,
}

impl Table for MCFG {
    fn header(&self) -> SDTHeader { self.header }
    fn expect_signature() -> &'static str { SIGNATURE }
}

#[repr(C,packed)]
#[derive(Copy, Clone, Debug)]
pub struct Entry {
    pub base_address: u64,
    pub pci_segment_group: u16,
    pub start_bus_number: u8,
    pub end_bus_number: u8,
    _reserved: u32,
}

impl Display for Entry {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let &Entry {
            pci_segment_group,
            start_bus_number,
            end_bus_number,
            ..
        } = self;
        f.write_fmt(format_args!(
            "MCFG Entry {{ base_address: {}, pci_segment_group: {}, start_bus_number: {}, end_bus_number: {} }}",
            PtrFormatted(self.base_address), pci_segment_group, start_bus_number, end_bus_number
        ))
    }
}

impl MCFG {
    pub fn iter_entries(&'static self) -> VariableEntryIterator<Entry> {
        unsafe { VariableEntryIterator::from(self) }
    }
}