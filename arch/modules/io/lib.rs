#![no_std]

pub mod serial {
    use rust_macros::{static_asserts::export_assert_fn, export_impl_all};

    export_impl_all!(kernel_native_io_impl::serial::Serial: kernel_native_defs::io::SerialController);
    export_assert_fn!(kernel_native_io_impl::serial::write_serial: fn(&str) -> Result<(), ()>);
}

