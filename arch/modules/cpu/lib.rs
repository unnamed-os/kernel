#![no_std]

use rust_macros::static_asserts::{
    export_impl_all,
    export_assert_fn
};

export_impl_all!(kernel_native_cpu_impl::CPU: kernel_native_defs::cpu::CPU);
export_assert_fn!(kernel_native_cpu_impl::cur_cpu: fn() -> CPU);

