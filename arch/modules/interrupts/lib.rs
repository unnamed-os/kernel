#![no_std]

use kernel_native_defs::interrupts::InterruptsTrait;
use rust_macros::static_asserts::{
    export_impl_all,
};

export_impl_all!(kernel_native_interrupts_impl::Interrupts: kernel_native_defs::interrupts::InterruptsTrait);

pub struct InterruptDisableGuard {
    were_enabled: bool,
}

pub fn disable_interrupts() -> InterruptDisableGuard {
    let guard = InterruptDisableGuard { were_enabled: Interrupts::are_interrupts_enabled() };
    Interrupts::disable_interrupts();
    guard
}

pub fn guard_interrupts_disabled<F>(f: F) where F : FnOnce() {
    let disabled_interrupts = disable_interrupts();
    f();
    drop(disabled_interrupts);
}

impl Drop for InterruptDisableGuard {
    fn drop(&mut self) {
        if self.were_enabled {
            Interrupts::enable_interrupts();
        }
    }
}

pub struct InterruptEnableGuard {
    were_enabled: bool,
}

pub fn enable_interrupts() -> InterruptEnableGuard {
    let guard = InterruptEnableGuard { were_enabled: Interrupts::are_interrupts_enabled() };
    Interrupts::enable_interrupts();
    guard
}

impl Drop for InterruptEnableGuard {
    fn drop(&mut self) {
        if !self.were_enabled {
            Interrupts::disable_interrupts();
        }
    }
}

pub fn guard_interrupts_enabled<F>(f: F) where F : FnOnce() {
    let enabled_interrupts = enable_interrupts();
    f();
    drop(enabled_interrupts);
}
