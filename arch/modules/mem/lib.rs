#![no_std]

use rust_macros::{static_asserts::export_impl_all, export_assert_fn};

export_impl_all!(kernel_native_mem_impl::Memory:
    kernel_native_defs::mem::MemoryFuncs,
    kernel_native_defs::mem::PagingFuncs, // for now we require paging to be implemented
);
