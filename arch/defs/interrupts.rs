
pub trait InterruptsTrait : InterruptFuncs {
    fn new() -> Self;

    fn enable_interrupts() {}
    fn disable_interrupts() {}
    fn are_interrupts_enabled() -> bool { false }
    fn interrupt_disable_guard<R : Copy, F : FnOnce() -> R>(f: F) -> R {
        // Racey. Use with care.
        let were_interrupts_enabled = Self::are_interrupts_enabled();
        Self::disable_interrupts();
        let result = f();
        if were_interrupts_enabled {
            Self::enable_interrupts();
        }
        result
    }
}

pub trait InterruptFuncs {
    fn init_ap(&self, cpu_id: usize);
    fn init_bsp(&self);
    fn init_timer(&self, cpu_id: usize);
    fn runtime_init_bsp(&self);
}