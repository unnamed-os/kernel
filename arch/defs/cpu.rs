use core::fmt;

use alloc::string::String;

pub trait CPU {
    fn halt_execution(&self) -> !;
    fn wait_for_interrupt(&self);
    fn check_features(&self) -> Result<(), &'static str>;
    fn cpu_name(&self) -> String;
    fn format_cpu_specifics<F>(&self, use_format: F) where F : FnOnce(fmt::Arguments);
    fn cpu_id(&self) -> usize;

    /// If supported, executes the pause instruction
    #[inline(always)]
    fn spin_pause() {}
}
