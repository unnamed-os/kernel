
use core::ops::Range;

use libkernel::{boot_iface::KernelMemInfo, mem::MemoryProtectionFlags};
use tinystd::{types::Addr, alignment::align_up};

pub trait MemoryFuncs {
    fn init(&self, mem_info: KernelMemInfo) -> Result<(), &'static str>;
    fn free_memory(&self) -> usize;
    fn used_memory(&self) -> usize;
    fn total_memory(&self) -> usize;
    fn reserved_memory(&self) -> usize { 0 }

    fn kernel_virt_memory_range(&self) -> Range<Addr>;
    fn kernel_virt_stack_memory_range(&self) -> Range<Addr>;
}

pub trait PagingFuncs {
    /// If map_pages is not implemented, then map_page must guarantee
    /// that the page_sizes as specified by the page_sizes function
    /// are implemented.
    fn map_page(
        &self,
        phys_addr: Addr,
        virt_addr: Addr,
        page_size: usize,
        prot: MemoryProtectionFlags,
    ) -> Result<(), &'static str>;

    fn preferred_page_size(&self, needed_size: usize) -> Result<usize, &'static str> {
        let page_sizes = self.page_sizes();
        return if page_sizes.is_empty() {
            Err("Page sizes must contain at least one entry")
        } else {
            Ok(*page_sizes.iter()
                    .filter(|&&page_size| page_size <= needed_size)
                    .max()
                    .unwrap_or(page_sizes.get(0).unwrap()))
        }
    }

    fn map_pages(
        &self,
        phys_start: Addr,
        virt_start: Addr,
        prot: MemoryProtectionFlags,
        size: usize,
    ) -> Result<usize, &'static str> {
        let mut preferred_page_size = self.preferred_page_size(size)?;
        let mut for_offset = 0_usize;

        // If the physical and virtual address are unaligned to the preferred page size,
        // we will need to map the first part of them using a smaller page size if the
        // difference is the same. Otherwise there needs to be a fallback to the smaller page size.
        let mut aligned_phys_start_addr = align_up(phys_start, preferred_page_size);
        let mut aligned_virt_start_addr = align_up(virt_start, preferred_page_size);
        let mut phys_alignment_diff = aligned_phys_start_addr - phys_start;
        let mut virt_alignment_diff = aligned_virt_start_addr - virt_start;

        while virt_alignment_diff != 0 || phys_alignment_diff != 0 {
            if phys_alignment_diff == virt_alignment_diff {
                self.map_pages(
                    phys_start,
                    virt_start,
                    prot,
                    virt_alignment_diff
                )?;
                for_offset += virt_alignment_diff;
                break;
            } else {
                // We need to downgrade the preferred page size until it fits
                preferred_page_size = self.preferred_page_size(preferred_page_size - 1)?;
                aligned_phys_start_addr = align_up(phys_start, preferred_page_size);
                aligned_virt_start_addr = align_up(virt_start, preferred_page_size);
                phys_alignment_diff = aligned_phys_start_addr - phys_start;
                virt_alignment_diff = aligned_virt_start_addr - virt_start;
            }
        }

        for addr_offset in (for_offset..size).step_by(preferred_page_size) {
            self.map_page(
                phys_start + addr_offset,
                virt_start + addr_offset,
                preferred_page_size,
                prot,
            ).or(Err("could not map page as part of pages range"))?;
        }

        Ok(preferred_page_size)
    }

    fn unmap_page(&self, virt: Addr) -> Result<(), &'static str>;

    fn unmap_pages(
        &self,
        virt_start: Addr,
        size: usize,
    ) -> Result<(), &'static str>;

    fn allocate_frame(&self, page_size: usize) -> Result<Addr, &'static str>;

    fn allocate_frames_and_map(
        &self,
        virt_start: Addr,
        size: usize,
        prot: MemoryProtectionFlags,
    ) -> Result<usize, &'static str> {
        let preferred_page_size = self.preferred_page_size(size)?;
        for addr_offset in (0..size).step_by(preferred_page_size) {
            let page_frame = self.allocate_frame(preferred_page_size)
                .or(Err("could not allocate a page frame to use for mapping"))?;
            self.map_page(
                page_frame,
                virt_start + addr_offset,
                preferred_page_size,
                prot,
            ).or(Err("could not map page as part of pages range"))?;
        }

        Ok(preferred_page_size)
    }

    fn remap_region(
        &self,
        start_vaddr: Addr,
        end_vaddr: Addr,
        mem_prot: MemoryProtectionFlags,
        page_size: Option<usize>,
        name: &'static str,
    ) -> Result<(), &'static str>;

    fn page_sizes(&self) -> &[usize];

    fn init_paging(&self) -> Result<(), &'static str>;
}
