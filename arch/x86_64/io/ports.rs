use x86_64::instructions;

#[inline(always)]
pub unsafe fn outb(port: u16, val: u8) {
    instructions::port::PortWrite::write_to_port(port, val);
}

#[inline(always)]
pub unsafe fn inb(port: u16) -> u8 {
    instructions::port::PortRead::read_from_port(port)
}
