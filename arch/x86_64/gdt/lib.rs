#![no_std]

extern crate alloc;

use alloc::boxed::Box;
use alloc::collections::BTreeMap;
use alloc::sync::Arc;
use arch_x86_64_stack::{MiniStack, MiniHeapStack};
use spin::Mutex;
use tinystd_alloc::lateinit::LateInitArc;
use x86_64::VirtAddr;
use x86_64::instructions::tables::load_tss;
use x86_64::registers::segmentation::{CS, Segment, DS, SS, ES, GS, FS};
use x86_64::structures::gdt::{GlobalDescriptorTable, Descriptor, SegmentSelector};
use x86_64::structures::tss::TaskStateSegment;
use lazy_static::lazy_static;

pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;

struct GDTSelectors {
    code_selector: SegmentSelector,
    data_selector: SegmentSelector,
    tss_selector: SegmentSelector,
}

lazy_static! {
    static ref GLOBAL_TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            static STACK: MiniStack = MiniStack::new();
            let stack_start = VirtAddr::from_ptr(&STACK);
            let stack_end = stack_start + STACK.size();
            stack_end
        };
        tss
    };
}

lazy_static! {
    static ref GLOBAL_GDT: (GlobalDescriptorTable, GDTSelectors) = {
        let mut gdt = GlobalDescriptorTable::new();
        let selectors = GDTSelectors {
            code_selector: gdt.add_entry(Descriptor::kernel_code_segment()),
            data_selector: gdt.add_entry(Descriptor::kernel_data_segment()),
            tss_selector: gdt.add_entry(Descriptor::tss_segment(&GLOBAL_TSS))
        };
        (gdt, selectors)
    };
}

lazy_static! {
    static ref AP_GDTS: Mutex<BTreeMap<usize, (
        Arc<Mutex<GlobalDescriptorTable>>, LateInitArc<GDTSelectors>
    )>> =
        Mutex::new(BTreeMap::new());
}

pub struct GDT;

impl GDT {
    pub fn init() {
        GLOBAL_GDT.0.load();
        unsafe {
            CS::set_reg(GLOBAL_GDT.1.code_selector);
            DS::set_reg(GLOBAL_GDT.1.data_selector);
            ES::set_reg(GLOBAL_GDT.1.data_selector);
            FS::set_reg(GLOBAL_GDT.1.data_selector);
            GS::set_reg(GLOBAL_GDT.1.data_selector);
            SS::set_reg(GLOBAL_GDT.1.data_selector);
            load_tss(GLOBAL_GDT.1.tss_selector);
        }
    }

    pub fn init_ap(cpu_id: usize) {
        /*Logger::new("arch/x86_64/gdt").infof(format_args!(
            "Loading GDT for AP {}", cpu_id
        ));*/

        let gdt_arc = Arc::new(Mutex::new(GlobalDescriptorTable::new()));
        let mut ap_gdts = AP_GDTS.lock();
        ap_gdts.insert(cpu_id, (gdt_arc, LateInitArc::new()));

        let (gdt_mutex, selectors) = ap_gdts.get(&cpu_id).unwrap();
        let mut gdt = gdt_mutex.lock();

        let mut tss = Box::new(TaskStateSegment::new());
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            let stack = MiniHeapStack::create();
            VirtAddr::new(stack.suitable_rsp_addr() as u64)
        };

        selectors.set(GDTSelectors {
            code_selector: gdt.add_entry(Descriptor::kernel_code_segment()),
            data_selector: gdt.add_entry(Descriptor::kernel_data_segment()),
            tss_selector: gdt.add_entry(Descriptor::tss_segment(Box::leak(tss)))
        });

        unsafe {
            gdt.load_unsafe();
            CS::set_reg(selectors.get().code_selector);
            DS::set_reg(selectors.get().data_selector);
            ES::set_reg(selectors.get().data_selector);
            FS::set_reg(selectors.get().data_selector);
            GS::set_reg(selectors.get().data_selector);
            SS::set_reg(selectors.get().data_selector);
            load_tss(selectors.get().tss_selector);
        }
    }
}
