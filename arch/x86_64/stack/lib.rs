#![no_std]

extern crate alloc;

use core::{ptr::{addr_of, slice_from_raw_parts_mut}, mem::size_of};

use alloc::{vec, boxed::Box};
use tinystd::{sizes::KIB, types::Addr, alignment::align_down, formatting::PtrFormatted};

const STACK_SIZE: usize = 128 * KIB as usize;
const MINI_STACK_SIZE: usize = 4 * KIB as usize;

#[repr(C,align(0x1000))]
pub struct Stack([u8; STACK_SIZE]);

impl Stack {
    pub const fn new() -> Self {
        Self([0; STACK_SIZE])
    }

    pub unsafe fn at<'a>(addr: Addr) -> &'a mut Self {
        assert_eq!(addr % 0x1000, 0, "address {} must be aligned", PtrFormatted(addr));
        (addr as *mut Self).as_mut().unwrap()
    }

    pub fn zero(&mut self) {
        self.0.fill(0);
    }

    pub fn addr(&self) -> Addr {
        addr_of!(self.0) as Addr
    }

    /// This will "push" a null pointer onto the stack and align it properly
    fn suitable_rsp_addr(&self) -> Addr {
        align_down(self.addr() + self.size() - 1 - size_of::<Addr>(), size_of::<usize>())
    }

    pub fn setup(&mut self) -> Addr {
        let suitable_addr = self.suitable_rsp_addr();
        unsafe { *(suitable_addr as *mut Addr) = 0 };
        suitable_addr
    }

    pub const fn size(&self) -> usize {
        self.0.len()
    }
}


#[repr(C,align(0x1000))]
pub struct MiniStack([u8; MINI_STACK_SIZE]);

impl MiniStack {
    pub const fn new() -> Self {
        Self([0; MINI_STACK_SIZE])
    }

    pub fn zero(&mut self) {
        self.0.fill(0);
    }

    pub fn addr(&self) -> Addr {
        addr_of!(self.0) as Addr
    }

    pub fn suitable_rsp_addr(&self) -> Addr {
        align_down(self.addr() + self.size() - 16, size_of::<usize>())
    }

    pub const fn size(&self) -> usize {
        self.0.len()
    }
}

pub struct MiniHeapStack;

impl MiniHeapStack {
    pub fn create() -> KernelStack {
        let storage = Box::leak(vec![0_u8; MINI_STACK_SIZE].into_boxed_slice());
        unsafe { KernelStack::new(storage.as_ptr() as Addr, MINI_STACK_SIZE) }
    }
}

pub struct KernelStack {
    // be careful here. these two are the same thing.
    // this struct is expected to be only used in a very narrow codepath
    stack_slice: &'static mut [u8],
    stack_slice_usize: &'static mut [usize],
}

impl KernelStack {
    pub unsafe fn new(addr: Addr, size: usize) -> Self {
        Self {
            stack_slice: slice_from_raw_parts_mut(addr as *mut u8, size).as_mut().unwrap(),
            stack_slice_usize: slice_from_raw_parts_mut(addr as *mut usize, size).as_mut().unwrap(),
        }
    }

    pub fn zero(&mut self) {
        self.stack_slice_usize.fill(0);
    }

    pub fn addr(&self) -> Addr {
        self.stack_slice.as_ptr() as Addr
    }

    /// This will "push" a null pointer onto the stack and align it properly
    pub fn suitable_rsp_addr(&self) -> Addr {
        align_down(self.addr() + self.size() - 1 - size_of::<Addr>(), size_of::<usize>())
    }

    pub fn setup(&mut self) -> Addr {
        let suitable_addr = self.suitable_rsp_addr();
        unsafe { *(suitable_addr as *mut Addr) = 0 };
        suitable_addr
    }

    pub const fn size(&self) -> usize {
        self.stack_slice.len()
    }
}
