#![no_std]

extern crate alloc;

use core::{fmt, str::FromStr, arch::{asm, self}};

use alloc::string::String;
use x86::cpuid::CpuId;
use x86_64::{instructions, registers::debug};

pub struct CPU {}

impl kernel_native_defs::cpu::CPU for CPU {
    #[inline(always)]
    fn halt_execution(&self) -> ! {
        instructions::interrupts::disable();
        instructions::hlt();
        unreachable!()
    }

    #[inline(always)]
    fn wait_for_interrupt(&self) {
        instructions::interrupts::enable_and_hlt();
    }

    fn check_features(&self) -> Result<(), &'static str> {
        let cpuid = CpuId::new();
        if let Some(features) = cpuid.get_feature_info() {
            if !features.has_apic() {
                return Err("APIC not supported");
            }
        } else {
            return Err("CPUID not supported");
        }
        Ok(())
    }

    fn cpu_name(&self) -> String {
        let cpuid = CpuId::new();
        let brand = cpuid.get_processor_brand_string().unwrap();
        String::from_str(brand.as_str()).unwrap()
    }

    fn cpu_id(&self) -> usize {
        let cpuid = CpuId::new();
        cpuid.get_extended_topology_info_v2()
            .and_then(|mut i| Some(i.nth(0).unwrap().x2apic_id() as usize))
            .unwrap_or_else(|| cpuid.get_extended_topology_info().unwrap().nth(0).unwrap().x2apic_id() as usize)
    }

    fn format_cpu_specifics<F>(&self, use_format: F) where F : FnOnce(fmt::Arguments) {
        let cpuid = CpuId::new();
        let vendor_info = cpuid.get_vendor_info().unwrap();
        let feature_info = cpuid.get_feature_info().unwrap();
        let family = feature_info.family_id();
        let model = feature_info.model_id();
        let stepping = feature_info.stepping_id();
        let core_count = feature_info.max_logical_processor_ids();
        use_format(format_args!(
            "{core_count}x {brand} Family {family:#x} Model {model:#x} Stepping {stepping:#x}",
            brand=vendor_info.as_str(),
            family=family,
            model=model,
            stepping=stepping,
            core_count=core_count,
        ));
    }

    #[inline(always)]
    fn spin_pause() {
        unsafe { arch::x86_64::_mm_pause() }
    }

}

// TODO: this needs to be changed to support SMP
//       Currently it just returns some CPU

pub fn cur_cpu() -> CPU {
    CPU {}
}
