use core::{arch::asm, sync::atomic::{AtomicUsize, Ordering}, slice};

use arch_x86_64_gdt::GDT;
use arch_x86_64_stack::{Stack, KernelStack};
use kernel_core::boot::{submit_existing_cpu_stack, notify_boot_protocol_done, ExistingStackInfo, consume_new_cpu_stack, wait_for_heap};
use kernel_native_cpu::cur_cpu;
use kernel_native_defs::cpu::CPU;

pub use kernel_core;
use kernel_runtime::runtime;
use libkernel::boot_iface::{BootInterface, KernelMemInfo, KernelMemInfoArea, MemAreaUsage, default_mem_info_areas, BootFramebuffer};
use klog::{Logger, libkernel_log::CommonLogger};
use libkernel_graphics_defs::framebuffer::{VideoModeMasks, VideoChannelMask};
use limine::{
    limine_tag,
    LimineBootInfoRequest,
    LimineMemmapRequest,
    LimineRsdpRequest,
    LimineHhdmRequest,
    LimineKernelAddressRequest,
    LimineMemoryMapEntryType,
    LimineMemmapEntry, LimineFramebufferRequest, LimineSmpRequest, LimineSmpInfo, LimineStackSizeRequest
};
use spin::Mutex;
use tinystd::{types::Addr, alignment::align_up, formatting::PtrFormatted};
use tinystd_sync::CountWaiter;
use x86_64::{registers::control::{Cr3, Cr3Flags}, structures::paging::PhysFrame, PhysAddr, instructions::tlb};

const PAGE_SIZE: usize = 0x1000;
const TEMP_STACK_SIZE: usize = PAGE_SIZE;

#[limine_tag]
static BOOT_INFO_REQUEST: LimineBootInfoRequest = LimineBootInfoRequest::new(0);

#[limine_tag]
static MEMMAP_REQUEST: LimineMemmapRequest = LimineMemmapRequest::new(0);

#[limine_tag]
static RSDP_REQUEST: LimineRsdpRequest = LimineRsdpRequest::new(0);

#[limine_tag]
static HHDM_REQUEST: LimineHhdmRequest = LimineHhdmRequest::new(0);

#[limine_tag]
static KERNEL_ADDRESS_REQUEST: LimineKernelAddressRequest = LimineKernelAddressRequest::new(0);

#[limine_tag]
static FRAMEBUFFER_REQUEST: LimineFramebufferRequest = LimineFramebufferRequest::new(0);

#[limine_tag]
static SMP_REQUEST: LimineSmpRequest = LimineSmpRequest::new(0);

#[limine_tag]
static STACK_SIZE_REQUEST: LimineStackSizeRequest = {
    let mut request = LimineStackSizeRequest::new(0);
    request.stack_size = TEMP_STACK_SIZE as u64;
    request
};

static STACK: Mutex<Stack> = Mutex::new(Stack::new());

#[inline(always)]
fn read_rsp() -> u64 {
    let value: u64;
    unsafe {
        asm!("mov {}, rsp", out(reg) value, options(nomem, nostack, preserves_flags));
    }
    value
}

#[inline(always)]
fn write_rsp(value: u64) {
    unsafe {
        asm!("mov rsp, {}", in(reg) value, options(nomem, nostack, preserves_flags))
    };
}

#[inline(always)]
fn write_rbp(value: u64) {
    unsafe {
        asm!("mov rbp, {}", in(reg) value, options(nomem, nostack, preserves_flags))
    };
}

#[no_mangle]
unsafe extern "C" fn native_x86_64_start() -> ! {
    {
        let mut stack = STACK.lock();
        // Switch to the fresh stack in bss
        write_rsp(stack.setup() as u64);
        // ↓ stack lock is dropped here
    }
    // ↑ stack lock went out of scope and thus got unlocked.
    // Write the rbp to make sure the stack frames end here.
    write_rbp(0);

    // Call the actual function. since some references will be relative
    // to rbp, it needs to be set to something.
    // Thus, this is trampoline code.
    native_x86_64_start_kernel();
}

static CPU_INIT_WAITER: CountWaiter = CountWaiter::new(0);

unsafe fn native_x86_64_start_kernel() -> ! {
    let stack_start = read_rsp();

    GDT::init();

    let logger = Logger::new("x86_64/boot/limine");

    if let Some(boot_info) = BOOT_INFO_REQUEST.get_response().get() {
        logger.infof(format_args!(
            "Landed in x86-64 by {} {}",
            boot_info.name.to_str().unwrap().to_str().unwrap(),
            boot_info.version.to_str().unwrap().to_str().unwrap(),
        ));
    } else {
        logger.fatal("Landed by unknown bootloader. Cannot boot.");
        cur_cpu().halt_execution();
    }

    if let Err(msg) = cur_cpu().check_features() {
        logger.fatalf(format_args!("{}. Cannot boot.", msg));
        cur_cpu().halt_execution();
    }

    cur_cpu().format_cpu_specifics(|args| logger.infof(args));

    let kernel_addr_info = KERNEL_ADDRESS_REQUEST.get_response().get().unwrap();
    let phys_base = kernel_addr_info.physical_base;
    let virt_base = kernel_addr_info.virtual_base;

    let hhdm = HHDM_REQUEST.get_response().get().unwrap().offset;
    let stack_size = {
        let stack = STACK.lock();
        stack.size()
    };

    let mut mem_info = KernelMemInfo {
        mem_areas: default_mem_info_areas(),
        page_size: PAGE_SIZE,
        stack_size,
        stack_start: stack_start as usize,
        phys_base: phys_base as usize,
        virt_base: virt_base as usize,
        hhdm: hhdm as usize,
    };

    let rsdp_addr = RSDP_REQUEST.get_response().get().unwrap().address.as_ptr().unwrap() as Addr;
    let memmap = MEMMAP_REQUEST.get_response().get().unwrap();

    memmap.memmap().iter()
        .map(|entry| entry.to_kernel_mem_info_area())
        .enumerate()
        .for_each(|(index, entry)| mem_info.mem_areas[index] = entry);

    let smp = SMP_REQUEST.get_response().get().unwrap();
    logger.infof(format_args!("BSP Local APIC ID: {}", smp.bsp_lapic_id));
    logger.infof(format_args!("CPU count: {}", smp.cpu_count));

    CPU_INIT_WAITER.set_count(smp.cpu_count as usize);

    let cpu_count = smp.cpu_count as usize;
    let cpus = unsafe { slice::from_raw_parts(smp.cpus.as_ptr(), cpu_count) };
    cpus.iter().for_each(|cpu| {
        if cpu.lapic_id == smp.bsp_lapic_id {
            return;
        }
        logger.infof(format_args!("Starting CPU {}", cpu.lapic_id));
        let atomic_goto = unsafe {
            (&cpu.goto_address as *const extern "C" fn(*const LimineSmpInfo) -> ! as Addr as *mut AtomicUsize).as_mut()
        }.unwrap();
        atomic_goto.store(land_ap as Addr, Ordering::Relaxed);
    });

    let mut boot_iface = BootInterface {
        acpi_rsdp: rsdp_addr - hhdm as Addr,
        meminfo: mem_info,
        fb: None,
        preboot_smp: true,
        ap_cpu_count: Some(cpu_count as usize),
    };

    fill_framebuffer_info(&mut boot_iface);

    if cpu_count <= 1 {
        logger.info("Uniprocessor system, not starting APs");
        notify_boot_protocol_done();
    } else {
        CPU_INIT_WAITER.decrement();
    }
    kernel_core::init::kernel_main(&boot_iface)
}

fn fill_framebuffer_info(boot_iface: &mut BootInterface) {
    if let Some(fb_info) = FRAMEBUFFER_REQUEST.get_response().get() {
        if fb_info.framebuffer_count > 0 {
            let framebuffer = unsafe { fb_info.framebuffers.as_ptr().as_ref() }.unwrap();
            boot_iface.fb = Some(
                BootFramebuffer {
                    phys_address: framebuffer.address.as_ptr().unwrap() as Addr - boot_iface.meminfo.hhdm,
                    width: framebuffer.width as i32,
                    height: framebuffer.height as i32,
                    pitch: framebuffer.pitch as usize,
                    bpp: framebuffer.bpp as u16,
                    masks: VideoModeMasks {
                        red_mask: VideoChannelMask {
                            shift: framebuffer.red_mask_shift,
                            size: framebuffer.red_mask_size
                        },
                        green_mask: VideoChannelMask {
                            shift: framebuffer.green_mask_shift,
                            size: framebuffer.green_mask_size
                        },
                        blue_mask:  VideoChannelMask {
                            shift: framebuffer.blue_mask_shift,
                            size: framebuffer.blue_mask_size
                        },
                    }
                }
            )
        }
    }
}

trait LimineMemoryMapEntryExtensions {
    fn to_kernel_mem_info_area(&self) -> KernelMemInfoArea;
}

impl LimineMemoryMapEntryExtensions for LimineMemmapEntry {
    fn to_kernel_mem_info_area(&self) -> KernelMemInfoArea {
        KernelMemInfoArea {
            phys_start: self.base as Addr,
            size_bytes: self.len as Addr,
            description: Some(self.typ.to_str()),
            usage: self.typ.usage(),
        }
    }
}

trait LimineMemoryMapEntryTypeExtensions {
    fn to_str(self) -> &'static str;
    fn usage(self) -> MemAreaUsage;
}

impl LimineMemoryMapEntryTypeExtensions for LimineMemoryMapEntryType {
    fn to_str(self) -> &'static str {
        match self {
            LimineMemoryMapEntryType::AcpiNvs => "acpi-nvs",
            LimineMemoryMapEntryType::AcpiReclaimable => "acpi-reclaimable",
            LimineMemoryMapEntryType::BadMemory => "bad-mem",
            LimineMemoryMapEntryType::BootloaderReclaimable => "bl-reclaimable",
            LimineMemoryMapEntryType::Framebuffer => "framebuffer",
            LimineMemoryMapEntryType::KernelAndModules => "kernel-and-modules",
            LimineMemoryMapEntryType::Reserved => "reserved",
            LimineMemoryMapEntryType::Usable => "usable",
        }
    }

    fn usage(self) -> MemAreaUsage {
        match self {
            LimineMemoryMapEntryType::AcpiReclaimable |
            LimineMemoryMapEntryType::BootloaderReclaimable |
            LimineMemoryMapEntryType::Usable => MemAreaUsage::Usable,

            LimineMemoryMapEntryType::AcpiNvs |
            LimineMemoryMapEntryType::Reserved |
            LimineMemoryMapEntryType::Framebuffer => MemAreaUsage::Reserved,

            LimineMemoryMapEntryType::KernelAndModules => MemAreaUsage::Used,
            LimineMemoryMapEntryType::BadMemory => MemAreaUsage::Bad,
        }
    }

}

pub extern "C" fn land_ap(smp_info: &LimineSmpInfo) -> ! {
    let rsp = read_rsp() as Addr;
    let &LimineSmpInfo {
        lapic_id,
        ..
    } = smp_info;

    let logger = Logger::new("x86_64/limine/smp-landing-pad");
    logger.infof(format_args!("CPU {} landed, rsp {}", lapic_id, PtrFormatted(rsp)));
    // First we submit the current stack to the kernel so it can add
    // it to the new memory map.
    submit_existing_cpu_stack(ExistingStackInfo {
        stack_end: align_up(rsp, TEMP_STACK_SIZE),
        stack_size: TEMP_STACK_SIZE,
        cpu_id: lapic_id as usize,
    });
    CPU_INIT_WAITER.decrement();
    CPU_INIT_WAITER.wait();
    notify_boot_protocol_done();

    // By now the kernel has loaded new page tables for the BSP. We also need them for the APs. Wait for it.
    unsafe {
        // TODO: One aspect we missed here is that the APs need to be notified when the TLB needs to be flushed
        //       This would probably be done using an interrupt of some sort
        //       I could imagine sending an interrupt that makes all the APs stop the work, synchronize and wait
        //       for the page tables to be modified. After the modification is complete, the APs will receive
        //       the page to be flushed or a request to flush the entire TLB. When this request is sent, all
        //       cores can desynchronize, do the flush and return to the work they were doing.
        //       This means we will have to avoid flushes as much as possible – or rely on page faults to trigger
        //       those flushes. Maybe it would be enough to send the interrupt, but not synchronize, and rather just
        //       set a flag to true to indicate that on a page fault, a flush should be attempted before continuing.
        //       But this would, in turn, make page faults slower.
        //       Also, when implementing those interrupts, interrupts have to be disabled during their execution.
        /*Cr3::write(PhysFrame::from_start_address(
            PhysAddr::new(wait_for_p4() as u64)
        ).unwrap(), Cr3Flags::empty())*/
    };

    wait_for_heap();

    GDT::init_ap(lapic_id as usize);

    // We will wait for the kernel to give us a new stack address that is mapped into virtual memory
    // We will get a stack start address and its size.
    let new_stack = consume_new_cpu_stack();
    // The new stack has been mapped into memory. We need to reload the page tables to make the change effective.
    tlb::flush_all();
    let mut kernel_stack = unsafe { KernelStack::new(new_stack.stack_start, new_stack.stack_size) };

    /*logger.infof(format_args!(
        "Received new stack address from kernel: {}, size: {}, suitable rsp: {}",
        tinystd::formatting::PtrFormatted(new_stack.stack_start),
        tinystd::sizes::BinarySize::from(new_stack.stack_size).rounded(),
        tinystd::formatting::PtrFormatted(kernel_stack.suitable_rsp_addr())
    ));*/

    // Set the rsp and rbp
    write_rsp(kernel_stack.setup() as u64);

    runtime().join_ap()
}
