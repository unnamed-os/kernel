use libkernel::boot_iface::KernelMemInfo;
use klog::{Logger, libkernel_log::CommonLogger};
use spin::RwLock;
use tinystd::{sizes::BinarySize, alignment::align_up};
use x86_64::{PhysAddr, structures::paging::{Size1GiB, PageSize, Size2MiB, Size4KiB}};

pub(super) trait FrameAllocator {
    fn allocate_frame(&self, page_size: usize) -> Result<PhysAddr, &'static str>;
    fn return_frame(&self, frame: PhysAddr, page_size: usize) -> Result<(), &'static str>;
    fn used_size(&self) -> usize;
}

struct BumpAllocatorState {
    next_offset: u64,
    current_area: usize,
}

impl BumpAllocatorState {
    const fn new() -> Self {
        Self {
            next_offset: 0,
            current_area: 0,
        }
    }
}

pub(super) struct BumpAllocator {
    state: RwLock<BumpAllocatorState>,
    mem_info: KernelMemInfo,
    logger: Logger,
}

impl BumpAllocator {
    pub(super) fn new(mem_info: KernelMemInfo) -> Self {
        Self {
            state: RwLock::new(BumpAllocatorState::new()),
            mem_info,
            logger: Logger::new("x86_64/mem/phys_bump_alloc"),
        }
    }
}

impl<'a> FrameAllocator for BumpAllocator {
    fn allocate_frame(&self, page_size: usize) -> Result<PhysAddr, &'static str> {
        match page_size as u64 {
            Size1GiB::SIZE | Size2MiB::SIZE | Size4KiB::SIZE => {}
            _ => {
                self.logger.errorf(format_args!(
                    "Invalid page size passed to allocate_frame: {} ({})", page_size, BinarySize::from(page_size)
                ));
                return Err("invalid page size passed to frame allocator");
            }
        }
        let mut state = self.state.write();
        let mut iterator = self.mem_info.iter_mem_areas();
        if let Some(area) = iterator.nth(state.current_area) {
            let phys_addr = area.phys_start + state.next_offset as usize;
            // Physical page frames have to be aligned to the page size boundary
            // otherwise there will be page faults.
            let aligned_next_offset = align_up(phys_addr, page_size) - area.phys_start as usize;
            if aligned_next_offset + page_size > area.size_bytes {
                // The remaining space of this area is too small.
                // Advance an area if possible
                if state.current_area + 1 == self.mem_info.mem_area_count() as usize {
                    self.logger.errorf(format_args!(
                        "mem areas exhausted (current_area: {}, max: {})",
                        state.current_area, self.mem_info.mem_area_count(),
                    ));
                    return Err("mem areas exhausted, physical memory full");
                }
                state.current_area += 1;
                state.next_offset = 0;
                // drop the iterator earlier so that the return does not reference borrowed stuff
                drop(iterator);
                // drop state so that the lock is released early
                drop(state);
                // Call the allocator again
                return self.allocate_frame(page_size);
            }
            let phys_addr = PhysAddr::try_new(area.phys_start as u64 + aligned_next_offset as u64).or_else(|err| {
                self.logger.errorf(format_args!(
                    "Invalid physical address while allocating frame: {:?}", err
                ));
                Err("could not allocate frame because of invalid physical address")
            })?;
            state.next_offset = aligned_next_offset as u64 + page_size as u64;
            if state.next_offset >= area.size_bytes as u64 {
                // Advance an area if possible. If it's not possible, just stay like this.
                // The next time the allocator is called, there will be an error.
                if state.current_area + 1 < self.mem_info.mem_area_count() as usize {
                    state.current_area += 1;
                    state.next_offset = 0;
                } else {
                    self.logger.warnf(format_args!(
                        "About to run out of physical memory. Current area: {}, next offset: {}, max: {}",
                        state.current_area, state.next_offset, self.mem_info.mem_area_count(),
                    ));
                }
            }
            Ok(phys_addr)
        } else {
            self.logger.errorf(format_args!(
                "coult not access current_area: {}, max: {}",
                state.current_area, self.mem_info.mem_area_count(),
            ));
            Err("could not access current mem area")
        }
    }

    fn return_frame(&self, frame: PhysAddr, page_size: usize) -> Result<(), &'static str> {
        let mut state = self.state.write();
        // We can only do something if the frame is the last allocated frame of the current area.
        // Otherwise we will just ignore this and this frame will be wasted.
        if page_size as u64 > state.next_offset {
            // Page size is bigger than next offset so this frame can't be from the current area.
            return Ok(())
        }
        if let Some(area) = self.mem_info.iter_mem_areas().nth(state.current_area) {
            let phys_addr = area.phys_start as u64 + state.next_offset - page_size as u64;
            if phys_addr == frame.as_u64() {
                state.next_offset -= page_size as u64;
            }
            Ok(())
        } else {
            self.logger.errorf(format_args!(
                "coult not access current_area: {}, max: {}",
                state.current_area, self.mem_info.mem_area_count(),
            ));
            Err("could not access current mem area")
        }
    }

    fn used_size(&self) -> usize {
        let state = self.state.read();
        self.mem_info.iter_mem_areas()
            .take(state.current_area)
            .fold(0, |acc, area| acc + area.size_bytes) +
            state.next_offset as usize
    }
}
