#![no_std]

extern crate alloc;

use core::{ops::Range, ptr::addr_of};

use alloc::sync::Arc;
use arch::addr::{PhysAddr, VirtAddr, PagedVirtAddr};
use kernel_mem::MappedArc;
use kernel_native_defs::mem::{MemoryFuncs, PagingFuncs};
use klog::{Logger, libkernel_log::CommonLogger};
use libkernel::{boot_iface::KernelMemInfo, mem::MemoryProtectionFlags};
use spin::Once;
use tinystd::{types::Addr, alignment::{align_down, align_up}, sizes::BinarySize, formatting::PtrFormatted};

mod arch;

mod paging;
use paging::PageTable;
use x86_64::structures::paging::{Size4KiB, Size2MiB, Size1GiB, PageSize};

pub struct Memory {
    root_pt: Arc<PageTable>,
    bootloader_root_pt: Arc<PageTable>,
    kernel_virt_memory_range: Once<Range<Addr>>,
    kernel_virt_stack_memory_range: Once<Range<Addr>>,
    logger: Logger,
}

impl Memory {
    pub fn new() -> Self {
        Self {
            root_pt: PageTable::new(None),
            bootloader_root_pt: unsafe { PageTable::current_page_table() },
            kernel_virt_memory_range: Once::new(),
            kernel_virt_stack_memory_range: Once::new(),
            logger: Logger::new("arch/x86_64/mem")
        }
    }
}

impl MemoryFuncs for Memory {
    fn init(&self, mem_info: KernelMemInfo) -> Result<(), &'static str> {
        let kimg_page_size = self.preferred_page_size(
            unsafe { addr_of!(kernel_image_end) } as Addr - unsafe { addr_of!(kernel_image_start) } as Addr
        ).unwrap();
        self.kernel_virt_memory_range.call_once(||
            mem_info.hhdm..align_down(
                unsafe { addr_of!(kernel_image_start) } as Addr,
                kimg_page_size
            )
        );

        let smallest_page_size = self.preferred_page_size(1).unwrap();
        self.kernel_virt_stack_memory_range.call_once(||
            align_up(
                unsafe { addr_of!(kernel_image_start) } as Addr,
                smallest_page_size
            )..align_down(Addr::MAX, smallest_page_size)
        );

        Ok(())
    }

    fn free_memory(&self) -> usize {
        todo!()
    }

    fn used_memory(&self) -> usize {
        todo!()
    }

    fn total_memory(&self) -> usize {
        todo!()
    }

    fn kernel_virt_memory_range(&self) -> Range<Addr> {
        self.kernel_virt_memory_range.get().unwrap().clone()
    }

    fn kernel_virt_stack_memory_range(&self) -> Range<Addr> {
        self.kernel_virt_stack_memory_range.get().unwrap().clone()
    }
}

impl PagingFuncs for Memory {
    fn map_page(
        &self,
        phys_addr: Addr,
        virt_addr: Addr,
        page_size: usize,
        prot: MemoryProtectionFlags,
    ) -> Result<(), &'static str> {
        self.root_pt.clone().map(
            PhysAddr::from(phys_addr as u64),
            VirtAddr::from(virt_addr as u64),
            page_size.try_into().map_err(|err| "could not convert page size")?
        ).map_err(|err| "could not map")
    }

    fn unmap_page(&self, virt: Addr) -> Result<(), &'static str> {
        todo!()
    }

    fn unmap_pages(
        &self,
        virt_start: Addr,
        size: usize,
    ) -> Result<(), &'static str> {
        todo!()
    }

    fn allocate_frame(&self, page_size: usize) -> Result<Addr, &'static str> {
        todo!()
    }

    fn remap_region(
        &self,
        start_vaddr: Addr,
        end_vaddr: Addr,
        mem_prot: MemoryProtectionFlags,
        page_size: Option<usize>,
        name: &'static str,
    ) -> Result<(), &'static str> {
        let start_paddr = PagedVirtAddr::new(VirtAddr::from(start_vaddr as u64), self.root_pt.clone())
            .map_err(|err| "could not create paged virt addr from start_vaddr")?
            .phys()
            .map_err(|err| {
                self.logger.warnf(format_args!(
                    "Could not get physical address from {}: {}",
                    PtrFormatted(start_vaddr), err
                ));
                "could not get phys from paged vaddr"
            })?;
        let real_size = end_vaddr - start_vaddr;
        let size = align_up(real_size as Addr, self.page_sizes()[0]);
        self.logger.infof(
            format_args!(
                "Remapping {} phys start {:?} to virt ({:?} - {:?}), size {} (unaligned {}), {}",
                name, start_paddr, start_vaddr, end_vaddr,
                BinarySize::from(size as usize).rounded(),
                BinarySize::from(real_size as usize).rounded(),
                mem_prot,
            )
        );
        todo!();

        Ok(())
    }

    fn page_sizes(&self) -> &[usize] {
        &[
            Size4KiB::SIZE as usize,
            Size2MiB::SIZE as usize,
            Size1GiB::SIZE as usize,
        ]
    }

    fn init_paging(&self) -> Result<(), &'static str> {
        unsafe {
            PageTable::current_page_table();
        }

        todo!()
    }
}

extern "C" {
    static kernel_image_start: u8;
    static kernel_image_end: u8;
    static section_bss_start: u8;
    static section_bss_end: u8;
    static section_data_start: u8;
    static section_data_end: u8;
    static section_rodata_start: u8;
    static section_rodata_end: u8;
    static section_text_start: u8;
    static section_text_end: u8;
}