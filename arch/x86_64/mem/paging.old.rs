use core::ptr::addr_of;

use alloc::boxed::Box;
use kernel_native_defs::mem::PagingFuncs;
use klog::{Logger, libkernel_log::CommonLogger};
use tinystd::{alignment::align_up, sizes::BinarySize};
use x86_64::{structures::paging::{
    PageTable, PageTableIndex, page_table::PageTableEntry, PageTableFlags,
    PageSize, Size4KiB, Size2MiB, PageOffset, Size1GiB
}, VirtAddr, PhysAddr, instructions::tlb};

use super::Memory;

pub(super) struct OffsetPageTableReader {
    p4: &'static PageTable,
    virt_offset: u64,
    phys_offset: u64,
}

#[derive(Debug)]
pub enum PageTableOption {
    None,
    SomePageTable(&'static PageTable),
    SomePage(PhysAddr),
}

#[derive(Debug)]
pub enum PageTableEntryOption {
    None,
    SomeEntry(PageTableEntry),
    EndEntry(PageTableEntry),
}

impl OffsetPageTableReader {
    pub fn new(p4: &'static PageTable, virt_offset: u64, phys_offset: u64) -> Self {
        Self { p4, virt_offset, phys_offset }
    }
}

impl PageTableReader for OffsetPageTableReader {
    fn p4(&self) -> &'static PageTable { self.p4 }

    fn get_page_table_entry(
        &self,
        page_table: &'static PageTable,
        index: PageTableIndex,
        is_p1: bool
    ) -> (PageTableEntryOption, PageTableOption) {
        let entry = &page_table[index];
        if entry.is_unused() {
            (PageTableEntryOption::None, PageTableOption::None)
        } else if is_p1 || entry.flags().contains(PageTableFlags::HUGE_PAGE) {
            (PageTableEntryOption::EndEntry(entry.clone()), PageTableOption::SomePage(entry.addr()))
        } else {
            match unsafe {
                ((entry.addr().as_u64() + self.phys_offset + self.virt_offset) as *const PageTable).as_ref()
            } {
                Some(p) => (PageTableEntryOption::SomeEntry(entry.clone()), PageTableOption::SomePageTable(p)),
                _ => return (PageTableEntryOption::None, PageTableOption::None)
            }
        }
    }
}

pub(super) trait PageTableReader {
    fn p4(&self) -> &'static PageTable;

    fn get_page_table_entry(
        &self,
        page_table: &'static PageTable,
        index: PageTableIndex,
        is_p1: bool
    ) -> (PageTableEntryOption, PageTableOption);

    fn entry_from(&self, virt: VirtAddr) -> (Option<PageTableEntry>, Option<PhysAddr>) {
        match self.get_page_table_entry(self.p4(), virt.p4_index(), false) {
            (_, PageTableOption::SomePageTable(p3)) => match self.get_page_table_entry(p3, virt.p3_index(), false) {
                (_, PageTableOption::SomePageTable(p2)) => match self.get_page_table_entry(p2, virt.p2_index(), false) {
                    (_, PageTableOption::SomePageTable(p1)) => match self.get_page_table_entry(p1, virt.p1_index(), true) {
                        // 4 KiB page
                        (PageTableEntryOption::EndEntry(entry), PageTableOption::SomePage(page)) =>
                                (Some(entry), Some(page + <PageOffset as Into<u64>>::into(virt.page_offset()))),
                        _ => (None, None),
                    }
                    // 2 MiB hugepage
                    (PageTableEntryOption::EndEntry(entry), PageTableOption::SomePage(page)) =>
                            (Some(entry), Some(
                                page +
                                (
                                    <PageOffset as Into<u64>>::into(virt.page_offset()) |
                                    (<PageTableIndex as Into<u64>>::into(virt.p1_index()) << 12)
                                )
                            )),
                    _ => (None, None),
                },
                // 1 GiB hugepage
                (PageTableEntryOption::EndEntry(entry), PageTableOption::SomePage(page)) =>
                        (Some(entry), Some(
                            page +
                            (
                                <PageOffset as Into<u64>>::into(virt.page_offset()) |
                                (<PageTableIndex as Into<u64>>::into(virt.p1_index()) << 12) |
                                (<PageTableIndex as Into<u64>>::into(virt.p2_index()) << 12 << 9)
                            )
                        )),
                _ => (None, None)
            }
            _ => (None, None)
        }
    }

    fn phys_from(&self, virt: VirtAddr) -> Option<PhysAddr> {
        match self.get_page_table_entry(self.p4(), virt.p4_index(), false) {
            (_, PageTableOption::SomePageTable(p3)) => match self.get_page_table_entry(p3, virt.p3_index(), false) {
                (_, PageTableOption::SomePageTable(p2)) => match self.get_page_table_entry(p2, virt.p2_index(), false) {
                    (_, PageTableOption::SomePageTable(p1)) => match self.get_page_table_entry(p1, virt.p1_index(), true) {
                        // 4 KiB page
                        (_, PageTableOption::SomePage(page)) =>
                                Some(page + <PageOffset as Into<u64>>::into(virt.page_offset())),
                        _ => None,
                    }
                    // 2 MiB hugepage
                    (_, PageTableOption::SomePage(page)) =>
                            Some(
                                page +
                                (
                                    <PageOffset as Into<u64>>::into(virt.page_offset()) |
                                    (<PageTableIndex as Into<u64>>::into(virt.p1_index()) << 12)
                                )
                            ),
                    _ => None,
                },
                // 1 GiB hugepage
                (_, PageTableOption::SomePage(page)) =>
                        Some(
                            page +
                            (
                                <PageOffset as Into<u64>>::into(virt.page_offset()) |
                                (<PageTableIndex as Into<u64>>::into(virt.p1_index()) << 12) |
                                (<PageTableIndex as Into<u64>>::into(virt.p2_index()) << 12 << 9)
                            )
                        ),
                _ => None
            }
            _ => None
        }
    }
}

#[derive(Debug)]
pub(super) struct PageMapperError(&'static str);

pub(super) struct SmartPageMapper;

impl PageMapper for SmartPageMapper {
    fn create_page_table<'a>(
        memory: &Memory,
        parent: &'a mut PageTable,
        index: PageTableIndex
    ) -> Result<(
        &'a mut PageTable, // the created page table
        &'a mut PageTableEntry // the entry in which the physical address of the created page table was set
    ), PageMapperError> {
        let pt = Box::try_new(PageTable::new())
            .or(Err(PageMapperError("Could not allocate page table on bootstrap heap")))?;
        let pt_phys = memory.phys_from_virt(VirtAddr::from_ptr(addr_of!(*pt)));

        let entry = &mut parent[index];
        *entry = PageTableEntry::new();
        entry.set_addr(pt_phys, Self::flags_for_page_tables());

        // Leak the page table so that it does not get deallocated
        Ok((Box::leak(pt), entry))
    }

    /// Creates a page table if it does not exist, otherwise uses an existing one
    fn need_page_table<'a>(
        memory: &Memory,
        parent: &'a mut PageTable,
        index: PageTableIndex
    ) -> Result<(
        &'a mut PageTable, // the created page table
        &'a mut PageTableEntry // the entry in which the physical address of the created page table was set
    ), PageMapperError> {
        if parent[index].is_unused() || !parent[index].flags().contains(PageTableFlags::PRESENT) {
            Self::create_page_table(memory, parent, index)
        } else {
            let entry = &parent[index];
            if entry.flags().contains(PageTableFlags::HUGE_PAGE) {
                let logger = Logger::new("x86_64/mem/EarlyOffsetBasedMapper");
                logger.notice("There was a problem using a page table.");
                logger.notice("Looks like you're trying to remap something over a hugepage region.");
                logger.notice(
                    "If that's what you're trying to do, then you should decrease the page size of the previous map"
                );
                logger.notice("For example, if you had mapped a 1 GiB page here, and now want 2 MiB, then you");
                logger.notice("need to use the common denominator. The same thing applies for 4 KiB pages.");
                logger.errorf(
                    format_args!(
                        "need_page_table: cannot use entry at {:?} of pt {:p} as pt: is hugepage. Entry: {:?}",
                        index, parent, entry
                    )
                );
                return Err(PageMapperError("cannot use as page table: is hugepage"));
            }
            if entry.flags().contains(PageTableFlags::NO_EXECUTE) {
                Logger::new("x86_64/mem/EarlyOffsetBasedMapper").errorf(
                    format_args!(
                        "need_page_table: cannot use entry at {:?} of pt {:p} as pt: NX bit is set. Entry: {:?}",
                        index, parent, entry
                    )
                );
                return Err(PageMapperError("cannot use as page table: NX bit is set"));
            }

            let entry = &mut parent[index];
            let pt_phys = entry.addr();
            let pt = unsafe { &mut *memory.bootloader_virt_from_phys_base(pt_phys).as_mut_ptr() };
            Ok((pt, entry))
        }
    }

    fn create_page_table_entry<'a>(
        page_table: &'a mut PageTable,
        index: PageTableIndex,
        phys: PhysAddr,
        flags: PageTableFlags
    ) -> Result<&'a mut PageTableEntry, PageMapperError> {
        let entry = &mut page_table[index];
        *entry = PageTableEntry::new();
        entry.set_addr(phys, flags);

        Ok(entry)
    }

    fn unmap<'a>(
        p4: &'a mut PageTable,
        memory: &Memory,
        virt: VirtAddr,
    ) -> Result<(), &'static str> {
        Logger::new("x86_64/paging/early_obm").notice("unmap stub!");
        Ok(())
    }
}

pub(super) trait PageMapper {

    fn flags_for_page_tables() -> PageTableFlags {
        PageTableFlags::WRITABLE | PageTableFlags::PRESENT
    }

    fn create_page_table<'a>(
        memory: &Memory,
        parent: &'a mut  PageTable,
        index: PageTableIndex
    ) -> Result<(
        &'a mut PageTable, // the created page table
        &'a mut  PageTableEntry // the entry in which the physical address of the created page table was set
    ), PageMapperError> ;

    fn need_page_table<'a>(
        memory: &'a Memory,
        parent: &'a mut PageTable,
        index: PageTableIndex
    ) -> Result<(
        &'a mut PageTable, // the created page table
        &'a mut PageTableEntry // the entry in which the physical address of the created page table was set
    ), PageMapperError>;

    fn create_page_table_entry<'a>(
        page_table: &'a mut PageTable,
        index: PageTableIndex,
        phys: PhysAddr,
        flags: PageTableFlags
    ) -> Result<&'a mut  PageTableEntry, PageMapperError>;

    fn map(
        memory: &Memory,
        phys: PhysAddr,
        virt: VirtAddr,
        page_size: u64,
        flags: PageTableFlags
    ) -> Result<(), PageMapperError> {
        if page_size != Size4KiB::SIZE && page_size != Size2MiB::SIZE && page_size != Size1GiB::SIZE {
            return Err(PageMapperError("invalid page size"));
        }

        let mut p4 = memory.p4.write();
        let flags = flags | PageTableFlags::PRESENT;
        let (mut pt, _) = Self::need_page_table(memory, &mut *p4, virt.p4_index()).or_else(|err| {
            Logger::new("x86_64/mem/PageMapper").errorf(
                format_args!(
                    "failed to get P3 from P4 at {:?} to map page ({:?} -> {:?}, {:?}, {}): {:?}",
                    virt.p2_index(), phys, virt, flags, BinarySize::from(page_size).rounded(), err
                )
            );
            Err(PageMapperError("failed to get or create P3"))
        })?;
        if page_size == Size4KiB::SIZE || page_size == Size2MiB::SIZE {
            (pt, _) = Self::need_page_table(memory, pt, virt.p3_index()).or_else(|err| {
                Logger::new("x86_64/mem/PageMapper").errorf(
                    format_args!(
                        "failed to get P2 from P3 at {:?} to map page ({:?} -> {:?}, {:?}, {}): {:?}",
                        virt.p2_index(), phys, virt, flags, BinarySize::from(page_size).rounded(), err
                    )
                );
                Err(PageMapperError("failed to get or create P2"))
            })?;
            if page_size == Size4KiB::SIZE {
                (pt, _) = Self::need_page_table(memory, pt, virt.p2_index()).or_else(|err| {
                    Logger::new("x86_64/mem/PageMapper").errorf(
                        format_args!(
                            "failed to get P1 from P2 at {:?} to map 4K page ({:?} -> {:?}, {:?}, {}): {:?}",
                            virt.p2_index(), phys, virt, flags, BinarySize::from(page_size).rounded(), err
                        )
                    );
                    Err(PageMapperError("failed to get or create P1"))
                })?;
                Self::create_page_table_entry(pt, virt.p1_index(), phys, flags)?;
            } else {
                Self::create_page_table_entry(pt, virt.p2_index(), phys, flags | PageTableFlags::HUGE_PAGE)?;
            }
        } else {
            Self::create_page_table_entry(pt, virt.p3_index(), phys, flags | PageTableFlags::HUGE_PAGE)?;
        }

        tlb::flush(virt);

        Ok(())
    }

    fn map_region(
        memory: &Memory,
        phys: PhysAddr,
        virt: VirtAddr,
        region_size: u64,
        // Specify None to automatically use the biggest possible
        page_size: Option<u64>,
        flags: PageTableFlags
    ) -> Result<(), PageMapperError> {
        let page_size = page_size.unwrap_or_else(|| memory.preferred_page_size(region_size as usize).unwrap() as u64);
        let page_count = align_up(region_size as usize, page_size as usize) as u64 / page_size;

        let extra_maps = match page_size {
            Size2MiB::SIZE | Size1GiB::SIZE => 1,
            _ => 0
        };
        for page_index in 0..(page_count - extra_maps) {
            Self::map(
                memory,
                phys + page_index * page_size,
                virt + page_index * page_size,
                page_size,
                flags
            )?
        }
        // Map the remaining region using a smaller page size to reduce waste of physical space
        if extra_maps != 0 {
            Self::map_region(
                memory,
                phys + (page_count - 1) * page_size,
                virt + (page_count - 1) * page_size,
                region_size % page_size,
                None,
                flags
            )?;
        }

        Ok(())
    }

    fn unmap<'a>(
        p4: &'a mut PageTable,
        memory: &Memory,
        virt: VirtAddr,
    ) -> Result<(), &'static str>;
}
