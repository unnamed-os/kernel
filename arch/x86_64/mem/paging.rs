use core::{mem::size_of, fmt::{Debug, Display}, borrow::Borrow};

use alloc::{sync::Arc, string::String, format, vec::Vec};
use spin::RwLock;
use tinystd::{types::Addr, formatting::PtrFormatted};
use x86_64::{structures::paging::{PageTableFlags}, registers::control::Cr3};

use crate::arch::addr::{PhysAddr, VIRT_ADDR_MASK, VirtAddr, PagedVirtAddr};

const PT_ENTRY_COUNT: u16 = (PageSize::size_4K().0 as usize / size_of::<u64>()) as u16;
const PT_ENTRY_MAX_INDEX: u16 = PT_ENTRY_COUNT - 1;

const VIRT_ADDR_PT_PHYS_MASK: u64 = VIRT_ADDR_MASK ^ 0xfff;

#[cfg(not(feature = "lv5"))]
pub(crate) const MAX_PT_LEVELS: usize = 4;
#[cfg(feature = "lv5")]
pub(crate) const MAX_PT_LEVELS: usize = 5;


#[repr(align(0x1000))]
#[repr(C)]
#[derive(Clone)]
pub struct PageTableEntries {
    entries: [PageTableEntry; PT_ENTRY_COUNT as usize],
}

impl PageTableEntries {
    fn new() -> Self {
        Self {
            entries: [PageTableEntry::new(); PT_ENTRY_COUNT as usize]
        }
    }
}

pub struct PageTable {
    entries: Arc<RwLock<PageTableEntries>>,
    sub_tables: RwLock<Vec<(u16, Arc<PageTable>)>>,
    parent: Option<Arc<PageTable>>
}

impl Debug for PageTable {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let entries = self.entries.read();
        f.debug_struct("PageTable")
            .field("address", &PtrFormatted(entries.entries.as_ptr() as Addr))
            .finish_non_exhaustive()
    }
}

pub struct PageTableEntryWrapper {
    /// Index of the entry
    index: PageTableIndex,
    /// The page table entry itself
    entry: PageTableEntry,
    /// The page table the entry belongs to
    table: Arc<PageTable>
}

impl PageTableEntryWrapper {
    pub fn from(index: PageTableIndex, entry: PageTableEntry, table: Arc<PageTable>) -> Self {
        Self { index, entry, table }
    }

    pub fn is_page_table(&self) -> bool {
        let flags = self.entry.flags();
        flags.contains(PageTableFlags::PRESENT | PageTableFlags::WRITABLE) &&
            !flags.contains(PageTableFlags::HUGE_PAGE) &&
            self.table.level() != 1 &&
            !flags.contains(PageTableFlags::NO_EXECUTE)
    }

    pub fn is_page(&self) -> bool {
        let flags = self.entry.flags();
        flags.contains(PageTableFlags::PRESENT) &&
            (self.table.level() == 1 || flags.contains(PageTableFlags::HUGE_PAGE))
    }

    pub fn get_page_table(&self) -> Option<Arc<PageTable>> {
        if self.is_page_table() {
            self.table.get_sub_table(self.index)
        } else {
            None
        }
    }

    pub fn get_page(&self) -> Option<PhysAddr> {
        if self.is_page() {
            Some(self.entry.addr())
        } else {
            None
        }
    }
}

#[repr(transparent)]
#[derive(Copy, Clone)]
pub struct PageTableEntry(u64);

impl PageTableEntry {
    pub const fn new() -> Self { Self(0) }
    pub fn is_empty(&self) -> bool { self.0 == 0 }
    pub fn reset(&mut self) { self.0 = 0  }

    #[inline(always)]
    pub fn flags(&self) -> PageTableFlags {
        PageTableFlags::from_bits_truncate(self.0)
    }

    #[inline(always)]
    pub fn addr(&self) -> PhysAddr {
        PhysAddr::from_truncate(self.0)
    }

    pub fn set_addr(&mut self, addr: PhysAddr) -> Result<(), String> {
        if addr.is_aligned() {
            self.0 = addr.bits() | self.flags().bits();
            Ok(())
        } else {
            Err(format!("{:?} is not aligned", addr))
        }
    }

    pub fn set_flags(&mut self, flags: PageTableFlags) -> Result<(), String> {
        self.0 = self.addr().bits() | flags.bits();
        Ok(())
    }

    pub fn set(&mut self, addr: PhysAddr, flags: PageTableFlags) -> Result<(), String> {
        if addr.is_aligned() {
            self.0 = addr.bits() | flags.bits();
            Ok(())
        } else {
            Err(format!("{:?} is not aligned", addr))
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct InvalidPageTableIndex(u16);

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct PageTableIndex(u16);

impl PageTableIndex {
    #[inline(always)]
    pub fn new(index: u16) -> Result<Self, InvalidPageTableIndex> {
        match index {
            0..=PT_ENTRY_MAX_INDEX => Ok(Self(index)),
            _ => Err(InvalidPageTableIndex(index))
        }
    }

    pub const fn new_truncated(index: u16) -> Self {
        Self(index % PT_ENTRY_COUNT)
    }

    #[inline(always)]
    pub fn into_usize(self) -> usize {
        <PageTableIndex as Into<usize>>::into(self)
    }
}

impl Into<u16> for PageTableIndex {
    fn into(self) -> u16 { self.0 }
}

impl Into<usize> for PageTableIndex {
    fn into(self) -> usize { self.0 as usize }
}

impl From<u16> for PageTableIndex {
    fn from(value: u16) -> Self { Self::new_truncated(value) }
}

impl From<u64> for PageTableIndex {
    fn from(value: u64) -> Self { Self::new_truncated(value as u16) }
}

impl From<usize> for PageTableIndex {
    fn from(value: usize) -> Self { Self::new_truncated(value as u16) }
}

impl PageTable {
    pub fn new(parent: Option<Arc<PageTable>>) -> Arc<Self> {
        Arc::new(Self {
            entries: Arc::new(RwLock::new(PageTableEntries::new())),
            sub_tables: RwLock::new(Vec::new()),
            parent
        })
    }

    /// Constructs a PageTable from an existing page table in memory.
    /// Uses an existing virtual address to reference that page table.
    /// The page table entries will be copied, so you won't be able to
    /// modify them.
    pub unsafe fn from(existing: VirtAddr) -> Arc<Self> {
        let pt = Arc::new(Self {
            entries: Arc::new(RwLock::new(
                existing.into_type::<PageTableEntries>().clone()
            )),
            sub_tables: RwLock::new(Vec::new()),
            parent: None
        });
        pt.clone().populate_existing_tables();
        pt
    }

    unsafe fn populate_existing_tables(self: Arc<Self>) {
        let entries = self.entries.read();
        entries.entries.iter().enumerate().for_each(|(index, entry)| {
            let entry = PageTableEntryWrapper::from(
                PageTableIndex::from(index), entry.clone(), self.clone()
            );
            if entry.is_page_table() {
                let page_table = entry.get_page_table().unwrap();
                let mut sub_tables = self.sub_tables.write();
                sub_tables.push((index as u16, page_table));
            }
        });
    }

    pub fn has_parent(&self) -> bool {
        return self.parent.is_some();
    }

    #[inline(always)]
    pub fn is_root(&self) -> bool {
        !self.has_parent()
    }

    pub fn root(self: Arc<Self>) -> Arc<Self> {
        self.parent.as_ref().and_then(|parent| Some(parent.clone().root())).unwrap_or(self)
    }

    pub fn virt_addr(self: Arc<Self>) -> PagedVirtAddr {
        let entries = self.entries.read();
        PagedVirtAddr::of(entries, self.clone().root()).unwrap()
    }

    pub fn level(&self) -> usize {
        let mut level = MAX_PT_LEVELS;
        if !self.has_parent() {
            return level;
        }
        level -= 1;
        let mut page_table = self.parent.as_ref()
            .expect("when PT has a parent, it must unwrap");
        while page_table.has_parent() {
            level -= 1;
            page_table = page_table.parent.as_ref()
                .expect("when PT has a parent, it must unwrap");
            assert!(level > 0);
        }
        level
    }

    pub(crate) fn set_entry(
        &self,
        index: PageTableIndex,
        phys: PhysAddr,
        flags: PageTableFlags
    ) -> Result<(), String> {
        let mut entries = self.entries.write();
        let entry = &mut entries.entries[index.into_usize()];
        *entry = PageTableEntry::new();
        entry.set(phys, flags).map_err(|e| format!("Failed to set entry: {}", e))
    }

    pub(crate) fn get_entry(
        self: Arc<Self>,
        index: PageTableIndex
    ) -> Result<PageTableEntryWrapper, String> {
        let entries = self.entries.read();
        Ok(PageTableEntryWrapper::from(
            index,
            entries.entries[index.into_usize()],
            self.clone()
        ))
    }

    pub(crate) fn create_page_table(
        self: Arc<Self>,
        index: PageTableIndex
    ) -> Result<(), String> {
        let page_table = PageTable::new(Option::Some(self.clone()));
        let phys_addr = page_table.phys_addr();
        self.set_entry(index, phys_addr, Self::flags_for_page_tables())
    }

    pub(crate) fn map(
        self: Arc<Self>,
        phys: PhysAddr,
        virt: VirtAddr,
        page_size: PageSize
    ) -> Result<(), String> {
        let root = self.clone().root();
        let root_level = root.level();
        let entry = self.get_entry(virt.pt_index(root_level))?;
        let current_level = root_level;
        if entry.is_page_table() {

        }
        todo!()
    }

    pub(crate) fn page_table_virt_addr(&self) -> VirtAddr {
        let entries = self.entries.read();
        VirtAddr::of(entries.entries)
    }

    pub(crate) fn get_sub_table(&self, index: PageTableIndex) -> Option<Arc<Self>> {
        let sub_tables = self.sub_tables.read();
        sub_tables.iter().find(|sub_table| sub_table.0 == index.0)
            .and_then(|sub_table| Some(sub_table.1.clone()))
    }

    pub(crate) fn get_entry_by_virt(self: Arc<Self>, addr: VirtAddr) -> Result<PageTableEntryWrapper, String> {
        let page_table = self.clone();
        let entry = self.get_entry(addr.pt_index(page_table.level()))?;
        if entry.is_page_table() {
            match entry.get_page_table() {
                Some(page_table) => page_table.get_entry_by_virt(addr),
                None => Err(format!(
                    "Unexpectedly couldn't get page table for addr {:?}, level {}",
                    addr, page_table.level()
                ))
            }
        } else {
            Ok(entry)
        }
    }

    pub(crate) unsafe fn current_page_table() -> Arc<Self> {
        let (cr3_frame, _) = Cr3::read();
        let cr3_addr = VirtAddr::from(cr3_frame.start_address().as_u64());
        Self::from(cr3_addr)
    }

    fn phys_addr(self: Arc<Self>) -> PhysAddr {
        self.virt_addr().phys().unwrap()
    }

    #[inline(always)]
    fn flags_for_page_tables() -> PageTableFlags {
        PageTableFlags::WRITABLE | PageTableFlags::PRESENT
    }

}

#[derive(PartialEq, PartialOrd, Eq, Ord)]
pub struct PageSize(u64);

impl PageSize {
    pub const fn size_4K() -> PageSize {
        Self(1 << 12 << 9 * (Self::level_4K() - 1))
    }

    pub const fn size_2M() -> PageSize {
        Self(1 << 12 << 9 * (Self::level_2M() - 1))
    }

    pub const fn size_1G() -> PageSize {
        Self(1 << 12 << 9 * (Self::level_1G() - 1))
    }

    #[cfg(feature = "lv5")]
    pub const fn size_512G() -> PageSize {
        Self(1 << 12 << 9 * (Self::level_512G() - 1))
    }

    #[cfg(feature = "lv5")]
    pub const fn level_512G() -> usize { MAX_PT_LEVELS - 1 }
    pub const fn level_1G() -> usize {
        #[cfg(feature = "lv5")]
        return Self::level_512G() - 1;
        #[cfg(not(feature = "lv5"))]
        return MAX_PT_LEVELS - 1;
    }
    pub const fn level_2M() -> usize { Self::level_1G() - 1 }
    pub const fn level_4K() -> usize { Self::level_2M() - 1 }

    pub fn level(self) -> usize {
        if self == Self::size_4K() { Self::level_4K() }
        else if self == Self::size_2M() { Self::level_2M() }
        else if self == Self::size_1G() { Self::level_1G() }
        else {
            #[cfg(feature = "lv5")]
            if self == Self::size_512G() { Self::level_512G() }
            unreachable!()
        }
    }


    pub const fn valid_page_sizes() -> [PageSize; MAX_PT_LEVELS - 1] {
        [
            Self::size_4K(),
            Self::size_2M(),
            Self::size_1G(),
            #[cfg(feature = "lv5")]
            Self::size_512G(),
        ]
    }

    pub fn valid_page_sizes_usize() -> [usize; MAX_PT_LEVELS - 1] {
        Self::valid_page_sizes().map(|size| size.into())
    }

    #[inline(always)]
    pub fn into_usize(self) -> usize {
        <PageSize as Into<usize>>::into(self)
    }

}

#[derive(Debug)]
pub enum PageSizeConversionErr {
    InvalidPageSize(usize)
}

impl Display for PageSizeConversionErr {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("Cannot convert page size: {:?}", self))
    }
}

impl TryFrom<usize> for PageSize {
    type Error = PageSizeConversionErr;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        if !Self::valid_page_sizes_usize().contains(&value) {
            Err(PageSizeConversionErr::InvalidPageSize(value))
        } else {
            Ok(PageSize(value as u64))
        }
    }
}

impl Into<usize> for PageSize {
    fn into(self) -> usize {
        self.0 as usize
    }
}

