use core::borrow::Borrow;
use core::fmt::{Debug, self, Display};
use core::ops::Range;
use core::ptr::addr_of;

use alloc::format;
use alloc::string::String;
use alloc::sync::Arc;
use tinystd::alignment::align_down;
use tinystd::{formatting::PtrFormatted, types::Addr};
use x86_64::structures::paging::{Size4KiB, PageSize};

use crate::paging::{PageTableIndex, PageTable, MAX_PT_LEVELS};

#[cfg(not(feature = "lv5"))]
pub(crate) const VIRT_ADDR_MASK: u64 = 0x000f_ffff_ffff_ffff;
#[cfg(feature = "lv5")]
pub(crate) const VIRT_ADDR_MASK: u64 = 0x1fff_ffff_ffff_ffff;

#[cfg(not(feature = "lv5"))]
pub(crate) const VIRT_ADDR_INVALID_RANGE: Range<u32> = 48..64;
#[cfg(feature = "lv5")]
pub(crate) const VIRT_ADDR_INVALID_RANGE: Range<u32> = 57..64;

pub(crate) const VIRT_ADDR_VALIDITY_MASK: u64 = !VIRT_ADDR_MASK;

pub(crate) const PHYS_ADDR_MASK: u64 = 0x000f_ffff_ffff_ffff;
pub(crate) const PHYS_ADDR_VALIDITY_MASK: u64 = !PHYS_ADDR_MASK;

const PHYS_ADDR_ALIGNMENT: usize = Size4KiB::SIZE as usize;


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct PhysAddr(u64);

impl Debug for PhysAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("PhysAddr")
            .field(&format_args!("{}", PtrFormatted(self.0)))
            .finish()
    }
}

pub struct InvalidPhysAddr(pub u64);

impl Debug for InvalidPhysAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("InvalidPhysAddr")
            .field(&format_args!("{}", PtrFormatted(self.0)))
            .finish()
    }
}

impl PhysAddr {
    #[inline(always)]
    pub fn try_from(addr: u64) -> Result<Self, InvalidPhysAddr> {
        match addr | PHYS_ADDR_VALIDITY_MASK {
            0 => Ok(Self(addr)),
            _ => Err(InvalidPhysAddr(addr))
        }
    }

    #[inline(always)]
    pub fn from(addr: u64) -> Self {
        Self::try_from(addr).expect("invalid physical address")
    }

    #[inline(always)]
    pub fn from_truncate(addr: u64) -> Self {
        Self(addr & PHYS_ADDR_VALIDITY_MASK)
    }

    #[inline(always)]
    pub fn is_aligned(&self) -> bool {
        self.0 == align_down(self.0 as usize, PHYS_ADDR_ALIGNMENT) as u64
    }

    #[inline(always)]
    pub fn as_u64(&self) -> u64 {
        self.0
    }

    #[inline(always)]
    pub fn bits(&self) -> u64 {
        self.0 & PHYS_ADDR_MASK
    }
}

impl Into<u64> for PhysAddr {
    #[inline(always)]
    fn into(self) -> u64 { self.0 }
}

impl Into<Addr> for PhysAddr {
    #[inline(always)]
    fn into(self) -> Addr { self.0 as Addr }
}

impl From<u64> for PhysAddr {
    #[inline(always)]
    fn from(value: u64) -> Self { Self::from(value) }
}

impl From<Addr> for PhysAddr {
    #[inline(always)]
    fn from(value: Addr) -> Self { Self::from(value as u64) }
}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct VirtAddr(u64);

impl Debug for VirtAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("VirtAddr")
            .field(&format_args!("{}", PtrFormatted(self.0)))
            .finish()
    }
}

impl Display for VirtAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("{}", PtrFormatted(self.0)))
    }
}

pub struct InvalidVirtAddr(pub u64);

impl Debug for InvalidVirtAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("InvalidVirtAddr")
            .field(&format_args!("{}", PtrFormatted(self.0)))
            .finish()
    }
}

impl VirtAddr {
    #[inline(always)]
    pub fn try_from(addr: u64) -> Result<Self, InvalidVirtAddr> {
        match addr & VIRT_ADDR_VALIDITY_MASK {
            0 | VIRT_ADDR_VALIDITY_MASK => Ok(Self(addr)),
            _ => Err(InvalidVirtAddr(addr))
        }
    }

    #[inline(always)]
    pub fn from(addr: u64) -> Self {
        Self::try_from(addr).expect("invalid virtual address")
    }

    #[inline(always)]
    pub fn pt_index(self, level: usize) -> PageTableIndex {
        (self.0 >> 12 >> (9 * level)).into()
    }

    #[inline(always)]
    pub fn p1_index(self) -> PageTableIndex { (self.0 >> 12).into() }

    #[inline(always)]
    pub fn p2_index(self) -> PageTableIndex { (self.0 >> 21).into() }

    #[inline(always)]
    pub fn p3_index(self) -> PageTableIndex { (self.0 >> 30).into() }

    #[inline(always)]
    pub fn p4_index(self) -> PageTableIndex { (self.0 >> 39).into() }

    #[cfg(feature = "lv5")]
    #[inline(always)]
    pub fn p5_index(self) -> PageTableIndex { (self.0 >> 48).into() }

    pub fn of<T : Borrow<T>>(item: T) -> Self {
        VirtAddr::from(item.borrow() as *const T as u64)
    }

    #[inline(always)]
    pub unsafe fn into_type<T>(&self) -> &T {
        self.try_into_type().unwrap()
    }

    #[inline(always)]
    pub unsafe fn try_into_type<T>(&self) -> Option<&T> {
        (self.0 as usize as *const T).as_ref()
    }
}

#[derive(Clone)]
pub struct PagedVirtAddr {
    addr: VirtAddr,
    root_pt: Arc<PageTable>
}

#[derive(Debug)]
pub enum PagedVirtAddrErr {
    PageTableNotRoot,
}

impl PagedVirtAddr {
    pub fn new(addr: VirtAddr, root_pt: Arc<PageTable>) -> Result<Self, PagedVirtAddrErr> {
        if root_pt.level() != MAX_PT_LEVELS {
            Err(PagedVirtAddrErr::PageTableNotRoot)
        } else {
            Ok(Self { addr, root_pt })
        }
    }

    pub fn of<T : Borrow<T>>(item: T, root_pt: Arc<PageTable>) -> Result<Self, PagedVirtAddrErr> {
        Self::new(VirtAddr::from(item.borrow() as *const T as u64), root_pt)
    }

    pub fn phys(&self) -> Result<PhysAddr, String> {
        self.root_pt.clone().get_entry_by_virt(self.addr)
            .map(|entry| entry.get_page())?
            .ok_or_else(|| format!("Could not find a page with {:?} in {:?}", self.addr, self.root_pt))
    }
}

impl Into<u64> for VirtAddr {
    #[inline(always)]
    fn into(self) -> u64 { self.0 }
}

impl Into<Addr> for VirtAddr {
    #[inline(always)]
    fn into(self) -> Addr { self.0 as Addr }
}

impl From<u64> for VirtAddr {
    #[inline(always)]
    fn from(value: u64) -> Self { Self::from(value) }
}

impl From<Addr> for VirtAddr {
    #[inline(always)]
    fn from(value: Addr) -> Self { Self::from(value as u64) }
}
