#![no_std]

#![feature(allocator_api)]

extern crate alloc;

use core::{ptr::addr_of, sync::atomic::{AtomicU64, AtomicBool, Ordering}, ops::Range};

use alloc::boxed::Box;
use kernel_mem::{heap::{bootstrap_heap_virt_addr, bootstrap_heap_size}, MappedRef};
use kernel_native_defs::mem::PagingFuncs;
use klog::{Logger, libkernel_log::CommonLogger};
use libkernel::{
    boot_iface::KernelMemInfo,
    mem::{MemoryProtectionFlags, mem_protection_r, mem_protection_rx, mem_protection_rw}
};
use spin::{RwLock, Once};
use tinystd::{types::Addr, sizes::BinarySize, formatting::PtrFormatted, alignment::{align_down, align_up}};

use x86_64::{
    structures::paging::{
        PageTable,
        PhysFrame,
        PageTableFlags, Size4KiB, PageSize, Size2MiB, Size1GiB
    },
    registers::control::{Cr3, Cr3Flags},
    PhysAddr,
    VirtAddr,
};

use self::{paging::{SmartPageMapper, PageMapper}, frame_alloc::FrameAllocator};

mod paging;
mod frame_alloc;
mod arch;

static BSP_P4: Once<Addr> = Once::new();

pub fn wait_for_p4() -> Addr {
    *BSP_P4.wait()
}

pub(crate) fn set_bsp_p4(p4: Addr) {
    BSP_P4.call_once(|| p4);
}

pub struct Memory {
    p4: RwLock<Box<PageTable>>,
    logger: Logger,
    mem_info: KernelMemInfo,
    frame_allocator: frame_alloc::BumpAllocator,
    using_bootloader_p4: AtomicBool,
    bootstrap_heap_virt_offs: AtomicU64,
}

impl Memory {

    fn current_pml4_phys_frame() -> PhysFrame {
        let (frame, _) = Cr3::read();
        frame
    }

    fn bootloader_virt_from_phys_base(&self, addr: PhysAddr) -> VirtAddr {
        VirtAddr::new(self.mem_info.virt_base as u64 + (addr.as_u64() - self.mem_info.phys_base as u64))
    }

    fn phys_from_virt(&self, addr: VirtAddr) -> PhysAddr {
        let phys_addr = if self.using_bootloader_p4.load(Ordering::Relaxed) {
            if addr.as_u64() & self.mem_info.virt_base as u64 == self.mem_info.virt_base as u64 {
                PhysAddr::try_new(addr.as_u64() - self.mem_info.virt_base as u64 + self.mem_info.phys_base as u64).ok()
            } else {
                PhysAddr::try_new(addr.as_u64() - self.mem_info.hhdm as u64).ok()
                //IdentityPageTableReader::new(self.current_pml4()).phys_from(addr)
            }
        } else {
            //OffsetPageTableReader::new(self.current_pml4(), PHYS_MEM_VM_OFFSET, 0).phys_from(addr)
            todo!()
        };
        match phys_addr {
            Some(phys_addr) => return phys_addr,
            option => {
                self.logger.errorf(format_args!(
                    "Invalid {:?} trying to get phys from virt {:?}. Using bl p4: {}",
                    phys_addr, addr,
                    self.using_bootloader_p4.load(Ordering::Relaxed)
                ));
                // cause panic
                option.unwrap()
            }
        }
    }

    fn current_pml4(&self) -> MappedRef<PageTable> {
        let pml4_frame = Memory::current_pml4_phys_frame();
        let pml4_virt_addr = if self.using_bootloader_p4.load(Ordering::Relaxed) {
            pml4_frame.start_address().as_u64()
        } else {
            panic!("Do not get the PML4 from CR3 at runtime!")
        };
        let pml4_virt = VirtAddr::new(pml4_virt_addr);
        unsafe {
            MappedRef::from_raw(
                pml4_frame.start_address().as_u64() as Addr,
                pml4_virt.as_u64() as Addr
            )
        }.unwrap()
    }

    fn remap_region(
        &self,
        start_vaddr: VirtAddr,
        end_vaddr: VirtAddr,
        mem_prot: MemoryProtectionFlags,
        page_size: Option<u64>,
        name: &'static str,
    ) -> Result<(), &'static str> {
        let start_paddr = self.phys_from_virt(start_vaddr);
        let real_size = end_vaddr.as_u64() - start_vaddr.as_u64();
        let size = align_up(real_size as Addr, self.mem_info.page_size);
        self.logger.infof(
            format_args!(
                "Remapping {} phys start {:?} to virt ({:?} - {:?}), size {} (unaligned {}), {}",
                name, start_paddr, start_vaddr, end_vaddr,
                BinarySize::from(size as usize).rounded(),
                BinarySize::from(real_size as usize).rounded(),
                mem_prot,
            )
        );
        SmartPageMapper::map_region(
            &self,
            start_paddr,
            start_vaddr,
            size as u64,
            page_size,
            page_table_flags_from_mem_prot_flags(mem_prot),
        ).or(Err("failed to remap bss"))?;

        Ok(())
    }

    fn remap(&self) -> Result<(), &'static str> {
        // First we remap the entire kernel image as readable, then we map the sections
        // that need other permissions. Sections like .got need to be mapped but we don't
        // track them explicitly. This will do until we do our own ELF parsing and map
        // according to program headers.
        self.remap_region(
            VirtAddr::new(unsafe { addr_of!(kernel_image_start) } as u64),
            VirtAddr::new(unsafe { addr_of!(kernel_image_end) } as u64),
            mem_protection_r(),
            // Use 4 KiB pages to ensure the maps that come after this work properly
            Some(Size4KiB::SIZE),
            "kernel image"
        )?;

        self.remap_region(
            VirtAddr::new(unsafe { addr_of!(section_bss_start) } as u64),
            VirtAddr::new(unsafe { addr_of!(section_bss_end) } as u64),
            mem_protection_rw(),
            Some(Size4KiB::SIZE),
            "bss section"
        )?;

        self.remap_region(
            VirtAddr::new(unsafe { addr_of!(section_data_start) } as u64),
            VirtAddr::new(unsafe { addr_of!(section_data_end) } as u64),
            mem_protection_rw(),
            Some(Size4KiB::SIZE),
            "data section"
        )?;

        self.remap_region(
            VirtAddr::new(unsafe { addr_of!(section_rodata_start) } as u64),
            VirtAddr::new(unsafe { addr_of!(section_rodata_end) } as u64),
            mem_protection_r(),
            Some(Size4KiB::SIZE),
            "rodata section"
        )?;

        // "text" shall be executable
        self.remap_region(
            VirtAddr::new(unsafe { addr_of!(section_text_start) } as u64),
            VirtAddr::new(unsafe { addr_of!(section_text_end) } as u64),
            mem_protection_rx(),
            Some(Size4KiB::SIZE),
            "text section"
        )?;

        // The bootstrap heap should be rewritable in the phys-remapped portion
        // so we can create page tables etc.
        let bootstrap_heap_virt_addr = bootstrap_heap_virt_addr();
        // Page tables are always aligned to 4K
        let bootstrap_heap_virt_addr_aligned = align_up(bootstrap_heap_virt_addr, Size4KiB::SIZE as usize);
        let bootstrap_heap_phys_addr = self.phys_from_virt(VirtAddr::new(bootstrap_heap_virt_addr_aligned as u64));

        let bootstrap_heap_virt_offs_addr = VirtAddr::new(bootstrap_heap_phys_addr.as_u64());
        self.bootstrap_heap_virt_offs.store(
            bootstrap_heap_virt_offs_addr.as_u64() - bootstrap_heap_virt_offs_addr.as_u64(),
            Ordering::Relaxed
        );
        self.logger.infof(format_args!(
            "Remapping bootstrap heap at {:?}/{:?}, size {}",
            bootstrap_heap_phys_addr, bootstrap_heap_virt_offs_addr,
            BinarySize::from(bootstrap_heap_size()).rounded()
        ));
        SmartPageMapper::map_region(
            &self,
            bootstrap_heap_phys_addr,
            bootstrap_heap_virt_offs_addr,
            bootstrap_heap_size() as u64,
            None,
            page_table_flags_from_mem_prot_flags(mem_protection_rw()),
        ).or_else(|err| {
            self.logger.errorf(
                format_args!(
                    "failed to remap bootstrap heap {:?} to {:?}: {:?}",
                    bootstrap_heap_phys_addr, bootstrap_heap_virt_offs_addr, err
                )
            );
            Err("failed to remap bootstrap heap")
        })?;

        let stack_size = self.mem_info.stack_size;
        let stack_vaddr = VirtAddr::new(self.mem_info.stack_start as u64).align_down(self.mem_info.page_size as u64);
        let stack_paddr = self.phys_from_virt(
            VirtAddr::new(self.mem_info.stack_start as u64)
        ).align_down(self.mem_info.page_size as u64);

        self.logger.infof(
            format_args!(
                "Remapping stack phys {:?} to virt {:?}, size {}",
                stack_paddr, stack_vaddr,
                BinarySize::from(stack_size).rounded()
            )
        );
        SmartPageMapper::map_region(
            &self,
            stack_paddr,
            stack_vaddr,
            stack_size as u64,
            Some(Size4KiB::SIZE),
            page_table_flags_from_mem_prot_flags(mem_protection_rw())
        ).or(Err("failed to remap stack"))?;

        Ok(())
    }

    unsafe fn load_p4(&self) {
        let p4 = self.p4.read();
        self.logger.infof(format_args!("Loading new P4, virt addr: {:p}", *p4));
        let p4_phys_frame = PhysFrame::from_start_address(
            self.phys_from_virt(VirtAddr::from_ptr(&**p4))
        ).unwrap();
        self.logger.infof(format_args!("Loading P4 table at {:?} into CR3", p4_phys_frame));
        Cr3::write(p4_phys_frame, Cr3Flags::empty());
        set_bsp_p4(p4_phys_frame.start_address().as_u64() as Addr);
    }

}

impl<'a> kernel_native_defs::mem::Memory for Memory {
    fn new(mem_info: KernelMemInfo) -> Self {
        let logger = Logger::new("x86_64/mem");
        Self {
            p4: match Box::try_new(PageTable::new())
                .or(Err("Could not allocate p4 on heap")) {
                    Err(msg) => {
                        logger.fatalf(
                            format_args!("Failed to allocate new P4: {}", msg)
                        );
                        panic!("Failed to allocate new P4")
                    }
                    Ok(p) => RwLock::new(p),
                },
            logger,
            mem_info,
            frame_allocator: frame_alloc::BumpAllocator::new(mem_info),
            using_bootloader_p4: AtomicBool::new(true),
            bootstrap_heap_virt_offs: AtomicU64::new(0),
        }
    }
}

impl kernel_native_defs::mem::MemoryFuncs for Memory {

    fn free_memory(&self) -> usize {
        self.total_memory() - self.used_memory()
    }

    fn used_memory(&self) -> usize {
        self.frame_allocator.used_size() +
            self.mem_info.iter_used_mem_areas().fold(0, |acc, area| acc + area.size_bytes)
    }

    fn total_memory(&self) -> usize {
        self.mem_info.iter_mem_areas().fold(0, |acc, area| acc + area.size_bytes) +
            self.mem_info.iter_used_mem_areas().fold(0, |acc, area| acc + area.size_bytes)
    }

    fn kernel_virt_memory_range(&self) -> Range<Addr> {
        let page_size = self.preferred_page_size(
            unsafe { addr_of!(kernel_image_end) } as Addr - unsafe { addr_of!(kernel_image_start) } as Addr
        ).unwrap();
        self.mem_info.hhdm..align_down(unsafe { addr_of!(kernel_image_start) } as Addr, page_size)
    }

    fn kernel_virt_stack_memory_range(&self) -> Range<Addr> {
        let page_size = self.preferred_page_size(1).unwrap();
        align_up(unsafe { addr_of!(kernel_image_start) } as Addr, page_size)..align_down(Addr::MAX, page_size)
    }
}

impl<'a> kernel_native_defs::mem::SupportsPaging for Memory {

    fn init_paging<F>(&self, before_effective_hook: F) where F : FnOnce(&Self) {
        self.logger.infof(
            format_args!("Current physical base: {}", PtrFormatted(self.mem_info.phys_base))
        );
        self.logger.infof(
            format_args!("Current virtual base: {}", PtrFormatted(self.mem_info.virt_base))
        );
        self.logger.infof(
            format_args!("Current HHDM offset: {}", PtrFormatted(self.mem_info.hhdm))
        );

        match self.remap() {
            Err(msg) => {
                self.logger.fatalf(
                    format_args!("Failed to remap into new page table: {}", msg)
                );
                panic!("Failed to remap")
            }
            _ => {}
        }

        before_effective_hook(self);

        unsafe {
            self.load_p4();
        }

        self.using_bootloader_p4.store(false, Ordering::Relaxed);
    }
}

impl kernel_native_defs::mem::PagingFuncs for Memory {

    fn map_page(
        &self,
        phys_addr: Addr,
        virt_addr: Addr,
        page_size: usize,
        prot: MemoryProtectionFlags
    ) -> Result<(), &'static str> {
        /*self.logger.debugf(format_args!(
            "Mapping phys {} to virt {}, page size {}, prot {:?}",
            PtrFormatted(phys_addr), PtrFormatted(virt_addr), BinarySize::from(page_size), prot
        ));
        OffsetPageMapper::map(
            &self,
            PhysAddr::try_new(phys_addr as u64).or_else(|err| {
                self.logger.errorf(format_args!(
                    "invalid phys_addr {} for map_page: {:?}", PtrFormatted(phys_addr), err
                ));
                Err("invalid physical address for phys_addr")
            })?,
            VirtAddr::new(virt_addr as u64),
            page_size as u64,
            page_table_flags_from_mem_prot_flags(prot),
        ).or_else(|err| {
            self.logger.errorf(
                format_args!(
                    "Failed to map using page mapper: {:?}", err
                )
            );
            Err("failed to map")
        })*/
        todo!()
    }

    fn unmap_page(&self, virt: Addr) -> Result<(), &'static str> {
        //Logger::new("x86_64/mem").notice("unmap_page stub!");
        Ok(())
    }

    fn unmap_pages(
        &self,
        virt_start: Addr,
        size: usize,
    ) -> Result<(), &'static str> {
        //Logger::new("x86_64/mem").notice("unmap_pages stub!");
        Ok(())
    }

    fn page_sizes(&self) -> &[usize] {
        &[
            Size4KiB::SIZE as usize,
            Size2MiB::SIZE as usize,
            Size1GiB::SIZE as usize,
        ]
    }

    fn allocate_frame(&self, page_size: usize) -> Result<Addr, &'static str> {
        match self.frame_allocator.allocate_frame(page_size) {
            Ok(phys_addr) => Ok(phys_addr.as_u64() as Addr),
            Err(msg) => Err(msg)
        }
    }

    fn remap_region(
        &self,
        start_vaddr: Addr,
        end_vaddr: Addr,
        mem_prot: MemoryProtectionFlags,
        page_size: Option<usize>,
        name: &'static str,
    ) -> Result<(), &'static str> {
        self.remap_region(
            VirtAddr::new(start_vaddr as u64),
            VirtAddr::new(end_vaddr as u64),
            mem_prot, page_size.and_then(|s| Some(s as u64)), name
        )
    }
}

fn page_table_flags_from_mem_prot_flags(mem_prot_flags: MemoryProtectionFlags) -> PageTableFlags {
    let mut flags = PageTableFlags::PRESENT;
    if mem_prot_flags.write {
        flags |= PageTableFlags::WRITABLE;
    }
    if !mem_prot_flags.execute {
        flags |= PageTableFlags::NO_EXECUTE;
    }
    flags
}

extern "C" {
    static kernel_image_start: u8;
    static kernel_image_end: u8;
    static section_bss_start: u8;
    static section_bss_end: u8;
    static section_data_start: u8;
    static section_data_end: u8;
    static section_rodata_start: u8;
    static section_rodata_end: u8;
    static section_text_start: u8;
    static section_text_end: u8;
}