use klog::{Logger, libkernel_log::CommonLogger};
use x86::{cpuid::CpuId, msr::{
    wrmsr, rdmsr,
    IA32_APIC_BASE, IA32_X2APIC_SIVR, IA32_X2APIC_LVT_ERROR, IA32_X2APIC_APICID,
    IA32_X2APIC_VERSION, IA32_X2APIC_EOI, IA32_X2APIC_ICR, IA32_X2APIC_ESR,
    IA32_X2APIC_LVT_TIMER, IA32_X2APIC_CUR_COUNT, IA32_X2APIC_DIV_CONF, IA32_TSC_DEADLINE, IA32_X2APIC_INIT_COUNT,
}, time::rdtsc};
use lazy_static::lazy_static;
use x86_64::{registers::control::{Cr4, Cr4Flags}};
use crate::devices::pit::PIT;

lazy_static! {
    static ref LOGGER: Logger = Logger::new("x86_64-interrupts/x2apic");
}

// Local X2APIC Manual 2.2
const X2APIC_MODE_ENABLE: u64 = 1 << 10;
const XAPIC_GLOBAL_ENABLE: u64 = 1 << 11;

// Vol 3A 9.4.4.1
const XAPIC_SVR_ENABLE: u64 = 1 << 8;

pub struct LocalX2APIC;

lazy_static! {
    static ref pit: PIT = PIT::new();
}

impl LocalX2APIC {
    pub fn init() -> Result<(), ()> {
        if !CpuId::new().get_feature_info().map_or(false, |feature_info| {
            feature_info.has_x2apic()
        }) {
            LOGGER.notice("x2APIC not supported, ymmv");
            return Err(());
        }

        if pit.init() {
            pit.set_count(u16::MAX);
        }

        unsafe {
            Self::enable_x2apic_mode();
            Self::setup_error_interrupt();
            Self::set_lvt_timer(64, LocalVectorTableTimerMode::OneShot);
        }

        Ok(())
    }

    pub fn init_timer() -> Result<(), ()> {
        Self::set_initial_count(u32::MAX);
        unsafe { Self::set_divide_configuration(0x3); }

        unsafe {
            Cr4::update(|flags| {
                *flags |= Cr4Flags::TIMESTAMP_DISABLE;
            })
        }

        Self::set_initial_count(10_000_000);

        Ok(())
    }

    unsafe fn enable_x2apic_mode() {
        wrmsr(IA32_APIC_BASE, rdmsr(IA32_APIC_BASE) | X2APIC_MODE_ENABLE);
        wrmsr(IA32_X2APIC_SIVR, XAPIC_SVR_ENABLE);
    }

    unsafe fn setup_error_interrupt() {
        Self::set_lvt_error(65);
    }

    pub fn apic_id() -> u64 {
        unsafe { rdmsr(IA32_X2APIC_APICID) }
    }

    pub fn version() -> u64 {
        unsafe { rdmsr(IA32_X2APIC_VERSION) }
    }

    pub(crate) unsafe fn eoi() {
        wrmsr(IA32_X2APIC_EOI, 0)
    }

    fn set_initial_count(value: u32) {
        unsafe { wrmsr(IA32_X2APIC_INIT_COUNT, value as u64); }
    }

    unsafe fn set_icr(value: u64) {
        wrmsr(IA32_X2APIC_ICR, value)
    }

    // Inter Processor Interrupt
    unsafe fn send_ipi(apic_id: u32) {
        // Numbers taken from:
        // https://github.com/redox-os/kernel/blob/master/src/arch/x86_64/device/local_apic.rs#L138
        Self::set_icr(0x4040_u64 | (apic_id as u64) << 32);
    }

    // Error Status Register
    fn read_esr() -> u64 {
        unsafe {
            wrmsr(IA32_X2APIC_ESR, 0);
            rdmsr(IA32_X2APIC_ESR)
        }
    }

    fn get_lvt_timer() -> u32 {
        unsafe { rdmsr(IA32_X2APIC_LVT_TIMER) as u32 }
    }

    unsafe fn set_lvt_timer(value: u32, mode: LocalVectorTableTimerMode) {
        // mode is bit 17:18 according to Intel SDM 3A 11.5.4.1
        wrmsr(IA32_X2APIC_LVT_TIMER, value as u64 | (mode as u64) << 18);
    }

    fn get_lvt_error() -> u32 {
        unsafe { rdmsr(IA32_X2APIC_LVT_ERROR) as u32 }
    }

    unsafe fn set_lvt_error(value: u32) {
        wrmsr(IA32_X2APIC_LVT_ERROR, value as u64);
    }

    fn cur_count() -> u32 {
        unsafe { rdmsr(IA32_X2APIC_CUR_COUNT) as u32 }
    }

    unsafe fn set_divide_configuration(div: u32) {
        wrmsr(IA32_X2APIC_DIV_CONF, div as u64)
    }

    fn get_divide_configuration() -> u32 {
        unsafe { rdmsr(IA32_X2APIC_DIV_CONF) as u32 }
    }

    pub fn set_tsc_deadline(deadline: u64) {
        unsafe { wrmsr(IA32_TSC_DEADLINE, deadline) }
    }

    #[inline(always)]
    pub fn current_tsc() -> u64 {
        unsafe { rdtsc() }
    }

}

#[repr(u32)]
pub enum LocalVectorTableTimerMode {
    OneShot = 0,
    Periodic = 1,
    TSCDeadLine = 2,
}
