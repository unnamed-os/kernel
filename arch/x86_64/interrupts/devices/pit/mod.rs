use core::ops;
use core::sync::atomic::{AtomicBool, Ordering};

use kernel_native_defs::interrupts::InterruptsTrait;
use kernel_native_defs::io::{IOPortReadWrite, IOPortWrite};
use spin::Mutex;
use x86::io::{outb, inb};

use crate::Interrupts;

const CHANNEL_0_DATA_PORT: u16 = 0x40;
const CHANNEL_1_DATA_PORT: u16 = 0x41;
const CHANNEL_2_DATA_PORT: u16 = 0x42;
const MODE_COMMAND_REGISTER: u16 = 0x43;
const RELOAD_PORT: u16 = 0x61;

trait CmdByteConversion {
    fn to_mode_cmd_byte(self) -> u8;
    fn from_mode_cmd_byte(cmd_byte: u8) -> Option<Self> where Self : Sized;
}

#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum ChannelSelect {
    Channel0 = 0b00,
    Channel1 = 0b01,
    Channel2 = 0b10,
    ReadBackCommand = 0b11,
}

impl CmdByteConversion for ChannelSelect {
    fn to_mode_cmd_byte(self) -> u8 { (self as u8) << 6 }
    fn from_mode_cmd_byte(cmd_byte: u8) -> Option<Self> where Self : Sized {
        match (cmd_byte >> 6) & 0b11 {
            v if v == ChannelSelect::Channel0 as u8 => Some(ChannelSelect::Channel0),
            v if v == ChannelSelect::Channel1 as u8 => Some(ChannelSelect::Channel1),
            v if v == ChannelSelect::Channel2 as u8 => Some(ChannelSelect::Channel2),
            v if v == ChannelSelect::ReadBackCommand as u8 => Some(ChannelSelect::ReadBackCommand),
            _ => None
        }
    }
}

#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum AccessMode {
    LatchCountValueCommand = 0b00,
    LoByteOnly = 0b01,
    HiByteOnly = 0b10,
    LoByteHiByte = 0b11,
}

impl CmdByteConversion for AccessMode {
    fn to_mode_cmd_byte(self) -> u8 { (self as u8) << 4 }

    fn from_mode_cmd_byte(cmd_byte: u8) -> Option<Self> where Self : Sized {
        match (cmd_byte >> 4) & 0b11 {
            v if v == AccessMode::LatchCountValueCommand as u8 => Some(AccessMode::LatchCountValueCommand),
            v if v == AccessMode::LoByteOnly as u8 => Some(AccessMode::LoByteOnly),
            v if v == AccessMode::HiByteOnly as u8 => Some(AccessMode::HiByteOnly),
            v if v == AccessMode::LoByteHiByte as u8 => Some(AccessMode::LoByteHiByte),
            _ => None
        }
    }
}

#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum OperatingMode {
    IntOnTerminalCount = 0b000,
    HwRetriggerableOneShot = 0b001,
    RateGenerator = 0b010,
    SquareWaveGenerator = 0b011,
    SoftwareTriggeredStrobe = 0b100,
    HardwareTriggeredStrobe = 0b101,
    //RateGenerator = 0b110, same as 0b010
    //SquareWaveGenerator = 0b111, same as 0b011
}

impl CmdByteConversion for OperatingMode {
    fn to_mode_cmd_byte(self) -> u8 { (self as u8) << 1 }

    fn from_mode_cmd_byte(cmd_byte: u8) -> Option<Self> where Self : Sized {
        match (cmd_byte << 1) & 0b111 {
            v if v == OperatingMode::IntOnTerminalCount as u8 => Some(OperatingMode::IntOnTerminalCount),
            v if v == OperatingMode::HwRetriggerableOneShot as u8 => Some(OperatingMode::HwRetriggerableOneShot),
            v if v == OperatingMode::RateGenerator as u8 || v == OperatingMode::RateGenerator as u8 + 0b100 => Some(OperatingMode::RateGenerator),
            v if v == OperatingMode::SquareWaveGenerator as u8 || v == OperatingMode::SquareWaveGenerator as u8 + 0b100 => Some(OperatingMode::SquareWaveGenerator),
            v if v == OperatingMode::SoftwareTriggeredStrobe as u8 => Some(OperatingMode::SoftwareTriggeredStrobe),
            v if v == OperatingMode::HardwareTriggeredStrobe as u8 => Some(OperatingMode::HardwareTriggeredStrobe),
            _ => None
        }
    }
}

#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum BCDBinaryMode {
    Binary16Bit = 0,
    FourDigitBCD = 1
}

impl CmdByteConversion for BCDBinaryMode {
    fn to_mode_cmd_byte(self) -> u8 { self as u8 }

    fn from_mode_cmd_byte(cmd_byte: u8) -> Option<Self> where Self : Sized {
        match cmd_byte {
            v if v == BCDBinaryMode::Binary16Bit as u8 => Some(BCDBinaryMode::Binary16Bit),
            v if v == BCDBinaryMode::FourDigitBCD as u8 => Some(BCDBinaryMode::FourDigitBCD),
            _ => None,
        }
    }
}

struct ModeCommand(u8);

impl ModeCommand {
    fn from_byte(mode_cmd: u8) -> Self { Self(mode_cmd) }
    fn to_byte(self) -> u8 { self.0 }

    fn get<C : CmdByteConversion>(self) -> C {
        self.try_get().unwrap()
    }

    fn try_get<C : CmdByteConversion>(self) -> Option<C> {
        C::from_mode_cmd_byte(self.0)
    }
}

impl Default for ModeCommand {
    fn default() -> Self {
        Self(0)
    }
}

impl<C : CmdByteConversion> ops::BitOrAssign<C> for ModeCommand {
    fn bitor_assign(&mut self, rhs: C) {
        self.0 |= rhs.to_mode_cmd_byte()
    }
}

impl<C : CmdByteConversion> ops::BitOr<C> for ModeCommand {
    type Output = ModeCommand;

    fn bitor(self, rhs: C) -> Self::Output {
        Self::from_byte(self.0 | rhs.to_mode_cmd_byte())
    }
}

struct Channel0DataPort;
struct Channel1DataPort;
struct Channel2DataPort;
struct ModeCommandRegister;
struct ReloadPort;

impl IOPortReadWrite for Channel0DataPort {
    unsafe fn read() -> Result<u8, ()> { Ok(inb(CHANNEL_0_DATA_PORT)) }
    unsafe fn write(val: u8) -> Result<(), ()> { Ok(outb(CHANNEL_0_DATA_PORT, val)) }
}

impl IOPortReadWrite for Channel1DataPort {
    unsafe fn read() -> Result<u8, ()> { Ok(inb(CHANNEL_1_DATA_PORT)) }
    unsafe fn write(val: u8) -> Result<(), ()> { Ok(outb(CHANNEL_1_DATA_PORT, val)) }
}

impl IOPortReadWrite for Channel2DataPort {
    unsafe fn read() -> Result<u8, ()> { Ok(inb(CHANNEL_2_DATA_PORT)) }
    unsafe fn write(val: u8) -> Result<(), ()> { Ok(outb(CHANNEL_2_DATA_PORT, val)) }
}

impl IOPortWrite for ModeCommandRegister {
    unsafe fn write(val: u8) -> Result<(), ()> { Ok(outb(MODE_COMMAND_REGISTER, val)) }
}

impl IOPortReadWrite for ReloadPort {
    unsafe fn read() -> Result<u8, ()> { Ok(inb(RELOAD_PORT)) }
    unsafe fn write(val: u8) -> Result<(), ()> { Ok(outb(RELOAD_PORT, val)) }
}

pub struct PIT {
    pit_mutex: Mutex<()>,
    is_initialized: AtomicBool,
}

impl PIT {
    pub fn new() -> Self {
        Self {
            pit_mutex: Mutex::new(()),
            is_initialized: AtomicBool::new(false),
        }
    }

    fn set(&self, mode_cmd: ModeCommand) {
        let _pit_mutex = self.pit_mutex.lock();
        unsafe {
            ModeCommandRegister::write(mode_cmd.to_byte()).unwrap();
        }
    }

    pub fn set_count(&self, count: u16) {
        let _pit_mutex = self.pit_mutex.lock();
        Interrupts::interrupt_disable_guard(|| {
            unsafe {
                Channel0DataPort::write(count as u8).unwrap();
                Channel0DataPort::write((count >> 8) as u8).unwrap();
            };
            self.reload_locked();
        });
    }

    fn reload_locked(&self) {
        let current_value = unsafe { ReloadPort::read() }.unwrap();
        // Clear bit 0 first
        unsafe { ReloadPort::write(current_value & 0xFE) }.unwrap();
        // Now reset the reload register
        unsafe { ReloadPort::write(current_value | 0x01) }.unwrap();
    }

    pub fn init(&self) -> bool {
        let is_initialized = self.is_initialized.swap(true, Ordering::Relaxed);
        if is_initialized {
            return is_initialized;
        }
        self.set(
            ModeCommand::default() |
                AccessMode::LoByteHiByte |
                ChannelSelect::Channel0 |
                OperatingMode::IntOnTerminalCount |
                BCDBinaryMode::Binary16Bit
        );
        false
    }
}
