use x86::io::outb;
use x86_64::{instructions::port::Port, structures::port::PortWrite};

const PIC1_COMMAND_PORT: u16 = 0x20;
const PIC1_DATA_PORT: u16 = 0x21;
const PIC2_COMMAND_PORT: u16 = 0xA0;
const PIC2_DATA_PORT: u16 = 0xA1;

const MODE_8086: u8 = 0x1;

#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum Command {
    Init = 0x10,
    InitICW4 = 0x11,
    EndOfInterrupt = 0x20,
}

impl PortWrite for Command {
    unsafe fn write_to_port(port: u16, value: Self) {
        outb(port, value as u8)
    }
}

struct PIC {
    base_offset: u8,
    command_port: Port<Command>,
    data_port: Port<u8>
}

impl PIC {
    unsafe fn end_of_interrupt(&mut self) {
        self.command_port.write(Command::EndOfInterrupt);
    }

    unsafe fn read_int_mask(&mut self) -> u8 {
        self.data_port.read()
    }

    unsafe fn write_int_mask(&mut self, mask: u8) {
        self.data_port.write(mask)
    }

    fn can_handle_interrupt(&self, int: u8) -> bool {
        self.base_offset <= int && int < self.base_offset + 8
    }
}

pub struct ChainedPICs {
    pic1: PIC,
    pic2: PIC,
}

impl ChainedPICs {
    pub const fn new(offset1: u8, offset2: u8) -> Self {
        Self {
            pic1: PIC {
                base_offset: offset1,
                command_port: Port::new(PIC1_COMMAND_PORT),
                data_port: Port::new(PIC1_DATA_PORT)
            },
            pic2: PIC {
                base_offset: offset2,
                command_port: Port::new(PIC2_COMMAND_PORT),
                data_port: Port::new(PIC2_DATA_PORT)
            }
        }
    }

    pub const fn new_contiguous(pic1_offset: u8) -> Self {
        Self::new(pic1_offset, pic1_offset + 8)
    }

    unsafe fn sketchy_wait() {
        Port::<u8>::new(0x80).write(0)
    }

    pub unsafe fn init(&mut self) {
        self.pic1.command_port.write(Command::InitICW4);
        Self::sketchy_wait();
        self.pic2.command_port.write(Command::InitICW4);
        Self::sketchy_wait();

        self.pic1.data_port.write(self.pic1.base_offset);
        Self::sketchy_wait();
        self.pic2.data_port.write(self.pic2.base_offset);
        Self::sketchy_wait();

        // Chaining
        self.pic1.data_port.write(4);
        Self::sketchy_wait();
        self.pic2.data_port.write(2);
        Self::sketchy_wait();

        self.pic1.data_port.write(MODE_8086);
        Self::sketchy_wait();
        self.pic2.data_port.write(MODE_8086);
        Self::sketchy_wait();

        self.disable();
        self.enable(2);
    }

    pub unsafe fn disable(&mut self) {
        self.pic1.write_int_mask(u8::MAX);
        self.pic2.write_int_mask(u8::MAX);
    }

    pub unsafe fn enable_all(&mut self) {
        self.pic1.write_int_mask(u8::MIN);
        self.pic2.write_int_mask(u8::MIN);
    }

    pub unsafe fn enable(&mut self, int_num: u8) {
        if int_num >= 8 {
            if int_num >= 16 {
                panic!("Invalid interrupt number");
            }
            let mask = self.pic2.read_int_mask();
            self.pic2.write_int_mask(mask | 1 << (8 - (int_num - 8)));
        } else {
            let mask = self.pic2.read_int_mask();
            self.pic1.write_int_mask(mask | 1 << (8 - int_num));
        }
    }

    pub unsafe fn end_of_interrupt(&mut self, int: u8) {
        if self.pic1.can_handle_interrupt(int) {
            self.pic1.end_of_interrupt();
        }
        if self.pic2.can_handle_interrupt(int) {
            self.pic1.end_of_interrupt();
            self.pic2.end_of_interrupt();
        }
    }

}
