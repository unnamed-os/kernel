use alloc::boxed::Box;
use kernel_native_cpu_impl::cur_cpu;
use kernel_native_defs::cpu::CPU;
use klog::{Logger, libkernel_log::CommonLogger};
use spin::Mutex;
use lazy_static::lazy_static;
use x86_64::{structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode}, registers::control::{Cr3, Cr2}};

use crate::devices::lapic::x2apic::LocalX2APIC;

lazy_static! {
    static ref GLOBAL_IDT: Mutex<InterruptDescriptorTable> = {
        let idt_mutex = Mutex::new(InterruptDescriptorTable::new());
        idt_mutex
    };
}

pub struct IDT;

impl IDT {

    fn register_default_handlers(mut idt: Box<InterruptDescriptorTable>) -> Box<InterruptDescriptorTable> {
        idt.alignment_check.set_handler_fn(alignment_check_handler_with_err_code);
        idt.bound_range_exceeded.set_handler_fn(bound_range_exceeded_handler);
        idt.breakpoint.set_handler_fn(breakpoint_handler);
        idt.debug.set_handler_fn(debug_handler);
        idt.device_not_available.set_handler_fn(device_not_available_handler);
        idt.divide_error.set_handler_fn(divide_error_handler);
        unsafe {
            idt.double_fault.set_handler_fn(double_fault_handler_with_err_code_no_return)
                .set_stack_index(arch_x86_64_gdt::DOUBLE_FAULT_IST_INDEX);
        }
        idt.general_protection_fault.set_handler_fn(general_protection_fault_handler_with_err_code);
        idt.invalid_opcode.set_handler_fn(invalid_opcode_handler);
        idt.invalid_tss.set_handler_fn(invalid_tss_handler_with_err_code);
        idt.machine_check.set_handler_fn(machine_check_handler_no_return);
        idt.non_maskable_interrupt.set_handler_fn(non_maskable_interrupt_handler);
        idt.overflow.set_handler_fn(overflow_handler);
        idt.page_fault.set_handler_fn(page_fault_handler);
        idt.security_exception.set_handler_fn(security_exception_handler_with_err_code);
        idt.segment_not_present.set_handler_fn(segment_not_present_handler_with_err_code);
        idt.simd_floating_point.set_handler_fn(simd_floating_point_handler);
        idt.stack_segment_fault.set_handler_fn(stack_segment_fault_handler_with_err_code);
        idt.virtualization.set_handler_fn(virtualization_handler);
        idt.vmm_communication_exception.set_handler_fn(vmm_communication_exception_handler_with_err_code);
        idt.x87_floating_point.set_handler_fn(x87_floating_point_handler);
        idt[64].set_handler_fn(apic_timer_handler);
        idt[65].set_handler_fn(generic_handler);
        // PIT
        idt[0].set_handler_fn(generic_handler);
        idt
    }

    pub fn load_idt(idt: Box<InterruptDescriptorTable>) {
        Box::leak(idt).load()
    }

    pub fn load_default_idt() {
        let idt = Box::new(InterruptDescriptorTable::new());
        let idt = Self::register_default_handlers(idt);
        Self::load_idt(idt);
    }
}

extern "x86-interrupt" fn alignment_check_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/alignment-check").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn bound_range_exceeded_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/bound-range-exceeded").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn breakpoint_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/breakpoint").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn generic_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/generic").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );

}

extern "x86-interrupt" fn apic_timer_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/apic-timer").noticef(
        format_args!(
            "Local APIC Timer Interrupt on CPU {}", cur_cpu().cpu_id()
        )
    );
    unsafe { LocalX2APIC::eoi(); }
}

extern "x86-interrupt" fn debug_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/debug").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn device_not_available_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/device-not-available").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn divide_error_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/divide-error").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn double_fault_handler_with_err_code_no_return(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) -> ! {
    Logger::new("x86_64/interrupts/default-handler/double-fault").noticef(
        format_args!(
            "Non-returning interrupt {:#?} with error code {}", stack_frame, error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn general_protection_fault_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/general-protection-fault").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn invalid_opcode_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/invalid-opcode").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn invalid_tss_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/invalid-tss").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn machine_check_handler_no_return(
    stack_frame: InterruptStackFrame,
) -> ! {
    Logger::new("x86_64/interrupts/default-handler/machine-check").noticef(
        format_args!(
            "Non-returning interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn non_maskable_interrupt_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/non-maskable-interrupt").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn overflow_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/overflow").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    page_fault_code: PageFaultErrorCode,
) {
    Logger::new("x86_64/interrupts/default-handler/page-fault").noticef(
        format_args!(
            "Page Fault {:#?}, {}, CR3: {:?}, CR2: {:?}", stack_frame, page_fault_code.bits(),
            Cr3::read().0, Cr2::read(),
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn security_exception_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/security-exception").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn segment_not_present_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/segment-not-present").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn simd_floating_point_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/simd-floating-point").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn stack_segment_fault_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/stack-segment-fault").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn virtualization_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/virtualization").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}

extern "x86-interrupt" fn vmm_communication_exception_handler_with_err_code(
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Logger::new("x86_64/interrupts/default-handler/vmm-communication-exception").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {} with error code {}", stack_frame, cur_cpu().cpu_id(), error_code,
        )
    );
    cur_cpu().halt_execution();
}

extern "x86-interrupt" fn x87_floating_point_handler(
    stack_frame: InterruptStackFrame,
) {
    Logger::new("x86_64/interrupts/default-handler/x87-floating-point").noticef(
        format_args!(
            "Interrupt {:#?} on CPU {}", stack_frame, cur_cpu().cpu_id()
        )
    );
}