#![no_std]
#![feature(abi_x86_interrupt)]

use devices::{lapic::x2apic::LocalX2APIC, pic::ChainedPICs};
use idt::IDT;
use kernel_native_defs::interrupts::{InterruptsTrait, InterruptFuncs};
use klog::{Logger, libkernel_log::CommonLogger};
use x86_64::instructions;

extern crate alloc;

mod idt;

mod devices;

pub struct Interrupts {
    logger: Logger,
}

impl InterruptsTrait for Interrupts {
    fn new() -> Self {
        Self {
            logger: Logger::new("x86_64/interrupts"),
        }
    }

    fn disable_interrupts() {
        instructions::interrupts::disable();
    }

    fn enable_interrupts() {
        instructions::interrupts::enable();
    }

    fn are_interrupts_enabled() -> bool {
        instructions::interrupts::are_enabled()
    }
}

impl InterruptFuncs for Interrupts {

    fn init_bsp(&self) {
        //self.logger.info("Initializing interrupts");
        let mut pic = ChainedPICs::new_contiguous(32);
        unsafe {
            pic.init();
            pic.disable();
        }
        IDT::load_default_idt();
    }

    fn init_ap(&self, cpu_id: usize) {
        //self.logger.infof(format_args!("Initializing interrupts for AP {}", cpu_id));
        let mut pic = ChainedPICs::new_contiguous(32);
        unsafe {
            pic.init();
            pic.disable();
        }
        IDT::load_default_idt();
        LocalX2APIC::init().unwrap();
        self.init_timer(cpu_id);
    }

    fn init_timer(&self, cpu_id: usize) {
        LocalX2APIC::init_timer().unwrap();
    }

    fn runtime_init_bsp(&self) {
        LocalX2APIC::init().unwrap();
    }
}