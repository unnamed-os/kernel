// SPDX-License-Identifier: GPL-3.0-or-later

#![no_main]
#![no_std]

// The boot module must use kernel-core and call into it
pub use kernel_native_boot;

