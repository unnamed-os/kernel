
#![no_std]

extern crate alloc;

use alloc::{vec::Vec, sync::Arc};
pub use libkernel_log;

use core::{fmt, sync::atomic::{AtomicBool, Ordering}};

use kernel_native_io::serial;
use libkernel_log::CommonLogger;
use spin::RwLock;

pub struct KernelLogWriter {
    additional_writers: Option<RwLock<Vec<Arc<dyn Fn(&str) -> Result<(), ()> + Send + Sync>>>>,
    disable_additional_writers: AtomicBool,
    force_serial: AtomicBool,
}

impl KernelLogWriter {
    const fn new() -> Self {
        Self {
            additional_writers: None,
            disable_additional_writers: AtomicBool::new(true),
            force_serial: AtomicBool::new(false),
        }
    }

    pub fn disable_additional_writers(&self) {
        self.disable_additional_writers.store(true, Ordering::SeqCst);
    }

    pub fn enable_additional_writers(&self) {
        self.disable_additional_writers.store(false, Ordering::SeqCst);
    }

    pub fn force_serial(&self) {
        self.force_serial.store(true, Ordering::SeqCst);
    }

    pub fn unforce_serial(&self) {
        self.force_serial.store(false, Ordering::SeqCst);
    }
}

static SHARED_KERNEL_LOG_WRITER: RwLock<KernelLogWriter> = RwLock::new(KernelLogWriter::new());

pub struct Logger {
    name: &'static str,
    log_writer: &'static RwLock<KernelLogWriter>,
}

impl fmt::Debug for Logger {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Logger").field("name", &self.name).finish()
    }
}

impl fmt::Write for KernelLogWriter {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let disable_additional_writers = self.disable_additional_writers.load(Ordering::Relaxed);
        if disable_additional_writers || self.force_serial.load(Ordering::Relaxed) {
            serial::write_serial(s).or(Err(fmt::Error))?;
        }
        if !disable_additional_writers {
            if let Some(additional_writers_mutex) = self.additional_writers.as_ref() {
                let additional_writers = additional_writers_mutex.read();
                for writer in additional_writers.iter() {
                    writer(s).or(Err(fmt::Error))?;
                }
            }
        }
        Ok(())
    }
}

pub fn register_additional_writer<WF>(f: WF) where WF : Fn(&str) -> Result<(), ()> + Send + Sync + 'static {
    let shared_log_writer = SHARED_KERNEL_LOG_WRITER.read();
    if shared_log_writer.additional_writers.is_none() {{
        drop(shared_log_writer);
        enable_additional_writers();
        let mut shared_log_writer = SHARED_KERNEL_LOG_WRITER.write();
        shared_log_writer.additional_writers = Some(RwLock::new(Vec::new()))
    }}
    let shared_log_writer = SHARED_KERNEL_LOG_WRITER.write();
    if let Some(additional_writers_mutex) = shared_log_writer.additional_writers.as_ref() {{
        let mut additional_writers = additional_writers_mutex.write();
        additional_writers.push(Arc::new(f));
    }}
}

pub fn disable_additional_writers() {
    let writer = SHARED_KERNEL_LOG_WRITER.read();
    writer.disable_additional_writers();
}

pub fn enable_additional_writers() {
    let writer = SHARED_KERNEL_LOG_WRITER.read();
    writer.enable_additional_writers();
}

pub fn force_serial_logger() {
    let writer = SHARED_KERNEL_LOG_WRITER.read();
    writer.force_serial();
}

pub fn unforce_serial_logger() {
    let writer = SHARED_KERNEL_LOG_WRITER.read();
    writer.unforce_serial();
}

impl CommonLogger<KernelLogWriter> for Logger {
    fn new(name: &'static str) -> Self {
        Self {
            name,
            log_writer: &SHARED_KERNEL_LOG_WRITER,
        }
    }

    fn use_writer<F>(&self, f: F) where F: FnOnce (&mut KernelLogWriter) {
        let mut writer = self.log_writer.write();
        f(&mut writer);
    }

    fn name(&self) -> &'static str {
        self.name
    }
}
