
use libkernel_graphics_defs::framebuffer::VideoModeMasks;
use tinystd::types::Addr;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MemAreaUsage {
    Uninitialized,
    Unknown,
    Usable,
    Used,
    Reserved,
    Bad,
}

#[derive(Debug, Copy, Clone)]
pub struct KernelMemInfoArea {
    pub phys_start: Addr,
    pub size_bytes: usize,
    pub description: Option<&'static str>,
    pub usage: MemAreaUsage,
}

impl KernelMemInfoArea {
    pub const fn default() -> Self {
        Self {
            phys_start: 0_usize,
            size_bytes: 0_usize,
            description: None,
            usage: MemAreaUsage::Uninitialized,
        }
    }
}

const MEM_AREA_LIMIT: usize = 64;

pub const fn default_mem_info_areas() -> [KernelMemInfoArea; MEM_AREA_LIMIT] {
    [KernelMemInfoArea::default(); MEM_AREA_LIMIT]
}

#[derive(Copy, Clone)]
pub struct KernelMemInfo {
    pub mem_areas: [KernelMemInfoArea; MEM_AREA_LIMIT],
    pub page_size: usize,
    pub stack_size: usize,
    pub stack_start: usize,
    pub phys_base: usize,
    pub virt_base: usize,
    pub hhdm: usize
}

#[derive(Copy, Clone)]
pub struct BootInterface {
    pub acpi_rsdp: Addr,
    pub meminfo: KernelMemInfo,
    pub fb: Option<BootFramebuffer>,
    pub preboot_smp: bool,
    pub ap_cpu_count: Option<usize>,
}

impl KernelMemInfo {
    pub fn iter_all_mem_areas(&self) -> impl Iterator<Item = &KernelMemInfoArea> {
        self.mem_areas.iter().filter(|entry| entry.usage != MemAreaUsage::Uninitialized)
    }

    pub fn mem_area_count(&self) -> usize  {
        self.iter_all_mem_areas().count()
    }

    pub fn iter_mem_areas(&self) -> impl Iterator<Item = &KernelMemInfoArea> {
        self.mem_areas.iter().filter(|entry| entry.usage == MemAreaUsage::Usable)
    }

    pub fn iter_used_mem_areas(&self) -> impl Iterator<Item = &KernelMemInfoArea> {
        self.mem_areas.iter().filter(|entry| entry.usage == MemAreaUsage::Used)
    }
}

#[derive(Copy, Clone, Debug)]
pub struct BootFramebuffer {
    pub phys_address: Addr,
    pub width: i32,
    pub height: i32,
    pub pitch: usize,
    pub bpp: u16,
    pub masks: VideoModeMasks,
}

impl BootFramebuffer {
    pub fn size(&self) -> usize {
        self.height as usize * self.pitch
    }
}
