use core::fmt;

#[derive(Copy, Clone, Debug)]
pub struct MemoryProtectionFlags {
    pub read: bool,
    pub write: bool,
    pub execute: bool,
}

pub const fn mem_protection_r() -> MemoryProtectionFlags {
    MemoryProtectionFlags { read: true, write: false, execute: false }
}

pub const fn mem_protection_rw() -> MemoryProtectionFlags {
    MemoryProtectionFlags { read: true, write: true, execute: false }
}

pub const fn mem_protection_rx() -> MemoryProtectionFlags {
    MemoryProtectionFlags { read: true, write: false, execute: true }
}

impl fmt::Display for MemoryProtectionFlags {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let r = if self.read { 'r' } else { '-' };
        let w = if self.write { 'w' } else { '-' };
        let x = if self.execute { 'x' } else { '-' };
        f.write_fmt(format_args!("{}{}{}", r, w, x))
    }
}
