use libkernel_graphics_defs::{geometry::point::Point, visual::color::Color, framebuffer::Framebuffer};

pub trait FramebufferRenderer {
    fn plot_pixel(&self, point: Point, color: Color);
    fn plot_row(&self, y: i32, color: Color) {
        for x in 0..self.width() {
            self.plot_pixel(Point::new(x, y), color)
        }
    }

    fn clear_with_color(&self, color: Color) {
        for y in 0..self.height() {
            self.plot_row(y, color);
        }
    }

    fn width(&self) -> i32;
    fn height(&self) -> i32;
    fn pitch(&self) -> usize;

    fn size(&self) -> usize {
        self.width() as usize * self.pitch()
    }
}

pub struct SimpleFramebufferRenderer {
    fb: Framebuffer<u32>,
}

impl SimpleFramebufferRenderer {
    pub fn new(fb: Framebuffer<u32>) -> Self {
        Self { fb }
    }
}

impl FramebufferRenderer for SimpleFramebufferRenderer {
    fn plot_pixel(&self, point: Point, color: Color) {
        let Point { x, y } = point;
        let index = y as usize * self.fb.width as usize + x as usize;
        let mut buf = self.fb.buffer.lock();
        buf[index] = self.fb.masks.pack(color.r, color.g, color.b);
    }

    fn plot_row(&self, y: i32, color: Color) {
        let index = y as usize * self.fb.width as usize;
        let mut buf = self.fb.buffer.lock();
        for i in index..index + self.fb.width as usize {
            buf[i] = self.fb.masks.pack(color.r, color.g, color.b);
        }
    }

    fn width(&self) -> i32 {
        self.fb.width
    }

    fn height(&self) -> i32 {
        self.fb.height
    }

    fn pitch(&self) -> usize {
        self.fb.pitch
    }
}


