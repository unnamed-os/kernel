use core::ops::{Add, Sub, Mul, Div};

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self { x: self.x + other.x, y: self.y + other.y }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self { x: self.x - other.x, y: self.y - other.y }
    }
}

impl Mul for Point {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        Self { x: self.x * other.x, y: self.y * other.y }
    }
}

impl Div for Point {
    type Output = Self;

    fn div(self, other: Self) -> Self::Output {
        Self { x: self.x / other.x, y: self.y / other.y }
    }
}

impl Default for Point {
    fn default() -> Self {
        Self { x: 0, y: 0 }
    }
}

impl From<(i32, i32)> for Point {
    fn from(value: (i32, i32)) -> Self {
        Self::new(value.0, value.1)
    }
}

impl From<(u32, u32)> for Point {
    fn from(value: (u32, u32)) -> Self {
        Self::new(value.0 as i32, value.1 as i32)
    }
}

impl From<(i16, i16)> for Point {
    fn from(value: (i16, i16)) -> Self {
        Self::new(value.0 as i32, value.1 as i32)
    }
}

impl From<(i8, i8)> for Point {
    fn from(value: (i8, i8)) -> Self {
        Self::new(value.0 as i32, value.1 as i32)
    }
}
