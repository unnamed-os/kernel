#![no_std]

extern crate alloc;

pub mod framebuffer;
pub mod geometry;
pub mod visual;
