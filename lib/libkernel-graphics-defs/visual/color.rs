use num::{Num, Unsigned};

// BPS: Bits Per Subpixel
// BPA: Bits Per Alpha Channel

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct BitColor<BPS : Num + Unsigned + Copy, BPA : Num + Unsigned> {
    pub r: BPS,
    pub g: BPS,
    pub b: BPS,
    pub a: BPA,
}

impl BitColor<u8, u8> {
    pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }

    pub fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self::rgba(r, g, b, 255)
    }

    pub fn grayscale(value: u8) -> Self {
        Self::rgb(value, value, value)
    }

    pub fn with_alpha(&self, a: u8) -> Self {
        Self::rgba(self.r, self.g, self.b, a)
    }

    pub fn at_intensity(&self, i: f32) -> Self {
        Self::rgb((self.r as f32 * i) as u8, (self.g as f32 * i) as u8, (self.b as f32 * i) as u8)
    }
}

// Default is 32-bit color (8 bpp)
pub type Color = BitColor<u8, u8>;

pub const WHITE: Color = Color::rgba(255, 255, 255, 255);
pub const BLACK: Color = Color::rgba(0, 0, 0, 255);
pub const RED: Color = Color::rgba(255, 0, 0, 255);
pub const GREEN: Color = Color::rgba(0, 255, 0, 255);
pub const BLUE: Color = Color::rgba(0, 255, 0, 255);
pub const CYAN: Color = Color::rgba(0, 255, 255, 255);
pub const MAGENTA: Color = Color::rgba(255, 0, 255, 255);
pub const YELLOW: Color = Color::rgba(255, 255, 0, 255);
