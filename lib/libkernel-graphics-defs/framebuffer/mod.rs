use alloc::sync::Arc;
use num::{Unsigned, Num};
use spin::Mutex;

pub struct Framebuffer<PixelWidth : Num + Unsigned + Copy + Send + 'static> {
    pub buffer: Arc<Mutex<&'static mut [PixelWidth]>>,
    pub width: i32,
    pub height: i32,
    pub pitch: usize,
    pub bpp: u16,
    pub masks: VideoModeMasks,
}

#[derive(Copy, Clone, Debug)]
pub struct VideoModeMasks {
    pub red_mask: VideoChannelMask,
    pub green_mask: VideoChannelMask,
    pub blue_mask: VideoChannelMask,
}

impl VideoModeMasks {
    pub fn pack(&self, red: u8, green: u8, blue: u8) -> u32 {
        self.red_mask.apply(red) | self.green_mask.apply(green) | self.blue_mask.apply(blue)
    }
}

#[derive(Copy, Clone, Debug)]
pub struct VideoChannelMask {
    pub size: u8,
    pub shift: u8,
}

impl VideoChannelMask {
    pub fn apply(&self, value: u8) -> u32 {
        let mask: u32 = (1 << self.size) - 1;
        ((value as u32) & mask) << self.shift
    }
}



