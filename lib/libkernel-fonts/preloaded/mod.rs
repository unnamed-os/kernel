use alloc::sync::Arc;
use fontdue::Font;
use tinystd_alloc::lateinit::LateInitArc;

pub struct PreloadedFont {
    font_bytes: &'static [u8],
    font: LateInitArc<Font>,
}

impl PreloadedFont {
    fn prepare_font(&self) {
        self.font.set(
            Font::from_bytes(
                self.font_bytes,
                fontdue::FontSettings::default()
            ).unwrap()
        );
    }

    pub fn font(&self) -> Arc<Font> {
        match self.font.try_get() {
            Ok(f) => f,
            Err(_) => {
                self.prepare_font();
                self.font.get()
            }
        }
    }
}

static HASKLIG_REGULAR: PreloadedFont = PreloadedFont {
    font_bytes: include_bytes!("../../../../out/product/kernel-font-hasklig/unzip/OTF/Hasklig-Regular.otf"),
    font: LateInitArc::new(),
};

pub fn default_console_font() -> &'static PreloadedFont { &HASKLIG_REGULAR }
