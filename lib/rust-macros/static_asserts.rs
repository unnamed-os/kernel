pub use static_assertions as sa;

#[macro_export]
// Copied verbatim from https://github.com/Acorn-OS/kernel/blob/4cb4d2c0d7fb481ef607bd336bafb3ea037508a1/kernel/src/arch/mod.rs#L10-14
// Licensed under the GNU General Public License 3.0
// Copyright Kim Dewelski ("iqon")
macro_rules! assert_fn {
    ($fn:path: $($tt:tt)*) => {
        const _: $($tt)* = $fn;
    };
}
// End of code copied verbatim
pub use assert_fn;

// Thanks to https://docs.rs/static_assertions/latest/src/static_assertions/assert_impl.rs.html#113-121
// and https://github.com/Acorn-OS/kernel/blob/4cb4d2c0d7fb481ef607bd336bafb3ea037508a1/kernel/src/arch/mod.rs#L10-21
// for inspiration
#[macro_export]
macro_rules! export_impl_all {
    ($type:ty: $($trait:path),+ $(,)?) => {
        rust_macros::static_asserts::sa::assert_impl_all!($type: $($trait),+);
        pub use $type;
    }
}
pub use export_impl_all;

#[macro_export]
macro_rules! export_assert_fn {
    ($fn:path: $($proto:tt)*) => {
        rust_macros::static_asserts::assert_fn!($fn: $($proto)*);
        pub use $fn;
    }
}
pub use export_assert_fn;

