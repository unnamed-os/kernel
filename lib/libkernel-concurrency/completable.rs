use core::{future::Future, task::{Poll, Waker}};

use alloc::sync::Arc;
use spin::Mutex;

struct CompletableState<T : Sized + Send + Copy> {
    value: Option<T>,
    waker: Option<Waker>
}

impl<T : Sized + Send + Copy> CompletableState<T> {
    fn new() -> Self {
        Self {
            value: None,
            waker: None,
        }
    }
}

#[derive(Clone)]
pub struct CompletableFuture<T : Sized + Send + Copy> {
    state: Arc<Mutex<CompletableState<T>>>,
}

impl<T : Sized + Send + Copy> CompletableFuture<T> {
    pub fn new() -> Self {
        Self {
            state: Arc::new(Mutex::new(CompletableState::new()))
        }
    }

    pub fn complete(&self, value: T) {
        let mut state = self.state.lock();
        state.value = Some(value);
        if state.waker.is_some() {
            let waker = state.waker.clone().unwrap();
            // drop the state to release the lock
            drop(state);
            waker.wake();
        }
    }

    pub fn get(&self) -> Option<T> {
        (*self.state.lock()).value
    }

    pub fn reset(&self) {
        let mut state = self.state.lock();
        state.value = None;
        state.waker = None;
    }
}

impl<T : Sized + Send + Copy> Future for CompletableFuture<T> {
    type Output = T;

    fn poll(self: core::pin::Pin<&mut Self>, cx: &mut core::task::Context<'_>) -> core::task::Poll<Self::Output> {
        let mut state = self.state.lock();
        if let Some(completed_value) = state.value {
            Poll::Ready(completed_value)
        } else {
            state.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}
