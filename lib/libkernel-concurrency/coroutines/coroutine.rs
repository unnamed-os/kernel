use core::{pin::Pin, sync::atomic::{AtomicU64, Ordering}};

use alloc::boxed::Box;
use futures::Future;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub(super) struct CoroutineId(u64);

impl CoroutineId {
    fn next() -> Self {
        static NEXT_TASK_ID: AtomicU64 = AtomicU64::new(1);
        Self(NEXT_TASK_ID.fetch_add(1, Ordering::Relaxed))
    }
}

pub struct Coroutine {
    pub(super) id: CoroutineId,
    pub(super) future: Pin<Box<dyn Future<Output = ()> + Send + Sync>>,
}

impl Coroutine {
    pub(crate) fn new(future: impl Future<Output = ()> + Send + Sync + 'static) -> Self {
        Self {
            id: CoroutineId::next(),
            future: Box::pin(future),
        }
    }

    pub(crate) fn poll(&mut self, cx: &mut core::task::Context) -> core::task::Poll<()> {
        self.future.as_mut().poll(cx)
    }
}

pub trait CoroutineAsyncAdapter {
    fn coroutine(self) -> Coroutine;
}

impl<A: Future<Output = ()> + Send + Sync + 'static> CoroutineAsyncAdapter for A {
    fn coroutine(self) -> Coroutine {
        Coroutine::new(self)
    }
}
