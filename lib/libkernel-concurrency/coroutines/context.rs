use core::task::Waker;

use alloc::{collections::{VecDeque, BTreeMap}, sync::Arc};
use spin::Mutex;

use super::coroutine::{CoroutineId, Coroutine};

pub struct CoroutineContext {
    pub(super) coroutines: Mutex<BTreeMap<CoroutineId, Arc<Mutex<Coroutine>>>>,
    pub(super) wakers: Arc<Mutex<BTreeMap<CoroutineId, Waker>>>,
    pub(super) queue: Arc<Mutex<VecDeque<CoroutineId>>>,
}

impl CoroutineContext {
    pub fn empty() -> Self {
        Self {
            coroutines: Mutex::new(BTreeMap::new()),
            wakers: Arc::new(Mutex::new(BTreeMap::new())),
            queue: Arc::new(Mutex::new(VecDeque::new())),
        }
    }
}
