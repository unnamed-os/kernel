use core::task::Waker;

use alloc::{collections::VecDeque, sync::Arc, task::Wake};
use futures::task::AtomicWaker;
use spin::Mutex;

use super::coroutine::CoroutineId;

static GLOBAL_WAKER: AtomicWaker = AtomicWaker::new();

pub(super) struct CoroutineContinuation {
    coroutine_id: CoroutineId,
    queue: Arc<Mutex<VecDeque<CoroutineId>>>,
}

impl CoroutineContinuation {
    pub(super) fn create(
        coroutine_id: CoroutineId,
        coroutine_queue: Arc<Mutex<VecDeque<CoroutineId>>>
    ) -> Waker {
        Waker::from(Arc::new(Self {
            coroutine_id,
            queue: coroutine_queue,
        }))
    }

    fn resume(&self) {
        (*self.queue.lock()).push_back(self.coroutine_id);
    }
}

impl Wake for CoroutineContinuation {
    fn wake(self: Arc<Self>) {
        self.resume();
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.resume();
    }
}

pub fn register_global_waker(waker: &Waker) {
    GLOBAL_WAKER.register(waker);
}