use core::{task::{Context, Poll, Waker}, sync::atomic::{AtomicBool, Ordering}};

use alloc::{sync::Arc, task::Wake, collections::BTreeMap};
use futures::Future;
use kernel_native_defs::cpu::CPU;
use klog::{Logger, libkernel_log::CommonLogger};
use spin::Mutex;

use super::{
    context::CoroutineContext,
    coroutine::{CoroutineAsyncAdapter, CoroutineId},
    CoroutineError,
    continuation::{CoroutineContinuation, register_global_waker}
};

pub struct CoroutineScope {
    pub(super) context: Arc<Mutex<CoroutineContext>>,
    logger: Logger,
    waker_registered: AtomicBool,
}

impl CoroutineScope {
    pub fn new(context: CoroutineContext) -> Self {
        Self {
            context: Arc::new(Mutex::new(context)),
            logger: Logger::new("libkernel/concurrency/coroutine-scope"),
            waker_registered: AtomicBool::new(false),
        }
    }
}

impl CoroutineScope {
    pub fn launch(&self, future: impl Future<Output = ()> + Send + Sync + 'static) -> Result<(), CoroutineError> {
        let coroutine = future.coroutine();
        let context = self.context.lock();
        let coroutine_id = coroutine.id;
        if (*context.coroutines.lock()).insert(coroutine_id, Arc::new(Mutex::new(coroutine))).is_some() {
            self.logger.fatalf(format_args!(
                "Tried to launch the same coroutine with {:?} twice.",
                coroutine_id
            ));
            panic!("Coroutine with same ID already launched")
        }
        (*context.queue.lock()).push_back(coroutine_id);
        Ok(())
    }

    pub fn run(&self) -> ! {
        if !self.waker_registered.swap(true, Ordering::SeqCst) {
            {
                register_global_waker(&Waker::from(Arc::new(CoroutineScopeWaker {
                    wakers: (*self.context.lock()).wakers.clone()
                })));
            }
        }
        loop {
            self.process_queue();
            kernel_native_cpu::cur_cpu().wait_for_interrupt();
        }
    }

    fn process_queue(&self) {
        while let Some(coroutine_id) = {
            let context = self.context.lock();
            let mut queue = context.queue.lock();
            queue.pop_front()
        } {
            let (coroutine, waker) = {
                let context = self.context.lock();
                let coroutines = context.coroutines.lock();
                let coroutine = match coroutines.get(&coroutine_id) {
                    Some(c) => c,
                    _ => continue
                }.clone();

                (coroutine, {
                    let mut wakers = context.wakers.lock();
                    wakers.entry(coroutine_id)
                        .or_insert_with(||
                            CoroutineContinuation::create(coroutine_id, context.queue.clone())
                        )
                        .clone()
                })
            };
            let mut wake_context = Context::from_waker(&waker);

            let mut coroutine = coroutine.lock();
            match coroutine.poll(&mut wake_context) {
                Poll::Ready(()) => {
                    let context = self.context.lock();
                    let mut coroutines = context.coroutines.lock();
                    coroutines.remove(&coroutine_id);
                    let mut wakers = context.wakers.lock();
                    wakers.remove(&coroutine_id);
                },
                Poll::Pending => {
                    let context = self.context.lock();
                    let mut queue = context.queue.lock();
                    queue.push_back(coroutine_id);
                }
            }
        }
    }
}

struct CoroutineScopeWaker {
    wakers: Arc<Mutex<BTreeMap<CoroutineId, Waker>>>
}

impl Wake for CoroutineScopeWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        // only wake the first waker otherwise we will cause a deadlock
        if let Some(waker) = {
            let wakers = self.wakers.lock();
            if let Some((_, waker)) = wakers.first_key_value() {
                Some(waker.clone())
            } else {
                None
            }
        } {
            waker.wake();
        }
    }
}
