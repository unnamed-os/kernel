
pub mod context;
pub mod scope;
pub mod coroutine;
pub mod continuation;

#[derive(Debug)]
pub enum CoroutineError {
    LaunchFailure,
    Err(&'static str),
}

