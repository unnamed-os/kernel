
#![no_std]

extern crate alloc;

use core::fmt;

type LogLevel = &'static str;

pub const LEVEL_FATAL: LogLevel = "FATAL";
pub const LEVEL_ERROR: LogLevel = "ERROR";
pub const LEVEL_WARN: LogLevel = "WARN";
pub const LEVEL_NOTICE: LogLevel = "NOTICE";
pub const LEVEL_INFO: LogLevel = "INFO";
pub const LEVEL_DEBUG: LogLevel = "DEBUG";
pub const LEVEL_TRACE: LogLevel = "TRACE";

pub trait CommonLogger<TWrite : fmt::Write> {
    fn new(name: &'static str) -> Self;

    fn use_writer<F>(&self, f: F) where F: FnOnce (&mut TWrite);
    fn name(&self) -> &'static str { "" }

    fn msgf(&self, level: LogLevel, args: fmt::Arguments) {
        self.use_writer(|writer| {
            // Ignore errors because we don't want to interfere with the kernel
            let _ = writer.write_fmt(format_args!("{l:<6} {n}: {a}\n", n=self.name(), l=level, a=args));
        })
    }

    fn fatalf(&self, args: fmt::Arguments) { self.msgf(LEVEL_FATAL, args) }
    fn errorf(&self, args: fmt::Arguments) { self.msgf(LEVEL_ERROR, args) }
    fn warnf(&self, args: fmt::Arguments) { self.msgf(LEVEL_WARN, args) }
    fn noticef(&self, args: fmt::Arguments) { self.msgf(LEVEL_NOTICE, args) }
    fn infof(&self, args: fmt::Arguments) { self.msgf(LEVEL_INFO, args) }
    fn debugf(&self, args: fmt::Arguments) { self.msgf(LEVEL_DEBUG, args) }
    fn tracef(&self, args: fmt::Arguments) { self.msgf(LEVEL_TRACE, args) }
    fn fatal(&self, msg: &str) { self.fatalf(format_args!("{}", msg)) }
    fn error(&self, msg: &str) { self.errorf(format_args!("{}", msg)) }
    fn warn(&self, msg: &str) { self.warnf(format_args!("{}", msg)) }
    fn notice(&self, msg: &str) { self.noticef(format_args!("{}", msg)) }
    fn info(&self, msg: &str) { self.infof(format_args!("{}", msg)) }
    fn debug(&self, msg: &str) { self.debugf(format_args!("{}", msg)) }
    fn trace(&self, msg: &str) { self.tracef(format_args!("{}", msg)) }
}
