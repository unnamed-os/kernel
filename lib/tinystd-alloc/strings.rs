use core::ptr::slice_from_raw_parts;
use alloc::string::String;
use crate::alloc::borrow::ToOwned;

pub unsafe fn string_from_parts(ptr: *const u8, len: usize) -> Result<String, &'static str> {
    let signature_slice = slice_from_raw_parts(ptr, len).as_ref()
        .ok_or("Can't convert ptr/slice combo to raw parts")?;
    String::from_utf8(signature_slice.to_owned())
        .ok().ok_or("Can't create String from utf8 slice")
}
