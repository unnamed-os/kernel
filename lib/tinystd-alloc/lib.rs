// SPDX-License-Identifier: GPL-3.0-or-later

#![no_std]

extern crate alloc;

pub mod strings;
pub mod lateinit;
pub mod slices;
pub mod alignment;
pub mod generics;

