use core::any::{TypeId, type_name, Any};
use core::fmt::Debug;

use alloc::vec;
use alloc::{sync::Arc, collections::BTreeMap, format, string::String, vec::Vec};
use spin::{RwLock, Mutex};
use tinystd_sync::Waiter;

pub struct AnyArc {
    arc: Arc<()>,
    type_id: TypeId,
}

impl Debug for AnyArc {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("AnyArc").field("arc", &self.arc).field("type_id", &self.type_id).finish()
    }
}

impl AnyArc {
    pub fn new<T : 'static>(t: T) -> Self {
        Self::of(Arc::new(t))
    }

    pub fn cloned_from<T : Clone + 'static>(t: T) -> Self {
        Self::of(Arc::new(t.clone()))
    }

    pub fn of<T : ?Sized + 'static>(t: Arc<T>) -> Self {
        Self {
            // SAFETY: It is safe to convert Arc<T> to Arc<()> since Arc
            //         only stores a pointer which always has the same size.
            //         Additionally, Arc accepts ?Sized which means that the size
            //         of T is not relevant for the size of Arc.
            //         Additionally, through the use of .clone() it is ensured that
            //         Arc is properly reference-counted.
            arc: unsafe {
                (&t as *const Arc<T> as *const Arc<()>).as_ref()
            }.unwrap().clone(),
            type_id: TypeId::of::<T>(),
        }
    }

    pub fn try_solidify<T : ?Sized + 'static>(&self) -> Option<Arc<T>> {
        if TypeId::of::<T>() == self.type_id {
            // SAFETY: It is safe to convert back for the reasons mentioned in of().
            //         Also, the if checked if the type inside the Arc is the same.
            Some(unsafe {
                (&self.arc as *const Arc<()> as *const Arc<T>).as_ref()
            }.unwrap().clone())
        } else {
            None
        }
    }

    pub fn solidify<T : ?Sized + 'static>(&self) -> Arc<T> {
        self.try_solidify().unwrap()
    }

    pub unsafe fn solidify_unchecked<T : ?Sized + 'static>(&self) -> Arc<T> {
        (&self.arc as *const Arc<()> as *const Arc<T>).as_ref().unwrap().clone()
    }

}

pub struct GenericTypeMap {
    map: BTreeMap<TypeId, AnyArc>
}

impl GenericTypeMap {
    pub fn new() -> Self {
        Self {
            map: BTreeMap::new()
        }
    }

    pub fn get<T : 'static>(&self) -> Option<Arc<T>> {
        if let Some(arc) = self.map.get(&TypeId::of::<T>()) {
            Some(
                // SAFETY: we already know that the type is correct because
                //         by getting a value from BTreeMap by TypeId, the implementation
                //         of BTreeMap, if correct, guarantees that the type can be cast safely
                unsafe { arc.solidify_unchecked() }
            )
        } else {
            None
        }
    }

    pub fn set<T : 'static>(&mut self, obj: T) -> Option<Arc<T>> {
        if let Some(arc) = self.map.insert(TypeId::of::<T>(), AnyArc::new(obj)) {
            Some(
                // SAFETY: we already know that the type is correct because
                //         by inserting a value from BTreeMap by TypeId, the implementation
                //         of BTreeMap, if correct, guarantees that the return type can be cast safely
                unsafe { arc.solidify_unchecked() }
            )
        } else {
            None
        }
    }

    pub fn remove<T : 'static>(&mut self) -> Option<Arc<T>> {
        if let Some(arc) = self.map.remove(&TypeId::of::<T>()) {
            Some(
                // SAFETY: we already know that the type is correct because
                //         by removing a value from BTreeMap by TypeId, the implementation
                //         of BTreeMap, if correct, guarantees that the return type can be cast safely
                unsafe { arc.solidify_unchecked() }
            )
        } else {
            None
        }
    }

    pub fn has<T : ?Sized  + 'static>(&self) -> bool {
        self.map.contains_key(&TypeId::of::<T>())
    }

}

pub struct GenericLateInitArc {
    value: RwLock<Option<AnyArc>>,
    waiter: Waiter,
    type_id: RwLock<Option<TypeId>>,
    implements: RwLock<Vec<TypeId>>,
}

impl Debug for GenericLateInitArc {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let value = self.value.read();
        let type_id = self.type_id.read();
        let implements = self.implements.read();
        f.debug_struct("GenericLateInitArc")
            .field("value", &value)
            .field("waiter", &"<waiter>")
            .field("type_id", &type_id)
            .field("implements", &implements).finish()
    }
}

impl GenericLateInitArc {
    pub fn new<T : 'static>() -> Self {
        let new = Self {
            value: RwLock::new(None),
            waiter: Waiter::new(),
            type_id: RwLock::new(Some(TypeId::of::<T>())),
            implements: RwLock::new(Vec::new()),
        };

        new
    }

    pub fn new_impl<T : ?Sized + Any>() -> Self {
        Self {
            value: RwLock::new(None),
            waiter: Waiter::new(),
            type_id: RwLock::new(None),
            implements: RwLock::new(vec![TypeId::of::<T>()]),
        }
    }

    pub fn new_unknown_impl() -> Self {
        Self {
            value: RwLock::new(None),
            waiter: Waiter::new(),
            type_id: RwLock::new(None),
            implements: RwLock::new(Vec::new()),
        }
    }

    /// Restricts the type of values that can be stored
    pub fn must_implement<T : ?Sized + Any>(&self) {
        let value = self.value.write();
        let type_id = self.type_id.read();
        let mut implements = self.implements.write();
        if value.is_some() && type_id.is_some_and(|type_id| implements.contains(&type_id)) {
            panic!("can't set bound for this GenericLateInitArc: already has a value out of bound")
        }
        implements.push(TypeId::of::<T>());
    }

    pub fn try_set<T : 'static>(&self, v: T) -> Result<(), String> {
        let mut value = self.value.write();
        let type_id = self.type_id.read();
        if type_id.is_some_and(|type_id| TypeId::of::<T>() != type_id) {
            return Err(format!(
                "Type {} does not match stored type",
                type_name::<T>()
            ))
        }
        if value.is_some() {
            return Err("Can't set value in GenericLateInitArc: is already initialized".into());
        }
        *value = Some(AnyArc::new(v));
        self.waiter.notify();
        Ok(())
    }

    pub fn set<T : 'static>(&self, v: T) {
        self.try_set(v).unwrap();
    }

    pub fn get<T : ?Sized + 'static>(&self) -> Arc<T> {
        self.value.read().as_ref().unwrap().solidify::<T>().clone()
    }

    pub unsafe fn get_unchecked<T : ?Sized + 'static>(&self) -> Arc<T> {
        self.value.read().as_ref().unwrap().solidify_unchecked::<T>().clone()
    }

    pub fn has_value(&self) -> bool {
        let value = self.value.read();
        value.is_some()
    }

    pub fn try_get<T : 'static>(&self) -> Result<Arc<T>, String> {
        let value = self.value.read();
        if value.is_none() {
            return Err("Can't get value in GenericLateInitArc: is not initialized".into());
        }
        if let Some(value) = value.as_ref().unwrap().try_solidify::<T>() {
            Ok(value)
        } else {
            Err(format!(
                "Can't get value in GenericLateInitArc: type {} does not match stored value",
                type_name::<T>(),
            ))
        }
    }

    pub fn wait_for_init(&self) {
        self.waiter.wait()
    }

    pub fn wait_for<T : ?Sized + 'static>(&self) -> Arc<T> {
        self.wait_for_init();
        self.get()
    }

}

pub struct GenericLateInitTypeMap {
    map: Mutex<BTreeMap<TypeId, Arc<GenericLateInitArc>>>
}

impl GenericLateInitTypeMap {
    pub fn new() -> Self {
        Self {
            map: Mutex::new(BTreeMap::new())
        }
    }

    pub fn get<T : Sized + 'static>(&self) -> Option<Arc<T>> {
        let mut map = self.map.lock();
        if !map.contains_key(&TypeId::of::<T>()) {
            map.insert(TypeId::of::<T>(), Arc::new(GenericLateInitArc::new::<T>()));
        }
        map.get(&TypeId::of::<T>()).and_then(|arc| Some(
            // SAFETY: getting it via the map ensures that the type matches
            unsafe { arc.get_unchecked::<T>() }
        ))
    }

    fn get_generic<T : 'static>(&self) -> Arc<GenericLateInitArc> {
        let mut map = self.map.lock();
        let type_id = TypeId::of::<T>();
        if !map.contains_key(&type_id) {
            map.insert(type_id, Arc::new(GenericLateInitArc::new::<T>()));
        }
        map.get(&type_id).unwrap().clone()
    }

    pub fn set<T : 'static>(&self, obj: T) {
        let mut map = self.map.lock();
        if let Some(arc) = map.get(&TypeId::of::<T>()) {
            arc.set(obj);
        } else {
            let arc = Arc::new(GenericLateInitArc::new::<T>());
            arc.set(obj);
            map.insert(TypeId::of::<T>(), arc);
        }
    }

    pub fn has<T : 'static>(&self) -> bool {
        let map = self.map.lock();
        map.contains_key(&TypeId::of::<T>())
    }

    pub fn wait_for<T : 'static>(&self) -> Arc<T> {
        self.get_generic::<T>().wait_for::<T>()
    }

}
