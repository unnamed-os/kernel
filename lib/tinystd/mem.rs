// SPDX-License-Identifier: GPL-3.0-or-later

use core::mem::size_of;

use crate::types::Addr;

pub unsafe fn memzero(dest: *mut u8, len: usize) {
    dest.write_bytes(0, len)
}

pub fn ptr_after_end_of<T, R>(value: &T) -> *const R {
    (value as *const T as Addr + size_of::<T>()) as *const R
}

pub fn addr_after_end_of<T>(value: &T) -> Addr {
    (value as *const T as Addr + size_of::<T>()) as Addr
}
